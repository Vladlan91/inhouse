<!DOCTYPE html>
<html>
<head>
    <title>Elegant Error Page Flat Responsive Widget Template :: w3layouts</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Elegant Error Page template Responsive, Login form web template,Flat Pricing tables,Flat Drop downs  Sign up Web Templates, Flat Web Templates, Login sign up Responsive web template, SmartPhone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- Custom Theme files -->
    <link href="/css/error.css" rel="stylesheet" type="text/css" media="all" />
    <!-- //Custom Theme files -->
    <!-- web font -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'><!--web font-->
    <!-- //web font -->
</head>
<body>
<!-- main -->
<div class="agileits-main">
    <div class="agileinfo-row">

        <div class="w3layouts-errortext">
            <h2>У<span>ПП</span>С!</h2>

            <h1>Виникла помилка!!!</h1>
            <p class="w3lstext"> <a href="#">Перейдіть на головну сторінку</a> спробуйте запит ще раз. </p>
            <div class="agile-search">

            </div>
            <div class="w3top-nav-right">
                <ul>
                    <li><a href="#">Головна</a></li>
                    <li><a href="#">Магазин</a></li>
                    <li><a href="#">Блог</a></li>
                    <li><a href="#">Контакти</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>