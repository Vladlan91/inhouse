$('#editor1').ckeditor();
$('#editor2').ckeditor();
$('#editor3').ckeditor();

$('.delete').click(function () {
    var res = confirm('Ви дійсно бажаєте видалити дане замовлення?');
    if (!res) return false;
});

$('.delete-cash').click(function () {
    var res = confirm('Ви дійсно бажаєте очистити кеш?');
    if (!res) return false;
});

$('.del-item').on('click', function () {
    var res = confirm('Підтвердіть дію');
    if (!res) return false;
    var $this = $(this),
        id = $this.data('id'),
        src = $this.data('src');
    $.ajax({
        url: adminpath + '/product/delete-gallery',
        data: {id: id, src: src},
        type: 'POST',
        beforeSend: function () {
            $this.closest('.file-upload').find('.overlay').css({'display':'block'});
        },
        success: function () {
            setTimeout(function () {
                $this.closest('.file-upload').find('.overlay').css({'display': 'none'});
                if (res == 1){
                    $this.fadeOut();
                }
            }, 1000);
        },
        error: function () {
            setTimeout(function () {
                $this.closest('.file-upload').find('.overlay').css({'display': 'none'});
                alert('Помилка');
            }, 1000);
        }
    });
});


$('.update').click(function () {
    var res = confirm('Ви дійсно бажаєте змінити статус?');
    if (!res) return false;
});
$('.mt-2 a').each(function () {
    var location = window.location.protocol + '//' + window.location.host + window.location.pathname;
        var link = this.href;
        if (link == location) {
            $(this).parent().addClass('bg-danger');
            $(this).closest('.nav-item').addClass('activ');
        }
});

$('#reset-filter').click(function () {
    $('#filter input[type=radio]').prop('checked', false);
    return false;
});

$(".select2").select2({
    placeholder: "Введіть назву товару",
    cache: true,
    minimumInputLength: 3,
    ajax: {
        url: adminpath + "/product/related-product",
        delay: 250,
        dataType: 'json',
        data: function (params) { // page is the one-based page number tracked by Select2
            return {
                q: params.term, //search term
                page: params.page // page number
            };
        },
        processResults: function (data, params) {
            return { results: data.items,
            };
        }
    }
});

if ($('div').is('#single')) {
    var buttonSingle = $("#single"),
        buttonMulti = $("#multi"),
        file;
}
if (buttonSingle){
    new AjaxUpload(buttonSingle, {
        action: adminpath + buttonSingle.data('url') + "?upload=1",
        data: {name: buttonSingle.data('name')},
        name: buttonSingle.data('name'),
        onSubmit: function(file,ext) {
            if (! (ext && /^(jpg|png|jpeg|gif)$/i.test(ext))){
                alert('Помилка, дозвіл завантаження тільки на картинки!');
                return false;
            }
            buttonSingle.closest('.file-upload').find('.overlay').css({'display':'block'});
        },
        onComplete: function (file, response) {
            setTimeout(function () {
                buttonSingle.closest('.file-upload').find('.overlay').css({'display': 'none'});

                response = JSON.parse(response);
                $('.' + buttonSingle.data('name')).html('<img class="margin del-item" src="/images/productavatar/' + response.file + '" style="max-height: 150px; border-radius: 5px; vertical-align: middle;">');
            }, 1000);
        }

    });
}
if (buttonMulti) {
    new AjaxUpload(buttonMulti, {
        action: adminpath + buttonMulti.data('url') + "?upload=1",
        data: {name: buttonMulti.data('name')},
        name: buttonMulti.data('name'),
        onSubmit: function(file,ext) {
            if (! (ext && /^(jpg|png|jpeg|gif)$/i.test(ext))){
                alert('Помилка, дозвіл завантаження тільки на картинки!');
                return false;
            }
            buttonMulti.closest('.file-upload').find('.overlay').css({'display':'block'});
        },
        onComplete: function (file, response) {
            setTimeout(function () {
                buttonMulti.closest('.file-upload').find('.overlay').css({'display': 'none'});

                response = JSON.parse(response);
                $('.' + buttonMulti.data('name')).append('<img class="margin" src="/images/productgallery/' + response.file + '" style="max-height: 150px; border-radius: 5px; vertical-align: middle;">');
            }, 1000);
        }

    });
}
$(function() {
    // Remove button click
    $(document).on(
        'click',
        '[data-role="dynamic-fields"] > .form-inline [data-role="remove"]',
        function(e) {
            e.preventDefault();
            $(this).closest('.form-inline').remove();
        }
    );
    // Add button click
    $(document).on(
        'click',
        '[data-role="dynamic-fields"] > .form-inline [data-role="add"]',
        function(e) {
            e.preventDefault();
            var container = $(this).closest('[data-role="dynamic-fields"]');
            new_field_group = container.children().filter('.form-inline:first-child').clone();
            new_field_group.find('input').each(function(){
                $(this).val('');
            });
            container.append(new_field_group);
        }
    );
});

$('#add').on('submit', function () {
   if (!isNumeric( $('#category_id').val() )){
       alert("Виберіть категорію");
       return false;
   }
});

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

if ($('div').is('#img')) {
    var buttonSingle = $("#img"),
        buttonMulti = $("#logo"),
        file;
}
if (buttonSingle){
    new AjaxUpload(buttonSingle, {
        action: adminpath + buttonSingle.data('url') + "?upload=1",
        data: {name: buttonSingle.data('name')},
        name: buttonSingle.data('name'),
        onSubmit: function(file,ext) {
            if (! (ext && /^(jpg|png|jpeg|gif)$/i.test(ext))){
                alert('Помилка, дозвіл завантаження тільки на картинки!');
                return false;
            }
            buttonSingle.closest('.file-upload').find('.overlay').css({'display':'block'});
        },
        onComplete: function (file, response) {
            setTimeout(function () {
                buttonSingle.closest('.file-upload').find('.overlay').css({'display': 'none'});

                response = JSON.parse(response);
                $('.' + buttonSingle.data('name')).html('<img class="margin del-item" src="/images/brands/' + response.file + '" style="max-height: 150px; border-radius: 5px; vertical-align: middle;">');
            }, 1000);
        }

    });
}
if (buttonMulti) {
    new AjaxUpload(buttonMulti, {
        action: adminpath + buttonMulti.data('url') + "?upload=1",
        data: {name: buttonMulti.data('name')},
        name: buttonMulti.data('name'),
        onSubmit: function(file,ext) {
            if (! (ext && /^(jpg|png|jpeg|gif)$/i.test(ext))){
                alert('Помилка, дозвіл завантаження тільки на картинки!');
                return false;
            }
            buttonMulti.closest('.file-upload').find('.overlay').css({'display':'block'});
        },
        onComplete: function (file, response) {
            setTimeout(function () {
                buttonMulti.closest('.file-upload').find('.overlay').css({'display': 'none'});

                response = JSON.parse(response);
                $('.' + buttonMulti.data('name')).append('<img class="margin" src="/images/brands/' + response.file + '" style="max-height: 150px; border-radius: 5px; vertical-align: middle;">');
            }, 1000);
        }

    });
}
