<div class="row" id="filter">
    <div class="col-12">
        <div class="card">
            <div class="card-header d-flex p-0">
                <ul class="nav nav-pills p-2">
                    <?php $i = 1; foreach ($this->groups as $group_id => $group_item):?>
                    <li class="nav-item"><a class="nav-link btn-warning  <?php if ($i == 1) echo 'active show'?> " href="#tab_<?=$group_id;?>" data-toggle="tab"><?=$group_item;?></a></li>
                    <?php $i++; endforeach;?>
                </ul>
                <div class="btn-group card-tools">
                    <button type="button" class="btn btn-tool bg-warning dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-wrench"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right bg-black" role="menu">
                        <a href="#" class="dropdown-item bg-black" id="reset-filter" >Обнулити фільтри</a>
                    </div>
                </div>
            </div><!-- /.card-header -->
            <div class="card-body">
                <div class="tab-content">
                    <?php if (!empty($this->attrs[$group_id])):?>
                        <?php $i = 1; foreach ($this->groups as $group_id => $group_item):?>
                            <div class="tab-pane <?php if ($i == 1) echo 'active show'?> bg-white" id="tab_<?=$group_id;?>">
                                <?php foreach ($this->attrs[$group_id] as $attr_id =>$value):?>
                                <?php
                                if (!empty($this->filter) && in_array($attr_id, $this->filter)){
                                    $checked = 'checked';
                                }else{
                                    $checked = null;
                                }
                                ?>
                                <div class="form-group">
                                    <label>
                                        <input type="radio" name="attrs[<?=$group_id;?>]" value="<?=$attr_id;?>" <?=$checked;?>> <?= $value;?>
                                    </label>
                                </div>
                                <?php $i++; endforeach;?>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</div>