<?php foreach ($this->groups as $group_id => $group_item):?>
    <section  class="sky-form">
        <h4><span class="glyphicon glyphicon-minus" aria-hidden="true"></span><?=$group_item;?></h4>
        <div class="row row1 row2 scroll-pane">
            <div class="col col-4">
                <?php if (!empty($this->attrs[$group_id])):?>
                    <?php foreach ($this->attrs[$group_id] as $attr_id => $value):?>
                        <?php
                        if (!empty($this->filter) && in_array($attr_id, $this->filter)){
                            $checked = 'checked';
                        }else{
                            $checked = null;
                        }
                        ?>
                        <label class="checkbox">
                            <input type="checkbox" name="attrs[<?=$group_id;?>][<?=$attr_id;?>]"  value="<?=$attr_id;?>" <?=$checked;?>><i></i><?=$value;?>
                        </label>
                    <?php endforeach;?>
                <?php endif;?>
            </div>
        </div>
    </section>
<?php endforeach;?>