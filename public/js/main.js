
$(document).ready(function() {

    setTimeout(function(){
        $('body').addClass('loaded');
        $('h1').css('color','#222222');
    }, 1000);

});

$(window).bind("load", function() {

    "use strict";

    $(".spn_hol").fadeOut(1000);
});

$('body').on('change', '.grid-addon input', function () {
    var checked = $('.grid-addon input:checked'),
        data = '';
    checked.each(function () {
        data += this.value + ',';
    })
    if (data){
        $.ajax({
            url: location.href,
            data: {filter: data},
            type: 'GET',
            beforeSend: function () {
                $('.preload').fadeIn(300, function () {
                    $('.product-one').hide();
                });
            },
            success: function (res) {
                $('.preload').delay(300).fadeOut('slow', function () {
                    $('.product-one').html(res).fadeIn();
                    var url = location.search.replace(/filter(.+?)(&|$)/g,'');
                    var newURL = location.pathname + url + (location.search ? "&" : "?") + "filter" + data;
                    newURL = newURL.replace('&&', '&');
                    newURL = newURL.replace('?&', '?');
                    history.pushState({}, '', newURL);

                });
            },
            error: function () {
                alert('Помилка передачі даних')
            }
        });
    }else {
        //window.location.assign("http://localhost:8888/");
        window.location = location.pathname;
    }
    console.log(data);

});

$('body').on('change', '.grid-addon-1 input', function () {
    var checked = $('.grid-addon-1 input:checked'),
        data = '';
    checked.each(function () {
        data += this.value + ',';
    })
    if (data){
        $.ajax({
            url: location.href,
            data: {filter: data},
            type: 'GET',
            beforeSend: function () {
                $('.preload').fadeIn(300, function () {
                    $('.user-one').hide();
                });
            },
            success: function (res) {
                $('.preload').delay(300).fadeOut('slow', function () {
                    $('.user-one').html(res).fadeIn();
                    var url = location.search.replace(/filter(.+?)(&|$)/g,'');
                    var newURL = location.pathname + url + (location.search ? "&" : "?") + "filter" + data;
                    newURL = newURL.replace('&&', '&');
                    newURL = newURL.replace('?&', '?');
                    history.pushState({}, '', newURL);
                });
            },
            error: function () {
                alert('Помилка передачі даних')
            }
        });
    }else {
        window.location = location.pathname;
    }
    console.log(data);

});

var products = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.whitespace,
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
        wildcard: '%QUERY',
        url: path + '/search/typeahead?query=%QUERY'
    }
});

products.initialize();

$("#typeahead").typeahead({
    highlight: true
},{
    name: 'product',
    display: 'title',
    limit: 9,
    source: products

});

$("#typeahead").bind('typeahead:select', function (ev, suggestion) {
    window.location = path + '/search/?s=' + encodeURIComponent(suggestion.title);
});

/*Cart*/
$('body').on('click','.add-to-card-link', function (e) {
    e.preventDefault();
    var id = $(this).data('id'),
        qty = $('.qty input').val() ? $('.qty input').val() : 1,
        mod = $('.availables select').val();
    $.ajax({
        url: '/cart/add',
        data: {id: id, qty: qty, mod: mod},
        type: 'GET',
        success: function (res) {
            showCart(res);

        },
        error: function () {
            alert('Ошибка! Спробуйте пізніше');
        }

    });
});

$('#cart .modal-body').on('click', '.close', function () {
    var id = $(this).data('id');
    $.ajax({
        url: '/cart/delete',
        data: {id: id},
        type: 'GET',
        success: function (res) {
            showCart(res);

        },
        error: function () {
            alert('Ошибка! Спробуйте пізніше');
        }

    });

});

$('#carts .modal-bodys').on('click', '.close', function () {
    var id = $(this).data('id');
    $.ajax({
        url: '/cart/deleteCheck',
        data: {id: id},
        type: 'GET',
        success: function (res) {
            showsCart(res);

        },
        error: function () {
            alert('Ошибка! Спробуйте пізніше');
        }

    });

});

function clearCart() {
    $.ajax({
        url: '/cart/clear',
        type: 'GET',
        success: function (res) {
            showCart(res);

        },
        error: function () {
            alert('Ошибка! Спробуйте пізніше');
        }

    });
}

function  showsCart(cart){
    $('#carts .modal-bodys').html(cart);
    if ($('.total11').text()){
        $('.simpleCart_total').html($('#carts .total11').text());
    }else {
        $('.simpleCart_total').text('Порожній кошик');
    }
}

function  showCart(cart){
    if($.trim(cart) == '<h3>Порожній кошик</h3>'){
        $('#cart .modal-footer a').css('display', 'none');
    }else {
        $('#cart .modal-footer a').css('display', 'inline-block');
    }
    $('#cart .modal-body').html(cart);
    $('#cart').modal();
    if ($('.total11').text()){
        $('.simpleCart_total').html($('#cart .total11').text());
    }else {
        $('.simpleCart_total').text('Порожній кошик');
    }
}

function getCart(){
    $.ajax({
        url: '/cart/show',
        type: 'GET',
        success: function (res) {
            showCart(res);

        },
        error: function () {
            alert('Ошибка! Спробуйте пізніше');
        }

    });
}

/*Cart*/
$('#currency').change(function () {
    window.location = 'currency/change?curr=' + $(this).val();

});
$('.availables select').on('change', function () {
    var modid = $(this).val(),
        mod = $(this).find('option').filter(':selected').data('title'),
        price = $(this).find('option').filter(':selected').data('price'),
        basePrice = $('#base-price').data('base');
    if (price) {
        $('#base-price').text(semboleLeft + price + semboleRight);
    }else{
        $('#base-price').text(semboleLeft + basePrice + semboleRight);
    }
    console.log(modid,mod,price);
    
});
$(document).ready(function(){
    $(".tab1 .single-bottom").hide();
    $(".tab2 .single-bottom").hide();
    $(".tab3 .single-bottom").hide();
    $(".tab4 .single-bottom").hide();
    $(".tab5 .single-bottom").hide();

    $(".tab1 ul").click(function(){
        $(".tab1 .single-bottom").slideToggle(300);
        $(".tab2 .single-bottom").hide();
        $(".tab3 .single-bottom").hide();
        $(".tab4 .single-bottom").hide();
        $(".tab5 .single-bottom").hide();
    })
    $(".tab2 ul").click(function(){
        $(".tab2 .single-bottom").slideToggle(300);
        $(".tab1 .single-bottom").hide();
        $(".tab3 .single-bottom").hide();
        $(".tab4 .single-bottom").hide();
        $(".tab5 .single-bottom").hide();
    })
    $(".tab3 ul").click(function(){
        $(".tab3 .single-bottom").slideToggle(300);
        $(".tab4 .single-bottom").hide();
        $(".tab5 .single-bottom").hide();
        $(".tab2 .single-bottom").hide();
        $(".tab1 .single-bottom").hide();
    })
    $(".tab4 ul").click(function(){
        $(".tab4 .single-bottom").slideToggle(300);
        $(".tab5 .single-bottom").hide();
        $(".tab3 .single-bottom").hide();
        $(".tab2 .single-bottom").hide();
        $(".tab1 .single-bottom").hide();
    })
    $(".tab5 ul").click(function(){
        $(".tab5 .single-bottom").slideToggle(300);
        $(".tab4 .single-bottom").hide();
        $(".tab3 .single-bottom").hide();
        $(".tab2 .single-bottom").hide();
        $(".tab1 .single-bottom").hide();
    })
});
