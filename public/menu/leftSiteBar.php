<?php $parent = isset($category['childs']);?>
<li>
    <a class="sitebare" href="services/<?=$category['alias'];?>"><?=$category['title'];?></a>
    <?php if(isset($category['childs'])): ?>
        <ul class="sitebare_ul" >
            <?= $this->getMenuHtml($category['childs']);?>
        </ul>
    <?php endif; ?>
</li>