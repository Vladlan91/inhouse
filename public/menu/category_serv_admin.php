<?php
$parent = isset($category['childs']);
if (!$parent){
    $red = 'nored';
    $ri = '<i class="fa fa-angle-right "></i>&ensp;&ensp;&ensp;&ensp;';
    $delete = '<a href="' . ADMIN . '/services/delete?id=' . $id . '" class="delete"><i class="fa fa-fw fa-close text-danger"></i></a>';
}else{
    $red = 'red';
    $ri = '<i class="fa fa-angle-double-right "></i>&ensp;&ensp;';
    $delete = '';
}
?>
    <p class="item-p">
        <a class="list-group-item <?=$red;?>" href="<?=ADMIN;?>/services/edit?id=<?=$id;?>"><?=$ri;?><?=$category['title'];?></a>
        <span><?=$delete;?></span>
    </p>
<?php if($parent): ?>
    <div class="list-group">
        <?= $this->getMenuHtml($category['childs']);?>
    </div>
<?php endif; ?>