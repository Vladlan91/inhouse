<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 14/10/2018
 * Time: 14:18
 */

namespace app\controllers;

use app\widgets\filter\Ufilter;
use inhouse\App;
use inhouse\libs\Pagination;

class BuilderController extends AppController {

    public function indexAction() {

        unset($_SESSION['ufilter']);
        $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
        $perpage = App::$app->getProperty('pagination');
        $sql_part = '';
        if (!empty($_GET['filter'])) {
            $filters = Ufilter::getFilter();
            if ($filters){
                $cnt = Ufilter::getCountGroups($filters);
                $sql_part = "AND id IN (SELECT product_id FROM u_attribute_product WHERE attr_id IN ($filters) GROUP BY product_id HAVING COUNT(product_id) = $cnt )";
            }
        }
        $total = \R::count('user', "role = 'builder' $sql_part ");
        $pagination = new Pagination($page, $perpage, $total);
        $start = $pagination->getStart();
        $users = \R::findAll('user', "role = 'builder' $sql_part LIMIT $start, $perpage");
        if ($this->isAjax()) {
            Ufilter::setFilter();
            $this->loadView('filter', compact('users', 'pagination'));
            debug($sql_part);
            debug($users,1);
        }
        $this->set(compact('users', 'pagination'));

    }

    public function viewAction(){
        $elias = $this->route['alias'];
        $elias .= '/';
        $users = \R::findOne('user', 'alias=?',[$elias] );
        //$products = \R::findAll('product', 'brand_id = ?', [$brands->id]);
        $this->setMeta($users->title, $users->description);
        $this->set(compact('users'));
    }


}