<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 25/05/2018
 * Time: 15:23
 */

namespace app\controllers;


use app\models\AppModel;
use app\widgets\currency\Currency;
use inhouse\App;
use inhouse\base\Controller;
use inhouse\Cash;


class AppController extends Controller {

    public function __construct($route) {
        parent::__construct($route);
        new AppModel();
        App::$app->setProperty('currencies', Currency::getCurrencies());
        App::$app->setProperty('currency', Currency::getCurrency(App::$app->getProperty('currencies')));
        App::$app->setProperty('cats', self::cashCategory("category","cats"));
        App::$app->setProperty('serv', self::cashCategory("servises","serv"));
    }

    public function cashCategory($table, $name){
        $cashe = Cash::instance();
        $cats = $cashe->get($name);
        if (!$cats){
            $cats = \R::getAssoc("SELECT * FROM $table");
            $cashe->set($name, $cats);
        }
        return $cats;
    }

    public function getRequestID($get = true, $id = 'id'){
        if ($get){
            $data = $_GET;
        }else{
            $data = $_POST;
        }
        $id = !empty($data[$id]) ? (int)$data[$id] : null;
        if (!$id){
            throw new \Exception('Сторінка не знайдена', 404);
        }
        return $id;
    }

    public function viewPage($table, $id, $date){
        $bean = \R::load($table, $id);
        $bean->view += 1;
        if ($date <= date('Y-m-d')) {
            $bean->owner_id = '0';
            $bean->bought_to = '';
        }
        $monthe = date("n");
        if ($bean->monthe == $monthe){
            $bean->monthe_view += 1;
        }else{
            $bean->last_month = $bean->monthe_view;
            $bean->monthe_view = '0';
            $bean->monthe = $monthe;
        }
        \R::store($bean);
    }


}