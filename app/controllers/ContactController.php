<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 17/06/2018
 * Time: 20:39
 */

namespace app\controllers;


use app\models\Contact;
use inhouse\App;

class ContactController extends AppController {

    public function indexAction(){
        $this->setMeta('Зворотній зв`язок');
    }

    public function addAction(){
        if (!empty($_POST)){
            $contact = new Contact();
            $data = $_POST;
            $contact->load($data);
            $contact->attributes['date'] = date('Y-m-d');
            $servises = \R::findOne('servises', 'id=?',[$contact->attributes['serves_id']] );
            $contact->attributes['serves_id'] = $servises['title'];
                if ($contact->save('contact')){
                    $_SESSION['success'] = 'Ваше повідомлення надіслано успішно, очікуйте на відповідь!';
                    $_SESSION['messages'] = $contact->attributes;
                    Contact::mailOrder($contact->attributes['email']);
                }
                else{
                    $_SESSION['error'] = 'Виникла помилка, надішліть ваше повідомлення повторно!';
                }
            redirect();
        }
        $this->setMeta('Зворотній зв`язок');
    }

}