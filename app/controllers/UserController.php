<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 21/06/2018
 * Time: 00:08
 */

namespace app\controllers;


use app\models\AppModel;
use app\models\User;
use RedBeanPHP\R;

class UserController extends AppController {


    public function signupAction(){
        if (!empty($_POST)){
            $user = new User();
            $data = $_POST;
            $user->load($data);
            if (!$user->validate($data) || !$user->checkUnique()){
                $user->getErrors();
                $_SESSION['form_data'] = $data;
                redirect();
            }else{
                $user->attributes['password'] = password_hash($user->attributes['password'], PASSWORD_DEFAULT);
                $id = $user->save('user');
                $alias = AppModel::createAlias('user','alias', $data['name'], $id);
                $productAlias = \R::load('user', $id);
                $alias .= '/';
                $productAlias->alias = $alias;
                \R::store($productAlias);
                $_SESSION['success'] = 'Ви успішно зареєструвались';
                }
            redirect('profile');
        }
        $this->setMeta('Реєстрація');
    }
    public function signupMasterAction(){
        $this->setMeta('Реєстрація');
    }
    public function signupBuilderAction(){
        $this->setMeta('Реєстрація');
    }

    public function loginAction(){
        if (!empty($_POST)){
            $user = new User();
            if ($user->login()){
                $_SESSION['success'] = 'Ви успіщно авторизовані';
                redirect('profile');
            }else{
                $_SESSION['error'] = 'Виникла помилка авторизаціїss';
            }
            redirect();
        }
        $this->setMeta('Авторизація');
    }

    public function logoutAction(){
        if (isset($_SESSION['user'])){
            unset($_SESSION['user']);
        }
        redirect();

    }

    public function orderViewAction(){
        if (!User::checkAuth())redirect('/user/login');
        $order_id = $this->getRequestID();
        $orde = \R::getRow("SELECT `order`.*,`user`.`name`, ROUND(SUM(`order_product`.`price`),2) AS `sum` FROM `order` JOIN `user` ON `order`.`user_id` = `user`.`id` JOIN `order_product` ON `order`.`id` = `order_product`.`order_id` WHERE `order`.`id` = ? GROUP BY `order`.`id` ORDER BY `order`.`status`, `order`.`id` LIMIT 1", [$order_id]);
        if ($orde){
            $order_products = \R::findAll("order_product", "order_id = ?",[$order_id]);
        }
        $products = \R::findAll('product');
        foreach ($order_products as $order_product => $v){
            foreach ($products as $product => $v){
                if ($order_products[$order_product]['product_id'] == $products[$product]['id']){
                    $order_products[$order_product]['img']= $products[$product]['img'];
                    $order_products[$order_product]['alias']= $products[$product]['alias'];
                }
            }
        }
        $this->setMeta('Деталі замовлення');
        $this->set(compact('order_products','orde'));
    }


    public function viewAction(){
        $user_id = $this->getRequestID();
        $user = \R::load('user', $user_id);
        $this->setMeta("Профайл №  $user->id");
        $this->set(compact('user'));
    }


    public  function profileAction(){
        if (!User::checkAuth())redirect('/user/login');
        unset($_SESSION['ufilter']);
        if (!empty($_POST)){
            $user = new \app\models\admin\User();
            $data = $_POST;
            $data['id'] = $_SESSION['user']['id'];
            $data['role'] = $_SESSION['user']['role'];
            $data['avatar'] = $_SESSION['user']['avatar'];
            $data['back_avatar'] = $_SESSION['user']['back_avatar'];
            $user->load($data);
            $user->editFilter($_SESSION['user']['id'], $data);
            if (!$user->attributes['password']){
                unset($user->attributes['password']);
            }else{
                $user->attributes['password'] = password_hash($user->attributes['password'], PASSWORD_DEFAULT);
            }
            if (!$user->validate($data) || !$user->checkUnique()){
                $user->getErrors();
                redirect();
            }
            if ($user->update('user', $_SESSION['user']['id'])){
                foreach ($user->attributes as $k => $v){
                    if ($k != 'password') $_SESSION['user'][$k] = $v;
                }
                $alias = AppModel::createAlias('user','alias', $data['name'], $_SESSION['user']['id']);
                $productAlias = \R::load('user', $_SESSION['user']['id']);
                $alias .= '/';
                $productAlias->alias = $alias;
                \R::store($productAlias);
                $_SESSION['success'] = 'Зміни внесені';
            }
            redirect();
        }
        $location = \R::findAll('location');
        $location_servises = \R::findAll('location_servises', 'owner_id = ?', [$_SESSION['user']['id']]);
        foreach ($location_servises as $item => $v){
            foreach ($location as $loc => $l){
                if ($v['location_id'] == $l['id'])
                    $v['location_id'] = $l['name'];
            }
        }
        $type_services = \R::findAll('type_services', 'owner_id = ?', [$_SESSION['user']['id']]);
        $select = \R::findAll('location_servises');
        foreach ($type_services as $item => $v){
            foreach ($select as $loc => $l){
                if ($v['servises_id'] == $l['id'])
                    $v['servises_id'] = $l['title'];
            }
        }
        $filter = \R::getCol('SELECT attr_id FROM u_attribute_product WHERE product_id = ?', [$_SESSION['user']['id']]);
        $pages = \R::getAll("SELECT `page`.`id`,`page`.`title`,`page`.`alias`,`page`.`servises_id`,`page`.`bought_to`,`page`.`monthe_view`,`page`.`price`,`servises`.`title` AS cat FROM `page` JOIN `servises` ON `page`.`servises_id` = `servises`.`id` WHERE `page`.`owner_id` = ?",  [$_SESSION['user']['id']]);
        $servises = \R::findAll('servises', 'owner_id = ?', [$_SESSION['user']['id']]);
        $orders = \R::findAll('order', 'user_id = ?', [$_SESSION['user']['id']]);
        $this->set(compact('orders','pages','servises', 'type_services','location_servises','filter'));
        $this->setMeta('Персональний кабінет');

    }


}