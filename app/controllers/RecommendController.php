<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 18/06/2018
 * Time: 01:12
 */

namespace app\controllers;


use inhouse\libs\Pagination;
use RedBeanPHP\R;

class RecommendController extends AppController {

    public function indexAction(){
        $select = \R::findAll('servises');
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $perpage = 8;
        $count = \R::count('page', "status = '1'");
        $pagination = new Pagination($page, $perpage,$count);
        $start = $pagination->getStart();
        $owners = \R::findAll('user');
        $pages = \R::findAll('page', "status = '1' LIMIT $start, $perpage");
        foreach ($pages as $item => $v){
            foreach ($owners as $owner => $o){
                if ($v['owner_id'] == $o['id'])
                    $v['owner_id'] = $o['name'];
            }
        }
        $owner = \R::findAll('user');
        foreach ($pages as $item => $v){
            foreach ($owner as $user => $u){
                if ($v['owner_id'] == $u['id'])
                    $v['owner_id'] = $u['name'];
            }
        }
        $this->setMeta($pages->title, $pages->description);
        $this->set(compact('pages','select','pagination'));

    }

    public function findAction(){
        $id = $this->getRequestID();
        $select = \R::findAll('servises');
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $perpage = 8;
        $count = \R::count('page',"servises_id = ?", [$id]);
        $pagination = new Pagination($page, $perpage,$count);
        $start = $pagination->getStart();
        $pages = \R::findAll('page', "status = '1' AND servises_id = ? LIMIT $start, $perpage", [$id]);
        $owner = \R::findAll('user');
        foreach ($pages as $item => $v){
            foreach ($owner as $user => $u){
                if ($v['owner_id'] == $u['id'])
                    $v['owner_id'] = $u['name'];
            }
        }
        $this->setMeta($pages->title, $pages->description);
        $this->set(compact('pages','select','pagination','id'));

    }

    public function viewAction(){
        $elias = $this->route['alias'];
        $elias .= '/';
        $servises = \R::findOne('page', 'alias=?',[$elias] );
        $owner = \R::findOne('user', 'id=?',[$servises->owner_id]);
        if (isset($servises)){
            $this->viewPage('page', $servises->id, $servises->bought_to);
            $servises_all = \R::findAll('page', "owner_id = ? AND id != ? LIMIT 10",[$owner->id, $servises->id]);
            if (!$servises_all){
                $servises_anather = \R::findAll('page', "servises_id = ? AND id != ? LIMIT 10",[$servises->servises_id, $servises->id]);
            }
        }else{
            throw new \Exception('Cтраница не найдена', 404);
        }
        $this->setMeta($servises->title, $servises->description);
        $this->set(compact('servises','owner','servises_all','servises_anather'));
    }

}