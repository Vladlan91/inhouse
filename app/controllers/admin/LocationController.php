<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 25/09/2018
 * Time: 21:35
 */

namespace app\controllers\admin;


use app\models\admin\Images;
use app\models\admin\Location;
use app\models\AppModel;
use inhouse\App;
use inhouse\libs\Pagination;
use RedBeanPHP\R;

class LocationController extends AppController {

    public function addAction(){
        if (!empty($_POST)){
            $location = new Location();
            $data = $_POST;
            $month = date("n");
            $location->load($data);
            $location->attributes['monthe'] = $month;
            $location->getImg();
            if (!$location->validate($data)){
                $location->getErrors();
                redirect();
            }
            if ($id = $location->save('location_servises',false)){
                if (empty($location->attributes['alias'])){
                    $alias = AppModel::createAlias('location_servises', 'alias', $data['title'], $id);
                    $cat = R::load('location_servises', $id);
                    $alias .= '/';
                    $cat->alias = $alias;
                    R::store($cat);
                }
                $_SESSION['success'] = 'Послуга створена';
            }
            redirect();
        }
        $select = \R::findAll('servises');
        $locations = \R::findAll('location');
        $users = \R::findAll('user', "role = 'master' OR role = 'builder'");
        $this->setMeta('Нова послуга');
        $this->set(compact('users','select','locations'));
    }

    public function editAction(){
        if (!empty($_POST)){
            $id = $this->getRequestID(false);
            $servises = new Location();
            $data = $_POST;
            $month = date("n");
            $servises->load($data);
            $servises->attributes['monthe'] = $month;
            $servises->getImg();
            if (!$servises->validate($data)){
                $servises->getErrors();
                redirect();
            }
            if ($servises->update('location_servises', $id)){
                $_SESSION['success'] = 'Послуга відкорегована';
            }
            redirect();
        }
        $id = $this->getRequestID();
        $select = \R::findAll('servises');
        $servises = R::load('location_servises',$id);
        $users = \R::findAll('user', "role = 'master' OR role = 'builder'");
        $locations = \R::findAll('location');
        App::$app->setProperty('parent_id', $servises->parent_id);
        $this->setMeta("Редагування послуги {$servises->title}");
        $this->set(compact('servises','users','locations','select'));
    }

    public function listAction(){
        $page = isset($_GET['page']) ? (int)$_GET['page'] : null;
        $perpage = 15;
        $count = \R::count('location_servises');
        $pagination = new Pagination($page, $perpage, $count);
        $start = $pagination->getStart();
        $pages = \R::getAll("SELECT `location_servises`.`id`,`location_servises`.`title`,`location_servises`.`servises_id`,`location_servises`.`bought_to`,`location_servises`.`view`,`location_servises`.`price`,`location_servises`.`owner_id`,`location_servises`.`monthe_view`,`location`.`name` AS town FROM `location_servises` JOIN `location` ON `location_servises`.`location_id` = `location`.`id` LIMIT $start, $perpage");
        $this->setMeta('Cписок сторінок');
        $this->set(compact('pages','pagination', 'count'));
    }

    public function viewAction(){
        $id = $this->getRequestID();
        $servises = R::load('location_servises',$id);
        $type = \R::findAll('type_services', "servises_id = ?",[$servises->id]);
        $location = R::load('location',$servises->location_id);
        $parent_services = R::load('servises',$servises->servises_id);
        $parent = R::load('servises',$parent_services->parent_id);
        $user = R::load('user',$servises->owner_id);
        $this->setMeta("Редагування послуги {$servises->title}");
        $this->set(compact('servises','location','user','parent','count','type'));
    }

    public function addImageAction(){
        if (isset($_GET['upload'])){
            $wmax = App::$app->getProperty('gallery_width');
            $hmax = App::$app->getProperty('gallery_height');
            $name = $_POST['name'];
            $img = new Images();
            $dir = 'location/';
            $img->uploadImg($name, $wmax, $hmax, $dir);
        }
    }
}