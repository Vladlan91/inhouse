<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 01/07/2018
 * Time: 12:29
 */

namespace app\controllers\admin;


use app\models\User;
use inhouse\libs\Pagination;

class UserController extends AppController{


    public function indexAction(){
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $perpage = 20;
        $count = \R::count('user');
        $pagination = new Pagination($page, $perpage,$count);
        $start = $pagination->getStart();
        $users = \R::findAll('user', "LIMIT $start, $perpage");
        $this->setMeta('Користувачі');
        $this->set(compact('users', 'pagination'));
    }

    public function addAction(){
        $this->setMeta(' Створення користувача');
    }

    public function editAction(){
        if (!empty($_POST)){
            $id = $this->getRequestID(false);
            $user = new \app\models\admin\User();
            $data = $_POST;
            $user->load($data);
            if (!$user->attributes['password']){
                unset($user->attributes['password']);
            }else{
                $user->attributes['password'] = password_hash($user->attributes['password'], PASSWORD_DEFAULT);
            }
            if (!$user->validate($data) || !$user->checkUnique()){
                $user->getErrors();
                redirect();
            }
            if ($user->update('user', $id)){
                $_SESSION['success'] = 'Зміни внесені';
            }
            redirect();

        }
        $user_id = $this->getRequestID();
        $user = \R::load('user', $user_id);
        $this->setMeta("Профайл №  $user->id");
        $orders = \R::getAll("SELECT `order`.`id`,`order`.`user_id`,`order`.`status`,`order`.`data`,`order`.`update_at`,`order`.`currency`,`user`.`name`, ROUND(SUM(`order_product`.`price`),2) AS `sum` FROM `order` JOIN `user` ON `order`.`user_id` = `user`.`id` JOIN `order_product` ON `order`.`id` = `order_product`.`order_id` WHERE user_id = {$user_id} GROUP BY `order`.`id` ORDER BY `order`.`status`, `order`.`id`");
        $this->set(compact('user', 'pagination', 'orders'));

    }

    public function loginAdminAction(){
        $this->layout = 'login';
        if (!empty($_POST)){
            $user = new User();
            if ($user->login(true)){
                $_SESSION['success'] = 'Ви успішно авторизовані';
            }else{
                $_SESSION['error'] = 'Логін / Пароль не вірний';
            }
            if (User::isAdmin()){
                redirect(ADMIN);
            }else{
                redirect();
            }

        }
    }

    public function addImageAction(){
        if (isset($_GET['upload'])){
            if ($_POST['name'] == 'single'){
                $wmax = App::$app->getProperty('img_width');
                $hmax = App::$app->getProperty('img_height');
                $dir = 'useravatar/';
            }else{
                $wmax = App::$app->getProperty('gallery_width');
                $hmax = App::$app->getProperty('gallery_height');
                $dir = 'portfolio/';
            }
            $name = $_POST['name'];
            $img = new Images();
            $img->uploadImg($name, $wmax, $hmax, $dir);
        }
    }

}