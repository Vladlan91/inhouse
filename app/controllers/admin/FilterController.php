<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 20/07/2018
 * Time: 15:42
 */

namespace app\controllers\admin;


use app\models\admin\FilterAttr;
use app\models\admin\FilterGroup;

class FilterController extends AppController {

    public function indexAction(){
        $attrs_group = \R::findAll('attribute_group');
        $this->setMeta("Група фільтрів");
        $this->set(compact('attrs_group'));

    }

    public function groupDeleteAction(){
        $id = $this->getRequestID();
        $count = \R::count('attribute_value', 'attr_group_id = ?', [$id]);
        if ($count){
            $_SESSION['error'] = "Видалити не можливо, в фільтрі існують групи!";
            redirect();
        }else{
            \R::exec("DELETE FROM attribute_group WHERE id = ?", [$id]);
            $_SESSION['success'] = "Видалено!";
            redirect();
        }
    }

    public function attrDeleteAction(){
        $id = $this->getRequestID();
        \R::exec("DELETE FROM attribute_product WHERE attr_id = ?", [$id]);
        \R::exec("DELETE FROM attribute_value WHERE id = ?", [$id]);
        $_SESSION['success'] = "Видалено!";
        redirect();
    }

    public function groupEditAction(){
        if (!empty($_POST)) {
            $id = $this->getRequestID(false);
            $group = new FilterGroup();
            $data = $_POST;
            $group->load($data);
            if (!$group->validate($data)) {
                $group->getErrors();
                redirect();
            }
            if ($group->update('attribute_group', $id)) {
                $_SESSION['success'] = 'Зміни добавлені';
                redirect();
            }

        }
        $id = $this->getRequestID();
        $group = \R::load('attribute_group', $id);
        $this->setMeta("Редагування групи");
        $this->set(compact('group'));
    }

    public function attrEditAction(){
        if (!empty($_POST)) {
            $id =  $this->getRequestID(false);
            $attr = new FilterAttr();
            $data = $_POST;
            $attr->load($data);
            if (!$attr->validate($data)) {
                $attr->getErrors();
                redirect();
            }
            if ($attr ->update('attribute_value',$id)) {
                $_SESSION['success'] = 'Атрібут змінено';
                redirect();
            }
        }
        $id =  $this->getRequestID();
        $attr = \R::load('attribute_value', $id);
        $group = \R::findAll('attribute_group');
        $this->setMeta("Редагування атрібуту");
        $this->set(compact('attr','group'));
    }

    public function attrAddAction(){
        if (!empty($_POST)) {
            $attr = new FilterAttr();
            $data = $_POST;
            $attr->load($data);
            if (!$attr->validate($data)) {
                $attr->getErrors();
                redirect();
            }
            if ($attr ->save('attribute_value',false)) {
                $_SESSION['success'] = 'Атрібут добавлений';
                redirect();
            }
        }
        $group = \R::findAll('attribute_group');
        $this->setMeta("Новий фільтрер");
        $this->set(compact('group'));
    }

    public function groupAddAction(){
        if (!empty($_POST)) {
            $group = new FilterGroup();
            $data = $_POST;
            $group->load($data);
            if (!$group->validate($data)) {
                $group->getErrors();
                redirect();
            }
            if ($group->save('attribute_group',false)) {
                $_SESSION['success'] = 'Група добавлена';
                redirect();
            }
        }
        $this->setMeta("Нова група фільтрів");
    }
    public function attrAction(){
        $attrs = \R::getAssoc("SELECT attribute_value.*, attribute_group.title FROM attribute_value JOIN attribute_group ON attribute_group.id = attribute_value.attr_group_id");
        $this->setMeta("Новий фільтр");
        $this->set(compact('attrs'));
    }
}