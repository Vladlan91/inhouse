<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 01/07/2018
 * Time: 08:12
 */

namespace app\controllers\admin;


use app\models\AppModel;
use app\models\User;
use inhouse\base\Controller;

class AppController extends Controller {

    public $layout = 'admin';

    public function __construct($route){
        new AppModel();
        parent::__construct($route);
        if (!User::isAdmin() && $route['action'] != 'login-admin'){
            redirect(ADMIN . '/user/login-admin');
        }

    }

    public function getRequestID($get = true, $id = 'id'){
        if ($get){
            $data = $_GET;
        }else{
            $data = $_POST;
        }
        $id = !empty($data[$id]) ? (int)$data[$id] : null;
        if (!$id){
            throw new \Exception('Сторінка не знайдена', 404);
        }
        return $id;
    }
}