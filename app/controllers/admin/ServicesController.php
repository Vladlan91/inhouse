<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 22/07/2018
 * Time: 21:27
 */

namespace app\controllers\admin;



use app\models\admin\Services;
use app\models\AppModel;
use app\models\Category;
use inhouse\App;
use RedBeanPHP\R;
use inhouse\libs\Pagination;

class ServicesController extends AppController {

    public function indexAction(){
        $this->setMeta('Список послуг');
    }

    public function deleteAction(){
        $id = $this->getRequestID();
        $children = R::count('servises','parent_id = ?', [$id]);
        $errors = '';
        if ($children){
            $errors = '<li>Видалення не можливе, в даній категорії існують потомки!</li>';
        }
        if ($errors){
            $_SESSION['error'] = "<ul>$errors</ul>";
            redirect();
        }
        $category = \R::load('servises',$id);
        R::trash($category);
        $_SESSION['success'] = 'Послуга видалена';
        redirect();
    }

    public function addAction(){
        if (!empty($_POST)){
            $servises = new Services();
            $data = $_POST;
            $month = date("n");
            $servises->load($data);
            $servises->attributes['monthe'] = $month;
            $servises->attributes['status'] = $servises->attributes['status'] ? '1' : '0';
            if (!$servises->validate($data)){
                $servises->getErrors();
                redirect();
            }
            if ($id = $servises->save('servises')){
                if (empty($servises->attributes['alias'])){
                    $alias = AppModel::createAlias('servises', 'alias', $data['title'], $id);
                    $cat = R::load('servises', $id);
                    $alias .= '/';
                    $cat->alias = $alias;
                    R::store($cat);
                }
                $_SESSION['success'] = 'Послуга створена';
            }
            redirect();

        }
        $users = \R::findAll('user', "role = 'master' OR role = 'builder'");
        $this->setMeta('Нова послуга');
        $this->set(compact('users'));
    }

    public function editAction(){
        if (!empty($_POST)){
            $id = $this->getRequestID(false);
            $servises = new Services();
            $data = $_POST;
            $month = date("n");
            $servises->load($data);
            $servises->attributes['monthe'] = $month;
            $servises->attributes['status'] = $servises->attributes['status'] ? '1' : '0';
            if (!$servises->validate($data)){
                $servises->getErrors();
                redirect();
            }
            if ($servises->update('servises', $id)){
                $_SESSION['success'] = 'Послуга відкорегована';
            }
            redirect();
        }
        $id = $this->getRequestID();
        $servises = R::load('servises',$id);
        $users = \R::findAll('user', "role = 'master' OR role = 'builder'");
        App::$app->setProperty('parent_id', $servises->parent_id);
        $this->setMeta("Редагування послуги {$servises->title}");
        $this->set(compact('servises','users'));

    }

    public function listAction(){
        $page = isset($_GET['page']) ? (int)$_GET['page'] : null;
        $perpage = 15;
        $count = \R::count('servises');
        $pagination = new Pagination($page, $perpage, $count);
        $start = $pagination->getStart();
        $pages = \R::getAll("SELECT `servises`.`id`,`servises`.`title`,`servises`.`price`,`servises`.`view`,`servises`.`bought_to`,`servises`.`owner_id`,`servises`.`monthe_view`,`servises`.`status`  FROM `servises` LIMIT $start, $perpage");
        $this->setMeta('Cписок сторінок');
        $this->set(compact('pages','pagination', 'count'));
    }

}