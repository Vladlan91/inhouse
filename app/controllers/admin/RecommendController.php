<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 06/08/2018
 * Time: 08:44
 */

namespace app\controllers\admin;


use app\models\admin\Images;
use app\models\admin\Recommend;
use app\models\AppModel;
use inhouse\App;
use inhouse\libs\Pagination;
use RedBeanPHP\R;
use app\models\admin\Product;

class RecommendController extends AppController {

    public function indexAction(){
        $page = isset($_GET['page']) ? (int)$_GET['page'] : null;
        $perpage = 10;
        $count = \R::count('page');
        $pagination = new Pagination($page, $perpage, $count);
        $start = $pagination->getStart();
        $pages = \R::getAll("SELECT `page`.`id`,`page`.`title`,`page`.`servises_id`,`page`.`bought_to`,`page`.`status`,`page`.`view`,`page`.`price`,`page`.`owner_id`,`page`.`monthe_view`,`servises`.`title` AS cat FROM `page` JOIN `servises` ON `page`.`servises_id` = `servises`.`id` LIMIT $start, $perpage");
        $this->setMeta('Cписок рекомендацій');
        $this->set(compact('pages','pagination', 'count'));
    }

    public function editAction(){
        if (!empty($_POST)){
            $id = $this->getRequestID(false);
            $recommend = new Recommend();
            $data = $_POST;
            $month = date("n");
            $recommend->load($data);
            $recommend->attributes['monthe'] = $month;
            $recommend->getImg();
            $recommend->attributes['status'] = $recommend->attributes['status'] ? '1' : '0';
            if (!$recommend->validate($data)){
                $recommend->getErrors();
                redirect();
            }
            if ($recommend->update('page', $id)){
                $_SESSION['success'] = 'Рекомендація відкорегована';
            }
            redirect();
        }
        $select = \R::findAll('servises');
        $id = $this->getRequestID();
        $recommend = R::load('page',$id);
        $users = \R::findAll('user', "role = 'master' OR role = 'builder'");
        App::$app->setProperty('servises_id', $recommend->parent_id);
        $this->setMeta("Редагування послуги {$recommend->title}");
        $this->set(compact('recommend','users','select'));

    }

    public function addAction(){
        if (!empty($_POST)){
            $recommend = new Recommend();
            $data = $_POST;
            $month = date("n");
            $recommend->load($data);
            $recommend->attributes['status'] = $recommend->attributes['status'] ? '1' : '0';
            $recommend->attributes['monthe'] = $month;
            $recommend->getImg();
            if (!$recommend->validate($data)){
                $recommend->getErrors();
                redirect();
            }
            if ($id = $recommend->save('page')){
                if (empty($recommend->attributes['alias'])){
                    $alias = AppModel::createAlias('page', 'alias', $data['title'], $id);
                    $cat = R::load('page', $id);
                    $alias .= '/';
                    $cat->alias = $alias;
                    R::store($cat);
                }
                $_SESSION['success'] = 'Послуга створена';
            }
            redirect();
        }
        $select = \R::findAll('servises');
        $users = \R::findAll('user', "role = 'master' OR role = 'builder'");
        $this->setMeta('Нова послуга');
        $this->set(compact('users','select'));
    }

    public function addImageAction(){
        if (isset($_GET['upload'])){
            $wmax = App::$app->getProperty('gallery_width');
            $hmax = App::$app->getProperty('gallery_height');
            $name = $_POST['name'];
            $img = new Images();
            $dir = 'recommend/';
            $img->uploadImg($name, $wmax, $hmax, $dir);
        }
    }



}