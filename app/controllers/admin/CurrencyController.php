<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 22/07/2018
 * Time: 16:34
 */

namespace app\controllers\admin;


use app\models\admin\Currency;

class CurrencyController extends AppController {

    public function indexAction(){
        $currency = \R::findAll('currency');
        $this->setMeta('Валюти магазину');
        $this->set(compact('currency'));

    }
    public function addAction(){
        if (!empty($_POST)) {
            $currency = new Currency();
            $data = $_POST;
            $currency->load($data);
            $currency->attributes['base'] = $currency->attributes['base'] ? '1' : '0';
            if (!$currency->validate($data)) {
                $currency->getErrors();
                redirect();
            }
            if ($currency->save('currency')) {
                $_SESSION['success'] = 'Валюта добавлена';
                redirect();
            }
        }
        $this->setMeta("Нова валюта");
    }

    public function deleteAction(){
        $id = $this->getRequestID();
        \R::exec("DELETE FROM currency WHERE id = ?", [$id]);
        $_SESSION['success'] = "Видалено!";
        redirect();
    }

    public function editAction(){
        if (!empty($_POST)) {
            $id =  $this->getRequestID(false);
            $currency = new Currency();
            $data = $_POST;
            $currency->load($data);
            $currency->attributes['base'] = $currency->attributes['base'] ? '1' : '0';
            if (!$currency->validate($data)) {
                $currency->getErrors();
                redirect();
            }
            if ($currency->update('currency', $id)) {
                $_SESSION['success'] = 'Валюта змінена';
                redirect();
            }
        }
        $id =  $this->getRequestID();
        $currency = \R::load('currency', $id);
        $this->setMeta('Редагування валюти');
        $this->set(compact('currency'));
    }

}