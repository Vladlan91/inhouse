<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 01/07/2018
 * Time: 14:28
 */

namespace app\controllers\admin;


use inhouse\libs\Pagination;

class OrderController extends AppController {
    public $order_id;

    public function indexAction(){
        $page = isset($_GET['page']) ? (int)$_GET['page'] : null;
        $perpage = 5;
        $count = \R::count('order');
        $pagination = new Pagination($page, $perpage, $count);
        $start = $pagination->getStart();
        $orders = \R::getAll("SELECT `order`.`id`,`order`.`user_id`,`order`.`status`,`order`.`data`,`order`.`update_at`,`order`.`currency`,`user`.`name`, ROUND(SUM(`order_product`.`price`),2) AS `sum` FROM `order` JOIN `user` ON `order`.`user_id` = `user`.`id` JOIN `order_product` ON `order`.`id` = `order_product`.`order_id` GROUP BY `order`.`id` ORDER BY `order`.`status`, `order`.`id` LIMIT $start, $perpage");
        $this->setMeta('Cписок замовлень');
        $this->set(compact('orders','pagination', 'count'));
    }

    public function viewAction(){
        $order_id = $this->getRequestID();
        $orde = \R::getRow("SELECT `order`.*,`user`.`name`, ROUND(SUM(`order_product`.`price`),2) AS `sum` FROM `order` JOIN `user` ON `order`.`user_id` = `user`.`id` JOIN `order_product` ON `order`.`id` = `order_product`.`order_id` WHERE `order`.`id` = ? GROUP BY `order`.`id` ORDER BY `order`.`status`, `order`.`id` LIMIT 1", [$order_id]);
           if ($orde){
               $order_products = \R::findAll("order_product", "order_id = ?",[$order_id]);
           }
        $products = \R::findAll('product');
        foreach ($order_products as $order_product => $v){
            foreach ($products as $product => $v){
                if ($order_products[$order_product]['product_id'] == $products[$product]['id'])
                    $order_products[$order_product]['img']= $products[$product]['img'];
                }
        }

        $this->setMeta("Замовлення № $order_id від {$orde['name']}");
        $this->set(compact('order_id','orde','order_products'));
    }

    public function changeAction(){
        $order_id = $this->getRequestID();
        $orde = \R::getRow("SELECT `order`.*,`user`.`name`, ROUND(SUM(`order_product`.`price`),2) AS `sum` FROM `order` JOIN `user` ON `order`.`user_id` = `user`.`id` JOIN `order_product` ON `order`.`id` = `order_product`.`order_id` WHERE `order`.`id` = ? GROUP BY `order`.`id` ORDER BY `order`.`status`, `order`.`id` LIMIT 1", [$order_id]);
        if ($orde){
            $order_products = \R::findAll("order_product", "order_id = ?",[$order_id]);
        }
        $products = \R::findAll('product');
        foreach ($order_products as $order_product => $v){
            foreach ($products as $product => $v){
                if ($order_products[$order_product]['product_id'] == $products[$product]['id'])
                    $order_products[$order_product]['img']= $products[$product]['img'];
            }
        }
        $roll = "'master', 'builder'";
        $users  = \R::find('user', "role IN ($roll)");
        $this->setMeta("Замовлення № $order_id від {$orde['name']}");
        $this->set(compact('order_id','orde','order_products','users'));
    }

    public function changedAction(){
        $order_id = $_SESSION['order_id_change'];
        $status = isset($_GET['opt']) ? $_GET['opt'] : 0;
        $order = \R::load('order',$order_id);
        if (!$order){
            throw new \Exception('Сторінка не знайдена', 404);
        }
        $order->status = $status;
        $order->update_at = date("Y-m-d H:i:s");
        \R::store($order);
        redirect();

    }

    public function deleteAction(){
        $order_id = $this->getRequestID();
        $order = \R::load('order',$order_id);
        \R::trash($order);
        unset($_SESSION['order_id_change']);
        $_SESSION['success'] = 'Обране замовлення видалено!';
        redirect(ADMIN . '/order');
    }
}