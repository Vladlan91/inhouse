<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 07/07/2018
 * Time: 10:04
 */

namespace app\controllers\admin;


use app\models\AppModel;
use app\models\Category;
use inhouse\App;
use RedBeanPHP\R;

class CategoryController extends AppController {

    public function indexAction(){
        $this->setMeta('Список категорій');
    }

    public function deleteAction(){
        $id = $this->getRequestID();
        $children = R::count('category','parent_id = ?', [$id]);
        $errors = '';
        if ($children){
            $errors = '<li>Видалення не можливе, в даній категорії існують потомки!</li>';
        }
        $product = R::count('product','category_id = ?', [$id]);
        if ($product){
            $errors .= '<li>Видалення не можливе, в даній категорії існують товари!</li>';
        }
        if ($errors){
            $_SESSION['error'] = "<ul>$errors</ul>";
            redirect();
        }
        $category = R::load('category',$id);
        R::trash($category);
        $_SESSION['success'] = 'Категорія видалена';
        redirect();
    }

    public function addAction(){
        if (!empty($_POST)){
            $category = new Category();
            $data = $_POST;
            $category->load($data);
            if (!$category->validate($data)){
                $category->getErrors();
                redirect();
            }
            if ($id = $category->save('category')){
                $alies = AppModel::createAlias('category', 'alies', $data['title'], $id);
                $cat = R::load('category', $id);
                $cat->alies = $alies;
                R::store($cat);
                $_SESSION['success'] = 'Категорія створена';
            }
            redirect();

        }
        $this->setMeta('Нова категорія');
    }

    public function editAction(){
        if (!empty($_POST)){
            $id = $this->getRequestID(false);
            $category = new Category();
            $data = $_POST;
            $category->load($data);
            if (!$category->validate($data)){
                $category->getErrors();
                redirect();
            }
            if ($category->update('category', $id)){
                $alies = AppModel::createAlias('category', 'alies', $data['title'], $id);
                $category = R::load('category', $id);
                $category->alies = $alies;
                R::store($category);
                $_SESSION['success'] = 'Категорія відкорегована';
            }
            redirect();
        }
        $id = $this->getRequestID();
        $category = R::load('category',$id);
        App::$app->setProperty('parent_id', $category->parent_id);
        $this->setMeta("Редагування категорії {$category->title}");
        $this->set(compact('category'));

    }

}