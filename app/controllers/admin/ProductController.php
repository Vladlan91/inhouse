<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 13/07/2018
 * Time: 17:18
 */

namespace app\controllers\admin;


use app\models\admin\Images;
use app\models\admin\Product;
use app\models\AppModel;
use inhouse\App;
use inhouse\libs\Pagination;

class ProductController extends AppController {

    public function indexAction(){
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $perpage = 12;
        $count = \R::count('product');
        $pagination = new Pagination($page, $perpage,$count);
        $start = $pagination->getStart();
        $products = \R::getAll("SELECT product.*, category.title AS cat, brand.title AS brand FROM product JOIN category ON category.id = product.category_id JOIN brand ON brand.id = product.brand_id LIMIT $start, $perpage");
        $this->setMeta('Товари');
        $this->set(compact('products', 'pagination'));
    }

    public function addAction(){
        if (!empty($_POST)){
            $data = $_POST;
            $product = new Product();
            $modific = $_POST['modification'];
            $mod = $product->getMod($modific);
            $product->load($data);
            $product->attributes['hit'] = $product->attributes['hit'] ? '1' : '0';
            $product->attributes['status'] = $product->attributes['status'] ? '1' : '0';
            $product->getImg();
            if (!$product->validate($data)){
                $product->getErrors();
                $_SESSION['form_data'] = $data;
                redirect();
            }
            if ($id = $product->save('product')){
                $product->saveGallery($id);
                $product->saveMod($mod,$id);
                $alias = AppModel::createAlias('product','alias', $data['title'],$id);
                $productAlias = \R::load('product', $id);
                $productAlias->alias = $alias;
                \R::store($productAlias);
                $product->editFilter($id,$data);
                $product->editRelated($id,$data);
                $_SESSION['success']= "Товар створено успішно";
            }
            redirect();
        }
        $brands = \R::findAll('brand');
        $this->setMeta('Новий товар');
        $this->set(compact('brands'));
    }

    public function editAction(){
        if (!empty($_POST)){
            $id = $this->getRequestID(false);
            $product = new Product();
            $data = $_POST;
            $modific = $_POST['modification'];
            $mod = $product->getMod($modific);
            $product->load($data);
            $product->attributes['hit'] = $product->attributes['hit'] ? '1' : '0';
            $product->attributes['status'] = $product->attributes['status'] ? '1' : '0';
            $product->getImg();
            if (!$product->validate($data)){
                $product->getErrors();
                redirect();
            }
            if ($product->update('product', $id)){
                $product->saveMod($mod,$id);
                $product->saveGallery($id);
                $product->editFilter($id,$data);
                $product->editRelated($id,$data);
                $alias = AppModel::createAlias('product','alias', $data['title'],$id);
                $productAlias = \R::load('product', $id);
                $productAlias->alias = $alias;
                \R::store($productAlias);
                $_SESSION['success']= "Зміни в товарі збережено!";
                redirect();
            }

        }

        $id = $this->getRequestID();
        $product = \R::load('product', $id);
        //$product_brand = \R::getRow("SELECT `product`.brand_id,`brand`.`title` `brand_title` FROM `product` JOIN `brand` ON `product`.`brand_id` = `brand`.`id`  WHERE `product`.`id` = ?", [$id]);
        App::$app->setProperty('parent_id', $product->category_id);
        $filter = \R::getCol('SELECT attr_id FROM attribute_product WHERE product_id = ?', [$id]);
        $related_product = \R::getAll("SELECT related_product.related_id, product.title FROM related_product JOIN product ON product.id = related_product.related_id WHERE related_product.product_id = ?", [$id]);
        $gallery = \R::getCol('SELECT img FROM gallery WHERE product_id = ?', [$id]);
        $modifications = \R::findAll('modification', 'product_id = ?',[$id] );
        $brands = \R::findAll('brand');
        $this->setMeta("Редагування товару № $id");
        $this->set(compact('product', 'gallery', 'related_product', 'filter','modifications','brands'));

    }

    public function addImageAction(){
        if (isset($_GET['upload'])){
            if ($_POST['name'] == 'single'){
                $wmax = App::$app->getProperty('img_width');
                $hmax = App::$app->getProperty('img_height');
                $dir = 'productavatar/';
            }else{
                $wmax = App::$app->getProperty('gallery_width');
                $hmax = App::$app->getProperty('gallery_height');
                $dir = 'productgallery/';
            }
            $name = $_POST['name'];
            $img = new Images();
            $img->uploadImg($name, $wmax, $hmax, $dir);
        }
    }

    public function relatedProductAction(){
        $q = isset($_GET['q']) ? $_GET['q'] : null;
        $data['items'] = [];
        $products = \R::getAssoc('SELECT id,title FROM product WHERE title LIKE ? LIMIT 5', ["%{$q}%"]);
       if ($products){
           $i = 0;
           foreach ($products as $id => $title){
               $data['items'][$i]['id'] = $id;
               $data['items'][$i]['text'] = $title;
               $i++;
           }
       }
        echo json_encode($data);
       die;
    }

    public function deleteGalleryAction(){
        $id = isset($_POST['id']) ? $_POST['id'] : null;
        $src = isset($_POST['src']) ? $_POST['src'] : null;
        if (!$id || !$src){
            return;
        }
        if (\R::exec("DELETE FROM gallery WHERE product_id = ? AND img = ?", [$id, $src])){
            @unlink(WWW . "/images/$src");
            exit('1');
        }
        return;
    }

    public function deleteAction(){
        $id = $this->getRequestID();
        $product = \R::load('product', $id);
        \R::trash($product);
        \R::exec("DELETE FROM gallery WHERE product_id = ?", [$id]);
        \R::exec("DELETE FROM modification WHERE product_id = ?", [$id]);
        \R::exec("DELETE FROM related_product WHERE product_id = ?", [$id]);
        $_SESSION['success'] = 'Обраний товар видалено!';
        redirect(ADMIN . '/product');
    }
}

