<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 20/07/2018
 * Time: 17:15
 */

namespace app\controllers\admin;


use app\models\admin\Brand;
use app\models\admin\Images;
use app\models\admin\Img;
use app\models\AppModel;
use inhouse\App;
use inhouse\libs\Pagination;
use RedBeanPHP\R;

class BrandController extends AppController {

    public function indexAction(){
    $page = isset($_GET['page']) ? $_GET['page'] : 1;
    $perpage = 10;
    $count = \R::count('brand');
    $pagination = new Pagination($page, $perpage,$count);
    $start = $pagination->getStart();
    $brands = \R::findAll('brand', "LIMIT $start, $perpage");
    $this->setMeta('Бренди');
    $this->set(compact('brands', 'pagination'));
    }

    public function editAction(){
        if (!empty($_POST)){
            $id = $this->getRequestID(false);
            $type = new Brand();
            $data = $_POST;
            $type->load($data);
            $type->getImg();
            if (!$type->validate($data)){
                $type->getErrors();
                redirect();
            }
            if ($type->update('brand', $id)){
                $alias = AppModel::createAlias('brand','alias', $data['title'],$id);
                $productAlias = \R::load('brand', $id);
                $alias .= '/';
                $productAlias->alias = $alias;
                \R::store($productAlias);
                $_SESSION['success'] = 'Виробника відкореговано';
            }
            redirect();
        }
        $id = $this->getRequestID();
        $brand = \R::load('brand',$id);
        $this->setMeta("Редагування послуги {$brand->title}");
        $this->set(compact('brand'));
    }

    public function addAction(){
        if (!empty($_POST)){
            $data = $_POST;
            $type = new Brand();
            $type->load($data);
            $type->getImg();
            if (!$type->validate($data)){
                $type->getErrors();
                redirect();
            }
            if ($id = $type->save('brand')){
                $alias = AppModel::createAlias('brand','alias', $data['title'],$id);
                $productAlias = \R::load('brand', $id);
                $alias .= '/';
                $productAlias->alias = $alias;
                \R::store($productAlias);
                $_SESSION['success']= "Виробника створено успішно";
            }
            redirect();
        }
        $this->setMeta('Новий товар');

    }

    public function addImageAction(){
        if (isset($_GET['upload'])){
            if ($_POST['name'] == 'img'){
                $wmax = App::$app->getProperty('brand_img_width');
                $hmax = App::$app->getProperty('brand_img_height');
                $dir = 'brands/';
            }else{
                $wmax = App::$app->getProperty('brand_gallery_width');
                $hmax = App::$app->getProperty('brand_gallery_height');
                $dir = 'brands/';
            }
            $name = $_POST['name'];
            $img = new Img();
            $img->uploadImg($name, $wmax, $hmax, $dir);
        }
    }
}
