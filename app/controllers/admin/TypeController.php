<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 28/09/2018
 * Time: 14:57
 */

namespace app\controllers\admin;


use app\models\admin\Images;
use app\models\admin\Type;
use app\models\AppModel;
use inhouse\App;
use inhouse\libs\Pagination;
use RedBeanPHP\R;

class TypeController extends AppController {

    public function addAction(){
        if (!empty($_POST)){
            $type = new Type();
            $data = $_POST;
            $month = date("n");
            $type->load($data);
            $type->attributes['monthe'] = $month;
            $type->getImg();
            if (!$type->validate($data)){
                $type->getErrors();
                redirect();
            }
            if ($id = $type->save('type_services',false)){
                if (empty($type->attributes['alias'])){
                    $alias = AppModel::createAlias('page', 'alias', $data['title'], $id);
                    $cat = R::load('type_services', $id);
                    $alias .= '/';
                    $cat->alias = $alias;
                    R::store($cat);
                }
                $_SESSION['success'] = 'Послуга створена';
            }
            redirect();
        }
        $location = \R::findAll('location');
        $select = \R::findAll('location_servises');
        foreach ($select as $item => $v){
            foreach ($location as $loc => $l){
                if ($v['location_id'] == $l['id'])
                    $v['location_id'] = $l['name'];
            }
        }
        $users = \R::findAll('user', "role = 'master' OR role = 'builder'");
        $this->setMeta('Нова послуга');
        $this->set(compact('users','select'));
    }

    public function editAction(){
        if (!empty($_POST)){
            $id = $this->getRequestID(false);
            $type = new Type();
            $data = $_POST;
            $month = date("n");
            $type->load($data);
            $type->attributes['monthe'] = $month;
            $type->getImg();
            if (!$type->validate($data)){
                $type->getErrors();
                redirect();
            }
            if ($type->update('type_services', $id)){
                $_SESSION['success'] = 'Тип послуги відкореговано';
            }
            redirect();
        }
        $id = $this->getRequestID();
        $location = \R::findAll('location');
        $select = \R::findAll('location_servises');
        foreach ($select as $item => $v){
            foreach ($location as $loc => $l){
                if ($v['location_id'] == $l['id'])
                    $v['location_id'] = $l['name'];
            }
        }
        $servises = \R::load('type_services',$id);
        $users = \R::findAll('user', "role = 'master' OR role = 'builder'");
        App::$app->setProperty('parent_id', $servises->parent_id);
        $this->setMeta("Редагування послуги {$servises->title}");
        $this->set(compact('servises','users','select'));
    }

    public function listAction(){
        $page = isset($_GET['page']) ? (int)$_GET['page'] : null;
        $perpage = 15;
        $count = \R::count('type_services');
        $pagination = new Pagination($page, $perpage, $count);
        $start = $pagination->getStart();
        $pages = \R::getAll("SELECT `type_services`.`id`,`type_services`.`title`,`type_services`.`servises_id`,`type_services`.`bought_to`,`type_services`.`view`,`type_services`.`price`,`type_services`.`owner_id`,`type_services`.`monthe_view`,`location_servises`.`title` AS type FROM `type_services` JOIN `location_servises` ON `type_services`.`servises_id` = `location_servises`.`id` LIMIT $start, $perpage");
        $this->setMeta('Cписок сторінок');
        $this->set(compact('pages','pagination', 'count'));
    }

    public function addImageAction(){
        if (isset($_GET['upload'])){
            $wmax = App::$app->getProperty('gallery_width');
            $hmax = App::$app->getProperty('gallery_height');
            $name = $_POST['name'];
            $img = new Images();
            $dir = 'type/';
            $img->uploadImg($name, $wmax, $hmax, $dir);
        }
    }

}