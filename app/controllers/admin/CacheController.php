<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 07/07/2018
 * Time: 01:40
 */

namespace app\controllers\admin;


use inhouse\Cash;

class CacheController extends AppController {

    public function indexAction(){

        $this->setMeta('Очищення кешу');
    }

    public function deleteAction(){
        $key = isset($_GET['key']) ? $_GET['key'] : null;
        $cache = Cash::instance();
        switch ($key){
            case 'filter':
                $cache->delete('filter_group');
                $cache->delete('filter_attrs');
                break;
            case 'servises':
                $cache->delete('inhouse_menu_ser');
                $cache->delete('inhouse_admin_cat');
                $cache->delete('inhouse_menu_ser_site');
                break;
            case 'category':
                $cache->delete('cats');
                $cache->delete('inhouse_menu');
                $cache->delete('admin_cat');
                $cache->delete('select_cat');
                break;
        }
        $_SESSION['success'] = 'Обраний кеш видалений!';
        redirect(ADMIN . '/cache');
    }
}