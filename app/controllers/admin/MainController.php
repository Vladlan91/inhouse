<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 01/07/2018
 * Time: 08:11
 */

namespace app\controllers\admin;


class MainController extends AppController {

    public function indexAction(){
        $newOrder = \R::count('order', "status = '0'");
        $allOrder = \R::count('order');
        $order = $newOrder/$allOrder * 100;
        $order = (int)$order;
        $countUser = \R::count('user');
        $countProduct = \R::count('product');
        $countCategory = \R::count('category');
        $this->setMeta('InHouse Aдмін панель');
        $this->set(compact('order','countUser', 'countProduct', 'countCategory'));
    }
}