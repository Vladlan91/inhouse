<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 26/10/2018
 * Time: 17:15
 */

namespace app\controllers\admin;


use inhouse\libs\Pagination;

class MessedgeController extends AppController
{
    public function messedgAction(){
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $perpage = 20;
        $count = \R::count('contact');
        $pagination = new Pagination($page, $perpage,$count);
        $start = $pagination->getStart();
        $massedge = \R::findAll('contact', "LIMIT $start, $perpage");
        $this->setMeta('Бренди');
        $this->set(compact('massedge', 'pagination'));
    }

    public function franchizAction(){
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $perpage = 20;
        $count = \R::count('more');
        $pagination = new Pagination($page, $perpage,$count);
        $start = $pagination->getStart();
        $massedge = \R::findAll('more', "LIMIT $start, $perpage");
        $this->setMeta('Бренди');
        $this->set(compact('massedge', 'pagination'));
    }

}