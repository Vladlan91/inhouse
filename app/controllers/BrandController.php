<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 15/10/2018
 * Time: 11:22
 */

namespace app\controllers;


use inhouse\App;
use inhouse\libs\Pagination;

class BrandController extends AppController
{
    public function indexAction(){
        $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
        $perpage = 8;
        $total = \R::count('brand');
        $pagination = new Pagination($page, $perpage, $total);
        $start = $pagination->getStart();
        $brands = \R::findAll('brand', "LIMIT $start, $perpage");
        $this->set(compact('brands', 'pagination'));


    }

    public function viewAction(){
        $elias = $this->route['alias'];
        $elias .= '/';
        $brands = \R::findOne('brand', 'alias=?',[$elias] );
        $products = \R::findAll('product', 'brand_id = ?', [$brands->id]);
        $hits = \R::findAll('product', "hit = '1' AND brand_id = ? LIMIT 8", [$brands->id]);
        $this->setMeta($brands->title, $brands->description);
        $this->set(compact('brands','products','hits'));
    }
}