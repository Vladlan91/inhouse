<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 17/07/2018
 * Time: 16:26
 */

namespace app\controllers;


use inhouse\libs\Pagination;
use RedBeanPHP\R;

class ServicesController extends AppController {


    public function indexAction(){
        $elias = $this->route['alias'];
        $elias .= '/';
        $servises = \R::findOne('servises', 'alias=?',[$elias] );
        $owner = \R::findOne('user', 'id=?',[$servises->owner_id]);
        if (isset($servises)){
            $this->viewPage('servises', $servises->id, $servises->bought_to);
        }else{
            throw new \Exception('Cтраница не найдена', 404);
        }
        $this->setMeta($servises->title, $servises->description);
        $this->set(compact('servises','owner'));
    }


    public function locationAction(){
        $select = \R::findAll('servises');
        $location = \R::findAll('location');
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $perpage = 8;
        $count = \R::count('location_servises');
        $pagination = new Pagination($page, $perpage,$count);
        $start = $pagination->getStart();
        $pages = \R::findAll('location_servises', " LIMIT $start, $perpage");
        foreach ($pages as $item => $v){
            foreach ($location as $loc => $l){
                if ($v['location_id'] == $l['id'])
                    $v['location_id'] = $l['name'];
            }
        }
        $owner = \R::findAll('user');
        foreach ($pages as $item => $v){
            foreach ($owner as $user => $u){
                if ($v['owner_id'] == $u['id'])
                    $v['owner_id'] = $u['name'];
            }
        }
        $this->setMeta($pages->title, $pages->description);
        $this->set(compact('pages','select','pagination','location'));

    }
}