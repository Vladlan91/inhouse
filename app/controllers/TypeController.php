<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 28/09/2018
 * Time: 15:21
 */

namespace app\controllers;


class TypeController extends AppController {

    public function indexAction(){
        $elias = $this->route['alias'];
        $elias .= '/';
        $servises = \R::findOne('type_services', 'alias=?',[$elias] );
        $parent = \R::findOne('location_servises', 'id=?',[$servises->servises_id] );
        $location = \R::findOne('location', 'id= ?',[$parent->location_id]);
        $owner = \R::findOne('user', 'id=?',[$servises->owner_id]);
        //$type_all = \R::findAll('type_services', "id != ? AND servises_id = ",[$servises->id, $servises->servises_id ]);
        $type_all = \R::findAll('type_services', "servises_id = ? AND id != ? LIMIT 10",[$servises->servises_id, $servises->id]);
        if (isset($servises)){
            $this->viewPage('type_services', $servises->id, $servises->bought_to);
        }else{
            throw new \Exception('Cтраница не найдена', 404);
        }
        $this->setMeta($servises->title, $servises->description);
        $this->set(compact('servises','owner','location','type_all'));
    }
}