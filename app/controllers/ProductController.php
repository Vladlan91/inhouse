<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 07/06/2018
 * Time: 13:47
 */

namespace app\controllers;


use app\models\Breadcrumbs;
use app\models\Product;
use inhouse\App;

class ProductController extends AppController {

    public function viewAction(){
        $elias = $this->route['alias'];
        $product = \R::findOne('product', "alias=? AND status = '1'",[$elias] );
        if (!$product){
            throw new \Exception('Cтраница не найдена', 404);
        }
        $breadcrumbs = Breadcrumbs::getBreadcrumbs($product->category_id, $product->title);
        $related = \R::getAll("SELECT * FROM related_product JOIN product ON product.id = related_product.related_id WHERE related_product.product_id = ?", [$product->id]);
        $brand = \R::findOne('brand', 'id = ?',[$product->brand_id] );
        $product['brend'] = $brand;
        $p_model = new Product();
        $p_model->setRecentlyViewed($product->id);
        $r_model = $p_model->getRecentlyViewed();
        $recentlyViewed = null;
        if ($r_model){
            $recentlyViewed  = \R::find('product', 'id IN ('.\R::genSlots($r_model).') LIMIT 3', $r_model);
        }
        $gallery = \R::findAll("gallery", 'product_id =?', [$product->id]);
        $this->setMeta($product->title, $product->description, $product->keywords);
        $modificat = \R::findAll('modification', 'product_id = ?', [$product->id]);
        $cats = App::$app->getProperty('cats');
        $this->set(compact('product','cats','related','gallery','recentlyViewed','breadcrumbs','modificat','brand'));
    }


}