<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 19/06/2018
 * Time: 08:33
 */

namespace app\controllers;


class SearchController extends AppController {

    public function typeaheadAction(){
        if ($this->isAjax()){
            $query = !empty(trim($_GET['query'])) ? trim($_GET['query']) : null;
            if ($query){
                $products = \R::getdAll("SELECT id, title FROM product WHERE title LIKE ? AND status = '1' LIMIT 10", ["%{$query}%"]);
                echo json_encode($products);
            }
        }
        die;

    }
    public function indexAction(){
        $query = !empty(trim($_GET['s'])) ? trim($_GET['s']) : null;
        if ($query){
            $products = \R::find('product',"title LIKE ? AND status ='1'", ["%{$query}%"]);
            $this->setMeta('Пошук - ' . h($query) );
        $this->set(compact('products','query'));
    }
    }

}