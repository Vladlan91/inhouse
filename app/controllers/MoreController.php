<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 26/10/2018
 * Time: 11:47
 */

namespace app\controllers;


use app\models\More;

class MoreController extends AppController
{

    public function franchizaAction(){

    }

    public function addAction(){
        if (!empty($_POST)){
            $more = new More();
            $data = $_POST;
            $more->load($data);
            $more->attributes['date'] = date('Y-m-d');
            if ($more->save('more')){
                $_SESSION['success'] = 'Ваше повідомлення надіслано успішно, очікуйте на відповідь!';
                $_SESSION['more'] = $more->attributes;
               // More::mailOrder($more->attributes['email']);
            }
            else{
                $_SESSION['error'] = 'Виникла помилка, надішліть ваше повідомлення повторно!';
            }
            redirect();
        }
    }

}