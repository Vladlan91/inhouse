<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 22/09/2018
 * Time: 15:35
 */

namespace app\controllers;


use inhouse\libs\Pagination;

class LocationController extends AppController {


    public function findAction(){
        $select = \R::findAll('servises');
        $location = \R::findAll('location');
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $perpage = 8;
        $count = \R::count('location_servises');
        $pagination = new Pagination($page, $perpage,$count);
        $start = $pagination->getStart();
        $data = $_POST;
        if ($data['servisec'] != '0' &&  $data['location'] != '0' ){
            $pages = \R::findAll('location_servises', "servises_id = ? AND location_id = ? LIMIT $start, $perpage",[$data['servisec'], $data['location']]);
        }elseif ($data['servisec'] == '0' &&  $data['location'] == '0' ){
            $pages = \R::findAll('location_servises', " LIMIT $start, $perpage");
        }elseif ($data['servisec'] == '0' ){
            $pages = \R::findAll('location_servises', "location_id = ?  LIMIT $start, $perpage",[$data['location']]);
        }elseif ($data['location'] == '0' ){
            $pages = \R::findAll('location_servises', "servises_id = ?  LIMIT $start, $perpage",[$data['servisec']]);
        }
        foreach ($pages as $item => $v){
            foreach ($location as $loc => $l){
                if ($v['location_id'] == $l['id'])
                    $v['location_id'] = $l['name'];
            }
        }
        $owner = \R::findAll('user');
        foreach ($pages as $item => $v){
            foreach ($owner as $user => $u){
                if ($v['owner_id'] == $u['id'])
                    $v['owner_id'] = $u['name'];
            }
        }
        $this->setMeta($pages->title, $pages->description);
        $this->set(compact('pages','select','pagination','location', 'data'));

    }

    public function viewAction(){
        $elias = $this->route['alias'];
        $elias .= '/';
        $servises = \R::findOne('location_servises', 'alias=?',[$elias] );
        $location = \R::findOne('location', 'id= ?',[$servises->location_id]);
        $type = \R::findAll('type_services', 'servises_id= ?',[$servises->id]);
        $owner = \R::findOne('user', 'id=?',[$servises->owner_id]);
        if (isset($servises)){
            $this->viewPage('location_servises', $servises->id, $servises->bought_to);
        }else{
                throw new \Exception('Cтраница не найдена', 404);
        }
        $this->setMeta($servises->title, $servises->description);
        $this->set(compact('servises','owner','location','type'));
    }
}