<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 06/08/2018
 * Time: 02:57
 */

namespace app\controllers;
use inhouse\libs\Pagination;
use RedBeanPHP\R;

class CalculatorController extends AppController{

    public function indexAction(){
        $select = R::findAll('servises', "parent_id = '5'");
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $perpage = 4;
        $count = R::count('page', "status = '1'");
        $pagination = new Pagination($page, $perpage,$count);
        $start = $pagination->getStart();
        $pages = R::findAll('page', "status = '1' LIMIT $start, $perpage");
        $this->setMeta($pages->title, $pages->description);
        $this->set(compact('pages','select','pagination'));

    }

}