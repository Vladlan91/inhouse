<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 19/06/2018
 * Time: 20:59
 */

namespace app\controllers;


use app\models\Breadcrumbs;
use app\models\Category;
use app\widgets\filter\Filter;
use inhouse\App;
use inhouse\libs\Pagination;

class CategoryController extends AppController {

    public function viewAction()
    {
        //unset($_SESSION['filter']);
        $alies = $this->route['alias'];
        $category = \R::findOne('category', 'alies=?', [$alies]);
        if (!$category) {
            throw new \Exception('Cтраница не найдена', 404);
        }
        $breadcrumbs = Breadcrumbs::getBreadcrumbs($category->id);
        $cat_model = new Category();
        $ids = $cat_model->getId($category->id);
        $ids = !$ids ? $category->id : $ids . $category->id;
        $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
        $perpage = App::$app->getProperty('pagination');
        $sql_part = '';
        if (!empty($_GET['filter'])) {
            $filters = Filter::getFilter();
            if ($filters){
                $cnt = Filter::getCountGroups($filters);
                $sql_part = "AND id IN (SELECT product_id FROM attribute_product WHERE attr_id IN ($filters) GROUP BY product_id HAVING COUNT(product_id) = $cnt )";
            }
            //$sql_part = "AND id IN (SELECT product_id FROM attribute_product WHERE attr_id IN ($filters))";
        }
        $total = \R::count('product', "category_id IN ($ids) $sql_part");
        $pagination = new Pagination($page, $perpage, $total);
        $start = $pagination->getStart();
        $products = \R::find('product', "status = '1' AND category_id IN ($ids) $sql_part LIMIT $start, $perpage");
        if ($this->isAjax()) {

            Filter::setFilter();
            $this->loadView('filter', compact('products', 'pagination'));


        }
        $this->setMeta($category->title, $category->description, $category->keywords);
        $this->set(compact('products', 'breadcrumbs', 'pagination'));


    }

}
