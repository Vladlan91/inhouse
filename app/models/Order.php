<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 23/06/2018
 * Time: 13:41
 */

namespace app\models;


use inhouse\App;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class Order extends AppModel {

    public $attributes = [

        'user_id' => '',
        'status' => '0',
        'currency' => '',
        //'note' => '',

    ];

    public static function saveOrder($data){

            $order = new Order();
            $order->load($data);
            $order_id = $order->save('order');
            self::saveOrderProduct($order_id);
        return $order_id;
    }

    public static function saveOrderProduct($order_id){
        $sql_part = '';
        foreach ($_SESSION['cart'] as $product_id => $product){
            $product_id = (int)$product_id;
            $sql_part .= "($order_id, $product_id, {$product['qty']}, '{$product['title']}', {$product['price']}, '{$product['brand_title']}', '{$product['brand_alias']}'),";
        }
        $sql_part = rtrim($sql_part,',');
       \R::exec("INSERT INTO order_product (order_id, product_id, qty, title, price, brand_title, brand_alias) VALUES $sql_part");
    }

    public static function mailOrder($order_id, $user_email){

        $transport = (new Swift_SmtpTransport(App::$app->getProperty('smtp_host'), App::$app->getProperty('smtp_port'), App::$app->getProperty('smtp_protocol')))
            ->setUsername(App::$app->getProperty('smtp_login'))
            ->setPassword(App::$app->getProperty('smtp_password'));


        $mailer = new Swift_Mailer($transport);

        ob_start();
        require APP . '/views/Mail/mail_order.php';
        $body = ob_get_clean();


        $message_client = (new Swift_Message("Ваше замовлення зареєстровано під №{$order_id}"))
            ->setFrom([App::$app->getProperty('smtp_login') => App::$app->getProperty('shop_name')])
            ->setTo($user_email)
            ->setBody($body,'text/html')
        ;

        $message_admin = (new Swift_Message("Ваше замовлення зареєстровано під №{$order_id}"))
            ->setFrom([App::$app->getProperty('smtp_login') => App::$app->getProperty('shop_name')])
            ->setTo(App::$app->getProperty('admin_email'))
            ->setBody($body,'text/html')
        ;


        $result = $mailer->send($message_client);
        $result = $mailer->send($message_admin);

        unset($_SESSION['cart.sum']);
        unset($_SESSION['cart.qty']);
        unset($_SESSION['cart']);
        unset($_SESSION['cart.currency']);
        $_SESSION['success'] = "Команда InHouse дякує Вам за замовлення, очікуйте на телефонний звязок нашого менеджера, для уточнення і підтверження інформації." ;
    }



}