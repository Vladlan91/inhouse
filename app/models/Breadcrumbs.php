<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 14/06/2018
 * Time: 11:19
 */

namespace app\models;


use inhouse\App;

class Breadcrumbs {

    public static function getBreadcrumbs($category_id, $name = ''){
        $cats = App::$app->getProperty('cats');
        $bread_array = self::getParts($cats, $category_id);
        $breadcrumbs = "<li><a href='/'>Головна</a></li>";
        if ($bread_array){
            foreach ($bread_array as $alies => $title){
                $breadcrumbs .= "<li><a href='/category/{$alies}'>$title</a></li>";
            }
        }
        if ($title){
            $breadcrumbs .= "<li class=\"active\">$name</li>";
        }
        return $breadcrumbs;
    }

    public static function getParts($cats, $id){
            if (!$id) return false;
            $breadcrumbs = [];
            foreach ($cats as $k => $v){
                if (isset($cats[$id])){
                    $breadcrumbs[$cats[$id]['alies']] = $cats[$id]['title'];
                        $id = $cats[$id]['parent_id'];

            }else break;

        }
        return array_reverse($breadcrumbs, true);
    }
}