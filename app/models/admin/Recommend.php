<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 15/08/2018
 * Time: 20:53
 */

namespace app\models\admin;


use app\models\AppModel;

class Recommend extends AppModel {

    public $attributes = [

        'title' => '',
        'keywords' => '',
        'description' => '',
        'servises_id' => '',
        'content' => '',
        'alias' => '',
        'view' => '0',
        'price' => '0',
        'monthe' => '0',
        'last_month' => '0',
        'monthe_view' => '0',
        'owner_id' => '0',
        'bought_to' => '',
        'status' => '0',
        'avatar' => '0',

    ];

    public $rules = [
        'required' => [
            ['title'],
        ]

    ];

    public function getImg(){
        if (!empty($_SESSION['single'])){
            $this->attributes['avatar'] = $_SESSION['single'];
            unset($_SESSION['single']);
        }
    }

}