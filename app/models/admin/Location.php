<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 25/09/2018
 * Time: 21:41
 */

namespace app\models\admin;


use app\models\AppModel;

class Location extends AppModel {

    public $attributes = [

        'title' => '',
        'keywords' => '',
        'description' => '',
        'servises_id' => '',
        'content' => '',
        'alias' => '',
        'view' => '0',
        'price' => '0',
        'monthe' => '0',
        'last_month' => '0',
        'monthe_view' => '0',
        'owner_id' => '0',
        'bought_to' => '',
        'location_id' => '0',
        'avatar' => '0',

    ];

    public $rules = [
        'required' => [
            ['title'],
        ]

    ];

    public function getImg(){
        if (!empty($_SESSION['single'])){
            $this->attributes['avatar'] = $_SESSION['single'];
            unset($_SESSION['single']);
        }
    }
}