<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 02/08/2018
 * Time: 16:21
 */

namespace app\models\admin;


use app\models\AppModel;

class Services extends AppModel {

    public $attributes = [

        'title' => '',
        'keywords' => '',
        'description' => '',
        'parent_id' => '',
        'content' => '',
        'alias' => '',
        'view' => '0',
        'price' => '0',
        'owner_id' => '0',
        'bought_to' => '',
        'monthe' => '0',
        'last_month' => '0',
        'monthe_view' => '0',
        'status' => '',

    ];

    public $rules = [
        'required' => [
            ['title'],
        ]

    ];

    public function getId($id){
        $cats = App::$app->getProperty('cats');
        $ids = null;
        foreach ($cats as $k => $v){
            if ($v['parent_id'] == $id){
                $ids .= $k . ',';
                $ids .= $this->getId($k);
            }
        }
        return $ids;
    }
}