<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 28/09/2018
 * Time: 15:19
 */

namespace app\models\admin;


use app\models\AppModel;

class Type extends AppModel {
    public $attributes = [

        'title' => '',
        'keywords' => '',
        'description' => '',
        'servises_id' => '',
        'content' => '',
        'alias' => '',
        'view' => '0',
        'price' => '0',
        'monthe' => '0',
        'last_month' => '0',
        'monthe_view' => '0',
        'owner_id' => '0',
        'bought_to' => '',
        'avatar' => '0',

    ];

    public $rules = [
        'required' => [
            ['title'],
        ]

    ];

    public function getImg(){
        if (!empty($_SESSION['single'])){
            $this->attributes['avatar'] = $_SESSION['single'];
            unset($_SESSION['single']);
        }
    }
}