<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 13/07/2018
 * Time: 15:12
 */

namespace app\models\admin;


use RedBeanPHP\R;

class User extends \app\models\User {

    public $attributes = [

        'login' => '',
        'password' => '',
        'name' => '',
        'email' => '',
        'address' => '',
        'role'=> '',
        'phone'=> '',
        'avatar'=> '',
        'description'=> '',
        'alias'=> '',
        'back_avatar' =>'',

    ];

    public $rules = [
        'required' => [
            ['login'],
            ['name'],
            ['email'],
            ['role'],
        ],
        'email' => [
            ['email'],
        ]

    ];

    public function checkUnique(){
        $user = R::findOne('user','(login = ? OR email = ?) AND id <> ?', [$this->attributes['login'], $this->attributes['email'], $this->attributes['id']]);
        if ($user){
            if ($this->attributes['email'] == $user->email){
                $this->errors['unique'][]= 'Даний емейл вже використовується';
            }
            if ($this->attributes['login'] == $user->login){
                $this->errors['unique'][]= 'Даний логін вже використовується';
            }
            return false;
        }
        return true;
    }


}