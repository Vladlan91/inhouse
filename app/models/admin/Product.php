<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 13/07/2018
 * Time: 17:18
 */

namespace app\models\admin;


use app\models\AppModel;

class Product extends AppModel {

    public $attributes = [
        'title'=>'',
        'category_id'=>'',
        'description'=>'',
        'hit'=>'',
        'content'=>'',
        'brand_id'=>'',
        'status'=>'',
        'keywords'=>'',
        'price'=>'',
        'properties'=>'',
        'application'=>'',
        'alias'=>'',

    ];

    public $rules = [
        'required' => [
            ['title'],
            ['category_id'],
            ['price'],
            ['brand_id'],
        ],
        'integer' => [
            ['category_id'],
            ['brand_id'],
        ],

    ];

    public function editRelated($id, $data){
        $relatedPrduct = \R::getCol('SELECT related_id FROM related_product WHERE product_id = ?', [$id]);
        //Звязані товари відсутні, видалення з таблиці attribute_product
        if (empty($data['related']) && !empty($relatedPrduct)){
            \R::exec("DELETE FROM related_product WHERE product_id = ?", [$id]);
            return;
        }
        //Звязані товари добавленні, доблення в таблицю attribute_product
        if (empty($relatedPrduct) && !empty($data['related'])){
            $sql_part = '';
            foreach ($data['related'] as $v){
                $sql_part .= "($id, $v),";
            }
            $sql_part = rtrim($sql_part, ',');
            \R::exec("INSERT INTO related_product (product_id, related_id) VALUES $sql_part");
            return;
        }
        //Звязані товари змінені, видалення і доблення в таблицю attribute_product
        if (!empty($data['related'])){
            $resolt = array_diff($relatedPrduct, $data['related']);
            if (!empty($resolt) || count($relatedPrduct) != count($data['related'])){
                \R::exec("DELETE FROM related_product WHERE product_id = ?", [$id]);
                $sql_part = '';
                foreach ($data['related'] as $v){
                    $sql_part .= "($id, $v),";
                }
                $sql_part = rtrim($sql_part, ',');
                \R::exec("INSERT INTO related_product (product_id, related_id) VALUES $sql_part");
            }
        }
    }

    public function editFilter($id, $data){
        $filter = \R::getCol('SELECT attr_id FROM attribute_product WHERE product_id = ?', [$id]);
        //Фільтри відсутні, видалення з таблиці attribute_product
        if (empty($data['attrs']) && !empty($filter)){
            \R::exec("DELETE FROM attribute_product WHERE product_id = ?", [$id]);
            return;
        }
        //Фільтри добавленні, доблення в таблицю attribute_product
        if (empty($filter) && !empty($data['attrs'])){
            $sql_part = '';
            foreach ($data['attrs'] as $v){
                $sql_part .= "($v, $id),";
            }
            $sql_part = rtrim($sql_part, ',');
            \R::exec("INSERT INTO attribute_product (attr_id, product_id) VALUES $sql_part");
            return;
        }
        //Фільтри змінені, видалення і доблення в таблицю attribute_product
        if (!empty($data['attrs'])){
            $resolt = array_diff($filter, $data['attrs']);
            if (!$resolt || count($filter) != count($data['attrs'])){
                \R::exec("DELETE FROM attribute_product WHERE product_id = ?", [$id]);
                $sql_part = '';
                foreach ($data['attrs'] as $v){
                    $sql_part .= "($v, $id),";
                }
                $sql_part = rtrim($sql_part, ',');
                \R::exec("INSERT INTO attribute_product (attr_id, product_id) VALUES $sql_part");
            }
        }
    }

    public function uploadImg($name, $wmax, $hmax){
        $uploaddir = WWW . '/images/';
        $ext = strtolower(preg_replace("#.+\.([a-z]+)$#i", "$1", $_FILES[$name]['name']));
        $types = array("image/gif", "image/png", "image/jpeg", "image/pjpeg", "image/x-png");
        if ($_FILES[$name]['size'] > 1048576){
            $res = array("error" => "Помилка! Максимальний розмір - 1 Mb!");
            exit(json_encode($res));
        }
        if ($_FILES[$name]['error']){
            $res = array("error" => "Помилка! Можливо файл завеликий!");
            exit(json_encode($res));
        }
        if (!in_array($_FILES[$name]['type'], $types)){
            $res = array("error" => "Дозволені формати - .png, .gif, .jpeg!");
            exit(json_encode($res));
        }
        $new_name = md5(time()).".$ext";
        $uploadfile = $uploaddir.$new_name;
        if (@move_uploaded_file($_FILES[$name]['tmp_name'], $uploadfile)){
            if ($name == 'single'){
                $_SESSION['single'] =  $new_name;
            }else{
                $_SESSION['multi'][] =  $new_name;
            }
            self::resize($uploadfile, $uploadfile, $wmax, $hmax, $ext);
            $res = array("file" => $new_name);
            exit(json_encode($res));
        }
    }

    public static function resize($target, $dest, $wmax, $hmax, $ext){
        list($w_orig, $h_orig) = getimagesize($target);
        $ratio = $w_orig / $h_orig;

        if (($wmax / $hmax) > $ratio){
            $wmax = $hmax * $ratio;
        }else{
            $hmax = $wmax / $ratio;
        }

        $img = "";
        switch ($ext){
            case ("gif"):
                $img = imagecreatefromgif($target);
                break;
            case ("png"):
                $img = imagecreatefrompng($target);
                break;
            default:
                $img = imagecreatefromjpeg($target);
        }
        $newImg = imagecreatetruecolor($wmax, $hmax);

        if ($ext == "png"){
            imagesavealpha($newImg,true);
            $transPng = imagecolorallocatealpha($newImg,0,0,0,127);
            imagefill($newImg, 0,0, $transPng);
        }
        imagecopyresampled($newImg, $img, 0, 0, 0, 0, $wmax, $hmax, $w_orig, $h_orig);
        switch ($ext){
            case ("gif"):
                imagegif($newImg,$dest);
                break;
            case ("png"):
                imagepng($newImg,$dest);
                break;
            default:
                imagejpeg($newImg,$dest);
        }
        imagedestroy($newImg);
    }
    public function getImg(){
        if (!empty($_SESSION['single'])){
            $this->attributes['img'] = $_SESSION['single'];
            unset($_SESSION['single']);
        }
    }

    public function saveGallery($id){
        if (!empty($_SESSION['multi'])){
            $sql_part = "";
            foreach ($_SESSION['multi'] as $v){
                $sql_part .= "('$v', $id),";
            }
            $sql_part = rtrim($sql_part, ',');
            \R::exec("INSERT INTO gallery (img, product_id) VALUES $sql_part");
            unset($_SESSION['multi']);
        }
    }

    public function saveMod($mod, $id){
            $sql_part = "";
        \R::exec("DELETE FROM modification WHERE product_id = ?", [$id]);
            foreach ($mod as $k => $v){
                $vt = $v['title'];
                $vp = $v['price'];
                $sql_part .= "($id, '$vt', '$vp' ),";
            }
            $sql_part = rtrim($sql_part, ',');
            \R::exec("INSERT INTO modification (product_id, title, price) VALUES $sql_part");
            unset($_SESSION['modification']);
    }

    public function getMod($data){
        $mod = [];
        $i = 0;
        foreach ($data as $k => $v){
            $vs = $k % 2;
            if ($vs == 0){
                $mod[$i]['title'] = $v;
            }else{
                $i = $i - 1;
                $mod[$i]['price'] = $v;
            }
            $i++;
        }
        return $mod;
    }

}