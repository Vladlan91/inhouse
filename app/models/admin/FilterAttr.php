<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 21/07/2018
 * Time: 09:32
 */

namespace app\models\admin;


use app\models\AppModel;

class FilterAttr extends AppModel {

    public $attributes = [
        'value' =>'',
        'attr_group_id' =>'',
    ];

    public $rules = [
        'required' => [
            ['value'],
            ['attr_group_id'],
        ],
        'integer'=>[
            ['attr_group_id'],
        ]
    ];

}