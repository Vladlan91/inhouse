<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 06/09/2018
 * Time: 08:15
 */

namespace app\models\admin;


use app\models\AppModel;

class Images extends AppModel {


    public function uploadImg($name, $wmax, $hmax, $dir){
        $uploaddir = WWW . '/images/' . $dir;
        $ext = strtolower(preg_replace("#.+\.([a-z]+)$#i", "$1", $_FILES[$name]['name']));
        $types = array("image/gif", "image/png", "image/jpeg", "image/pjpeg", "image/x-png");
        if ($_FILES[$name]['size'] > 1048576){
            $res = array("error" => "Помилка! Максимальний розмір - 1 Mb!");
            exit(json_encode($res));
        }
        if ($_FILES[$name]['error']){
            $res = array("error" => "Помилка! Можливо файл завеликий!");
            exit(json_encode($res));
        }
        if (!in_array($_FILES[$name]['type'], $types)){
            $res = array("error" => "Дозволені формати - .png, .gif, .jpeg!");
            exit(json_encode($res));
        }
        $new_name = md5(time()).".$ext";
        $uploadfile = $uploaddir.$new_name;
        if (@move_uploaded_file($_FILES[$name]['tmp_name'], $uploadfile)){
            if ($name == 'single'){
                $_SESSION['single'] =  $new_name;
            }else{
                $_SESSION['multi'][] =  $new_name;
            }
            self::resize($uploadfile, $uploadfile, $wmax, $hmax, $ext);
            $res = array("file" => $new_name);
            exit(json_encode($res));
        }
    }

    public static function resize($target, $dest, $wmax, $hmax, $ext){
        list($w_orig, $h_orig) = getimagesize($target);
        $ratio = $w_orig / $h_orig;

        if (($wmax / $hmax) > $ratio){
            $wmax = $hmax * $ratio;
        }else{
            $hmax = $wmax / $ratio;
        }

        $img = "";
        switch ($ext){
            case ("gif"):
                $img = imagecreatefromgif($target);
                break;
            case ("png"):
                $img = imagecreatefrompng($target);
                break;
            default:
                $img = imagecreatefromjpeg($target);
        }
        $newImg = imagecreatetruecolor($wmax, $hmax);

        if ($ext == "png"){
            imagesavealpha($newImg,true);
            $transPng = imagecolorallocatealpha($newImg,0,0,0,127);
            imagefill($newImg, 0,0, $transPng);
        }
        imagecopyresampled($newImg, $img, 0, 0, 0, 0, $wmax, $hmax, $w_orig, $h_orig);
        switch ($ext){
            case ("gif"):
                imagegif($newImg,$dest);
                break;
            case ("png"):
                imagepng($newImg,$dest);
                break;
            default:
                imagejpeg($newImg,$dest);
        }
        imagedestroy($newImg);
    }

}