<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 21/07/2018
 * Time: 09:32
 */

namespace app\models\admin;


use app\models\AppModel;

class FilterGroup extends AppModel {

    public $attributes = [
        'title' =>'',
    ];

    public $rules = [
        'required' => [
            ['title'],
        ],
    ];
}