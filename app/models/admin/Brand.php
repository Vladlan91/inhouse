<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 20/07/2018
 * Time: 17:37
 */

namespace app\models\admin;


use app\models\AppModel;

class Brand extends AppModel {

    public $attributes = [
        'title'=>'',
        'alias'=>'',
        'date_start'=>'',
        'date_finish'=>'',
        'plan'=>'',
        'img'=>'',
        'description'=>'',
        'history'=>'',
        'delivery'=>'',
        'contact'=>'',
        'logo'=>'',

    ];

    public $rules = [
        'required' => [
            ['title'],
        ]

    ];

    public function getImg(){
        if (!empty($_SESSION['img'])){
            $this->attributes['img'] = $_SESSION['img'];
            $this->attributes['logo'] = $_SESSION['logo'];
            unset($_SESSION['img']);
            unset($_SESSION['logo']);
        }
    }

}