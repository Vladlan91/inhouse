<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 20/06/2018
 * Time: 11:30
 */

namespace app\models;


use inhouse\App;

class Category extends AppModel {

    public $attributes = [

        'title' => '',
        'keywords' => '',
        'description' => '',
        'parent_id' => '',

    ];

    public $rules = [
        'required' => [
            ['title'],
        ]

    ];

    public function getId($id){
        $cats = App::$app->getProperty('cats');
        $ids = null;
        foreach ($cats as $k => $v){
            if ($v['parent_id'] == $id){
                $ids .= $k . ',';
                $ids .= $this->getId($k);
            }
        }
        return $ids;
    }

}