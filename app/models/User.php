<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 21/06/2018
 * Time: 00:21
 */

namespace app\models;


use RedBeanPHP\R;

class User extends AppModel {

    public $attributes = [

        'login' => '',
        'password' => '',
        'name' => '',
        'email' => '',
        'address' => '',
        'role'=> '',
        'phone'=> '',
        'avatar'=> '',
        'description'=> '',
        'alias'=> '',
        'back_avatar' =>'',



    ];

    public $rules = [
        'required' => [
            ['login'],
            ['password'],
            ['name'],
            ['email'],
            ['address'],

        ],
        'email' => [
            ['email'],
        ],
        'lengthMin' => [
            ['password', 6],
        ]
    ];

    public function checkUnique(){
        $user = R::findOne('user','login = ? OR email = ?', [$this->attributes['login'], $this->attributes['email']]);
            if ($user){
                if ($this->attributes['email'] == $user->email){
                    $this->errors['unique'][]= 'Даний емейл вже використовується';
                }
                if ($this->attributes['login'] == $user->login){
                    $this->errors['unique'][]= 'Даний логін вже використовується';
                }
                return false;
        }
        return true;
    }

    public function login($isAdmin = false){
        $login = !empty(trim($_POST['login'])) ? trim($_POST['login']) : null;
        $password = !empty(trim($_POST['password'])) ? trim($_POST['password']) : null;
        if ($login && $password) {
            if ($isAdmin) {
                $user = R::findOne('user', "login = ? AND role = 'admin'", [$login]);
            }else{
                $user = R::findOne('user', "login = ?", [$login]);
            }
            if ($user){
                if (password_verify($password, $user->password)){
                    foreach ($user as $item=>$value) {
                        if ($item !='password') {
                            $_SESSION['user'][$item] = $value;
                        }
                        
                    }
                    return true;
                }

            }
        } return false;
    }

    public static function checkAuth(){
        return isset($_SESSION['user']);
    }
    public static function isAdmin(){
        return (isset($_SESSION['user']) && $_SESSION['user']['role'] == 'admin');
    }

    public function editFilter($id, $data){
        $filter = \R::getCol('SELECT attr_id FROM u_attribute_product WHERE product_id = ?', [$id]);
        //Фільтри відсутні, видалення з таблиці attribute_product
        if (empty($data['attrs']) && !empty($filter)){
            \R::exec("DELETE FROM u_attribute_product WHERE product_id = ?", [$id]);
            return;
        }
        //Фільтри добавленні, доблення в таблицю attribute_product
        if (empty($filter) && !empty($data['attrs'])){
            $sql_part = '';
            foreach ($data['attrs'] as $v => $k){
                foreach ($k as $item => $value){
                    $newar[] = $value;
                }
            }
            foreach ($newar as $v => $k){
                $sql_part .= "($k, $id),";
            }

            $sql_part = rtrim($sql_part, ',');
            \R::exec("INSERT INTO u_attribute_product (attr_id, product_id) VALUES $sql_part");
            return;
        }
        //Фільтри змінені, видалення і доблення в таблицю attribute_product
        if (!empty($data['attrs'])){
            $resolt = array_diff($filter, $data['attrs']);
            if (!$resolt || count($filter) != count($data['attrs'])){
                \R::exec("DELETE FROM u_attribute_product WHERE product_id = ?", [$id]);
                $sql_part = '';
                foreach ($data['attrs'] as $v => $k){
                    foreach ($k as $item => $value){
                        $newar[] = $value;
                    }
                }
                foreach ($newar as $v => $k){
                    $sql_part .= "($k, $id),";
                }
                $sql_part = rtrim($sql_part, ',');
                \R::exec("INSERT INTO u_attribute_product (attr_id, product_id) VALUES $sql_part");
            }
        }
    }

    public function getImg(){
        if (!empty($_SESSION['single'])){
            $this->attributes['avatar'] = $_SESSION['single'];
            unset($_SESSION['single']);
        }
    }

    public function saveGallery($id){
        if (!empty($_SESSION['multi'])){
            $sql_part = "";
            foreach ($_SESSION['multi'] as $v){
                $sql_part .= "('$v', $id),";
            }
            $sql_part = rtrim($sql_part, ',');
            \R::exec("INSERT INTO gallery (img, product_id) VALUES $sql_part");
            unset($_SESSION['multi']);
        }
    }


}