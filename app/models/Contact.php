<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 13/09/2018
 * Time: 12:10
 */

namespace app\models;


use inhouse\App;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class Contact extends AppModel {

    public $attributes = [

        'name' => '',
        'phone' => '',
        'serves_id' => '',
        'email' => '',
        'content' => '',
        'date' => '',
    ];

    public $rules = [
        'required' => [
            ['name'],
            ['phone'],
            ['email'],
            ['serves_id'],
            ['content'],


        ],
        'email' => [
            ['email'],
        ]
    ];

    public static function mailOrder($user_email){

        $transport = (new Swift_SmtpTransport(App::$app->getProperty('smtp_host'), App::$app->getProperty('smtp_port'), App::$app->getProperty('smtp_protocol')))
            ->setUsername(App::$app->getProperty('smtp_login'))
            ->setPassword(App::$app->getProperty('smtp_password'));


        $mailer = new Swift_Mailer($transport);

        ob_start();
        require APP . '/views/Mail/mail_massages.php';
        $body = ob_get_clean();


        $message_client = (new Swift_Message("Ваше повідомлення зареєстровано"))
            ->setFrom([App::$app->getProperty('smtp_login') => App::$app->getProperty('shop_name')])
            ->setTo($user_email)
            ->setBody($body,'text/html')
        ;

        $message_admin = (new Swift_Message("Отримано нове повідомлення"))
            ->setFrom([App::$app->getProperty('smtp_login') => App::$app->getProperty('shop_name')])
            ->setTo(App::$app->getProperty('admin_email'))
            ->setBody($body,'text/html')
        ;


        $result = $mailer->send($message_client);
        $result = $mailer->send($message_admin);

        unset($_SESSION['messages']);
        $_SESSION['success'] = "Команда InHouse дякує Вам за запитання, очікуйте на телефонний звязок нашого менеджера, для уточнення і підтверження інформації." ;
    }

}