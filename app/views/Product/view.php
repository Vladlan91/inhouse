
<div class="head-bread">
    <div class="container">
        <ol class="breadcrumb">
            <?=$breadcrumbs;?>
        </ol>
    </div>
</div>
<div class="showcase-grid">
    <div class="container">
        <div class="col-md-12 showcase">
            <div class="col-md-6 showcase">
                <div class="flexslider">
                    <?php if ($gallery):?>
                    <ul class="slides">
                        <?php foreach ($gallery as $item):?>
                        <li data-thumb="images/productgallery/<?=$item->img; ?>">
                            <div class="thumb-image"> <img src="images/productgallery/<?=$item->img; ?>" class="img-responsive"> </div>
                        </li>
                        <?php endforeach;?>
                        <?php else: ?>
                            <img style="width: 300px;" src="images/productavatar/<?=$product->img; ?>" data-imagezoom="true" class="img-responsive">
                        <?php endif;?>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
            <?php $curr = \inhouse\App::$app->getProperty('currency'); ?>
            <div class="col-md-6 showcase">
                <div class="showcase-rt-top">
                    <div class="pull-left shoe-name">
                        <h3><?=$product->title; ?></h3>
                        <h3><?=$product->brend['title']; ?></h3>
                        <p><a href="category/<?=$cats[$product->category_id]['alies']; ?>"><?=$cats[$product->category_id]['title']; ?></a></p>
                        <h5 id="base-price" data-base="<?=$product->price * $curr['value']; ?>"><?= $curr['symbol_left']; ?><?=$product->price * $curr['value']; ?><?= $curr['symbol_right']; ?></h5>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <hr class="featurette-divider">
                <div class="shocase-rt-bot">
                    <div class="float-qty-chart">
                        <ul>
                            <li class="qty">
                                <h3>Модефікація</h3>
                                <div class="availables">
                                    <select class="form-control siz-chrt">
                                        <?php foreach ($modificat as $mod):?>
                                            <option data-title="<?=$mod->title;?>" data-price="<?=$mod->price * $curr['value'];?>" value="<?=$mod->id;?>"><?=$mod->title;?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </li>
                            <li class="qty">
                                <h3>Кількість</h3>
                                <input type="number" value="1" size="4" name="quantity" min="1" step="1" class="form-control qnty-chrt">
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <ul>
                        <li class="ad-2-crt simpleCart_shelfItem">
                            <a id="productAdd" data-id="<?=$product->id; ?>" class="btn add_card add-to-card-link item_add" href="cart/add?id=<?=$product->id; ?>" role="button">Додати в кошик</a>
                            <a class="btn" href="#" role="button">Купити</a>
                        </li>
                    </ul>
                </div>
                <div class="showcase-last">
                    <h3>Призначення</h3>
                        <h4><?=$product->content; ?></h4>
                </div>
            </div>
                <div class="clearfix"></div>
            <div class="col-md-12">
                <div class="specifications">
                        <h3>Деталі продукту</h3>
                        <div class="detai-tabs">
                            <ul class="nav nav-pills tab-nike" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Характеристики</a></li>
                                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Застосування</a></li>
                                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Відгуки</a></li>
                            </ul>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">
                                    <?=$product->properties; ?>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="profile">
                                    <?=$product->application; ?>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="messages">

                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<?php if ($related):?>
        <div class="specifications">
            <div class="container">
                <h3>З цим товаром купують</h3>

                    <div class="product">
                        <div class="container">
                            <div class="product-top">
                                <div class="product-one">
                                    <?php foreach ($related as $item): ?>
                                        <div class="col-md-3 product-left">
                                            <div class="product-main simpleCart_shelfItem">
                                                <a href="product/<?=$item['alias']; ?>" class="mask"><img class="img-responsive zoom-img" src="images/productavatar/<?= $item['img']; ?>" alt="" /></a>
                                                <div class="product-bottom">

                                                    <?php $item_text = pruning($item['content'], 55); ?>
                                                    <h3><?=$item['title']; ?></h3>
                                                    <p><?=$item_text; ?></p>
                                                    <h4><a id="productAdd" data-id="<?=$item['id']; ?>" class="item_add add-to-card-link item_add" href="card/add?id=<?=$item['id']; ?>" ><i></i></a> <span class=" item_price"><?= $curr['symbol_left']; ?><?= $item['price'] * $curr['value']; ?><?= $curr['symbol_right']; ?></span></h4>
                                                </div>
                                                 <?php if ($item['hit']):?>
                                                    <div class="srch">
                                                        <span>НОВИНКА</span>
                                                    </div>
                                                 <?php endif; ?>
                                            </div>
                                        </div>
                                    <?php endforeach;?>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
<?php endif; ?>
<?php if ($recentlyViewed):?>
    <div class="specifications">
        <div class="container">
            <h3>Ви переглядали</h3>

            <div class="product">
                <div class="container">
                    <div class="product-top">
                        <div class="product-one">
                            <?php foreach ($recentlyViewed as $item): ?>
                                <div class="col-md-3 product-left">
                                    <div class="product-main simpleCart_shelfItem">
                                        <a href="product/<?=$item['alias']; ?>" class="mask"><img class="img-responsive zoom-img" src="images/productavatar/<?= $item['img']; ?>" alt="" /></a>
                                        <div class="product-bottom">

                                            <?php $item_text = pruning($item['content'], 55); ?>
                                            <h3><?=$item['title']; ?></h3>
                                            <p><?=$item_text; ?></p>
                                            <h4><a id="productAdd" data-id="<?=$item['id']; ?>" class="item_add add-to-card-link item_add" href="card/add?id=<?=$item['id']; ?>" ><i></i></a> <span class=" item_price"><?= $curr['symbol_left']; ?><?= $item['price'] * $curr['value']; ?><?= $curr['symbol_right']; ?></span></h4>
                                        </div>
                                        <?php if ($item['hit']):?>
                                            <div class="srch">
                                                <span>НОВИНКА</span>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endforeach;?>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
