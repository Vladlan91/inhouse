<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="<?= PATH ?>/images/ic.png" type="image/png">
<!--    <link href="/css/style.css" rel="stylesheet" type="text/css" media="all" />-->
    <base href="/adminLTE/">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?=$this->getMeta(); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="dist/css/adminlte.css">
    <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
    <link rel="stylesheet" href="plugins/morris/morris.css">
    <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="plugins/select2/select2.css">
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="my.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand bg-danger navbar-light border-bottom">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?= ADMIN ?>/" class="nav-link">CRM</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?= ADMIN ?>/" class="nav-link">Аналітика</a>
            </li>
        </ul>

        <!-- SEARCH FORM -->
        <form class="form-inline ml-3">
            <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </form>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Messages Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="fa fa-comments-o"></i>
                    <span class="badge badge-danger navbar-badge">3</span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <a href="#" class="dropdown-item">
                        <!-- Message Start -->
                        <div class="media">

                            <img src="dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                            <div class="media-body">
                                <h3 class="dropdown-item-title">
                                    Brad Diesel
                                    <span class="float-right text-sm text-danger"><i class="fa fa-star"></i></span>
                                </h3>
                                <p class="text-sm">Call me whenever you can...</p>
                                <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i> 4 Hours Ago</p>
                            </div>
                        </div>
                        <!-- Message End -->
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <!-- Message Start -->
                        <div class="media">
                            <img src="dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                            <div class="media-body">
                                <h3 class="dropdown-item-title">
                                    John Pierce
                                    <span class="float-right text-sm text-muted"><i class="fa fa-star"></i></span>
                                </h3>
                                <p class="text-sm">I got your message bro</p>
                                <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i> 4 Hours Ago</p>
                            </div>
                        </div>
                        <!-- Message End -->
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <!-- Message Start -->
                        <div class="media">
                            <img src="dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                            <div class="media-body">
                                <h3 class="dropdown-item-title">
                                    Nora Silvester
                                    <span class="float-right text-sm text-warning"><i class="fa fa-star"></i></span>
                                </h3>
                                <p class="text-sm">The subject goes here</p>
                                <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i> 4 Hours Ago</p>
                            </div>
                        </div>
                        <!-- Message End -->
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
                </div>
            </li>
            <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="fa fa-bell-o"></i>
                    <span class="badge badge-warning navbar-badge">15</span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <span class="dropdown-item dropdown-header">15 Notifications</span>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fa fa-envelope mr-2"></i> 4 new messages
                        <span class="float-right text-muted text-sm">3 mins</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fa fa-users mr-2"></i> 8 friend requests
                        <span class="float-right text-muted text-sm">12 hours</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fa fa-file mr-2"></i> 3 new reports
                        <span class="float-right text-muted text-sm">2 days</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
                        class="fa fa-th-large"></i></a>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="<?= PATH ?>" class="brand-link">
            <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light">INHOUSE</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <?php if ($_SESSION['user']['useravatar']):?>
                        <img src="dist/img/<?=$_SESSION['user']['useravatar'];?>" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                    <?php  else:?>
                        <img src="/images/avatar-profile-icon-man.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                    <?php endif;?>
                </div>
                <div class="info">
                    <a href="<?=ADMIN;?>/user/edit?id=<?=$_SESSION['user']['id'];?>" class="d-block"><?=$_SESSION['user']['name'];?></a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item has-treeview menu-open">
                        <a href="<?= ADMIN ?>/" class="nav-link ">
                            <i class="nav-icon fa fa-dashboard"></i>
                            <p>Cтатистика</p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview menu-open">
                        <a href="<?= ADMIN ?>/order" class="nav-link ">
                            <i class="nav-icon fa fa-book"></i>
                            <p>Замовлення</p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="" class="nav-link ">
                            <i class="nav-icon fa fa-braille"></i>
                            <p>Категорії товарів
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= ADMIN ?>/category" class="nav-link ">
                                    <i class="fa fa-bars nav-icon"></i>
                                    <p>Список категорій</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= ADMIN ?>/category/add" class="nav-link">
                                    <i class="fa fa-clipboard nav-icon"></i>
                                    <p>Додати категорію</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="" class="nav-link ">
                            <i class="nav-icon fa fa-anchor"></i>
                            <p>Загальні послуги
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= ADMIN ?>/services" class="nav-link">
                                    <i class="fa fa-bars nav-icon"></i>
                                    <p>Список послуг</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= ADMIN ?>/services/list" class="nav-link">
                                    <i class="fa fa-bars nav-icon"></i>
                                    <p>Список сторінок</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= ADMIN ?>/services/add" class="nav-link">
                                    <i class="fa fa-clipboard nav-icon"></i>
                                    <p>Додати послугу</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="" class="nav-link ">
                            <i class="nav-icon fa fa-map"></i>
                            <p>Послуги у містах
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= ADMIN ?>/location/list" class="nav-link">
                                    <i class="fa fa-bars nav-icon"></i>
                                    <p>Список сторінок</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= ADMIN ?>/location/add" class="nav-link">
                                    <i class="fa fa-clipboard nav-icon"></i>
                                    <p>Додати послугу</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="" class="nav-link ">
                            <i class="nav-icon fa fa-map"></i>
                            <p>Види послуг
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= ADMIN ?>/type/list" class="nav-link">
                                    <i class="fa fa-bars nav-icon"></i>
                                    <p>Список сторінок</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= ADMIN ?>/type/add" class="nav-link">
                                    <i class="fa fa-clipboard nav-icon"></i>
                                    <p>Додати вид послуги</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="" class="nav-link ">
                            <i class="nav-icon fa fa-columns"></i>
                            <p>Рекомендації
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= ADMIN ?>/recommend" class="nav-link">
                                    <i class="fa fa-bars nav-icon"></i>
                                    <p>Список сторінок</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= ADMIN ?>/recommend/add" class="nav-link">
                                    <i class="fa fa-clipboard nav-icon"></i>
                                    <p>Додати сторінку</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="" class="nav-link ">
                            <i class="nav-icon fa fa-cart-plus"></i>
                            <p>Товари
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= ADMIN ?>/product" class="nav-link ">
                                    <i class="fa fa-bars nav-icon"></i>
                                    <p>Список товарів</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= ADMIN ?>/product/add" class="nav-link">
                                    <i class="fa fa-clipboard nav-icon"></i>
                                    <p>Додати товари</p>
                                </a>
                            </li>
                        </ul>
                    </li>
<!--                    <li class="nav-item has-treeview">-->
<!--                        <a href="" class="nav-link ">-->
<!--                            <i class="nav-icon fa fa-building"></i>-->
<!--                            <p>Міста-->
<!--                                <i class="right fa fa-angle-left"></i>-->
<!--                            </p>-->
<!--                        </a>-->
<!--                        <ul class="nav nav-treeview">-->
<!--                            <li class="nav-item">-->
<!--                                <a href="" class="nav-link ">-->
<!--                                    <i class="fa fa-bars nav-icon"></i>-->
<!--                                    <p>Список міст</p>-->
<!--                                </a>-->
<!--                            </li>-->
<!--                            <li class="nav-item">-->
<!--                                <a href="" class="nav-link">-->
<!--                                    <i class="fa fa-clipboard nav-icon"></i>-->
<!--                                    <p>Додати місто</p>-->
<!--                                </a>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </li>-->
                    <li class="nav-item has-treeview">
                        <a href="" class="nav-link ">
                            <i class="nav-icon fa fa-usd"></i>
                            <p>Валюти
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= ADMIN ?>/currency" class="nav-link ">
                                    <i class="fa fa-bars nav-icon"></i>
                                    <p>Список валют</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= ADMIN ?>/currency/add" class="nav-link">
                                    <i class="fa fa-clipboard nav-icon"></i>
                                    <p>Додати валюту</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="" class="nav-link ">
                            <i class="nav-icon fa fa-linode"></i>
                            <p>Фільтри товарів
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= ADMIN ?>/filter" class="nav-link ">
                                    <i class="fa fa-bars nav-icon"></i>
                                    <p>Групи фільтрів</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= ADMIN ?>/filter/attr" class="nav-link">
                                    <i class="fa fa-clipboard nav-icon"></i>
                                    <p>Фільтри</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="" class="nav-link ">
                            <i class="nav-icon fa fa-linode"></i>
                            <p>Фільтри користувачів
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= ADMIN ?>/ufilter" class="nav-link ">
                                    <i class="fa fa-bars nav-icon"></i>
                                    <p>Групи фільтрів</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= ADMIN ?>/ufilter/attr" class="nav-link">
                                    <i class="fa fa-clipboard nav-icon"></i>
                                    <p>Фільтри</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview menu-open">
                        <a href="<?= ADMIN ?>/cache" class="nav-link ">
                            <i class="nav-icon fa fa-connectdevelop"></i>
                            <p>Кешування</p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="" class="nav-link ">
                            <i class="nav-icon fa fa-address-card"></i>
                            <p>Користувачі
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?=ADMIN;?>/user" class="nav-link ">
                                    <i class="fa fa-bars nav-icon"></i>
                                    <p>Список користувачів</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=ADMIN;?>/user/add" class="nav-link">
                                    <i class="fa fa-clipboard nav-icon"></i>
                                    <p>Додати користувача</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="" class="nav-link ">
                            <i class="nav-icon fa fa-industry"></i>
                            <p>Виробники
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?=ADMIN;?>/brand" class="nav-link ">
                                    <i class="fa fa-bars nav-icon"></i>
                                    <p>Список виробники</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=ADMIN;?>/brand/add" class="nav-link">
                                    <i class="fa fa-clipboard nav-icon"></i>
                                    <p>Додати виробника</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="" class="nav-link ">
                            <i class="nav-icon fa fa-apple"></i>
                            <p>Повідомлення
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?=ADMIN;?>/messedge/franchiz" class="nav-link ">
                                    <i class="fa fa-binoculars nav-icon"></i>
                                    <p>Запит на франшизу</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=ADMIN;?>/messedge/messedg" class="nav-link">
                                    <i class="fa fa-camera-retro nav-icon"></i>
                                    <p>Загальні запитання</p>
                                </a>
                            </li>
                        </ul>
                    </li>
<!--                    <li class="nav-item has-treeview">-->
<!--                        <a href="" class="nav-link ">-->
<!--                            <i class="nav-icon fa fa-apple"></i>-->
<!--                            <p>Банери та реклама-->
<!--                                <i class="right fa fa-angle-left"></i>-->
<!--                            </p>-->
<!--                        </a>-->
<!--                        <ul class="nav nav-treeview">-->
<!--                            <li class="nav-item">-->
<!--                                <a href="" class="nav-link ">-->
<!--                                    <i class="fa fa-binoculars nav-icon"></i>-->
<!--                                    <p>Банери</p>-->
<!--                                </a>-->
<!--                            </li>-->
<!--                            <li class="nav-item">-->
<!--                                <a href="" class="nav-link">-->
<!--                                    <i class="fa fa-camera-retro nav-icon"></i>-->
<!--                                    <p>Бренди</p>-->
<!--                                </a>-->
<!--                            </li>-->
<!--                            <li class="nav-item">-->
<!--                                <a href="" class="nav-link">-->
<!--                                    <i class="fa fa-bullhorn nav-icon"></i>-->
<!--                                    <p>Хіти року</p>-->
<!--                                </a>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </li>-->

<!--                    <li class="nav-item has-treeview">-->
<!--                        <a href="" class="nav-link ">-->
<!--                            <i class="nav-icon fa fa-percent"></i>-->
<!--                            <p>Дилерство-->
<!--                                <i class="right fa fa-angle-left"></i>-->
<!--                            </p>-->
<!--                        </a>-->
<!--                        <ul class="nav nav-treeview">-->
<!--                            <li class="nav-item">-->
<!--                                <a href="" class="nav-link ">-->
<!--                                    <i class="fa fa-registered nav-icon"></i>-->
<!--                                    <p>Знижки виробників</p>-->
<!--                                </a>-->
<!--                            </li>-->
<!--                            <li class="nav-item">-->
<!--                                <a href="" class="nav-link">-->
<!--                                    <i class="fa fa-sitemap nav-icon"></i>-->
<!--                                    <p>Знижки користувачів</p>-->
<!--                                </a>-->
<!--                            </li>-->
<!--                            <li class="nav-item">-->
<!--                                <a href="" class="nav-link">-->
<!--                                    <i class="fa fa-balance-scale nav-icon"></i>-->
<!--                                    <p>Порівняння</p>-->
<!--                                </a>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </li>-->
<!--                    <li class="nav-item has-treeview">-->
<!--                        <a href="--><?//= ADMIN ?><!--/analitic" class="nav-link ">-->
<!--                            <i class="nav-icon fa fa-cc-visa"></i>-->
<!--                            <p>Прибутoк-->
<!--                                <i class="right fa fa-angle-left"></i>-->
<!--                            </p>-->
<!--                        </a>-->
<!--                        <ul class="nav nav-treeview">-->
<!--                            <li class="nav-item">-->
<!--                                <a href="" class="nav-link ">-->
<!--                                    <i class="fa fa-registered nav-icon"></i>-->
<!--                                    <p>Реклама</p>-->
<!--                                </a>-->
<!--                            </li>-->
<!--                            <li class="nav-item">-->
<!--                                <a href="" class="nav-link">-->
<!--                                    <i class="fa fa-sitemap nav-icon"></i>-->
<!--                                    <p>Продажі</p>-->
<!--                                </a>-->
<!--                            </li>-->
<!--                            <li class="nav-item">-->
<!--                                <a href="" class="nav-link">-->
<!--                                    <i class="fa fa-balance-scale nav-icon"></i>-->
<!--                                    <p>Загальний звіт</p>-->
<!--                                </a>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </li>-->
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>
    <div class="content-wrapper">
       <?=$content;?>
    </div>

    <aside class="control-sidebar control-sidebar-dark"></aside>
</div>
<!-- ./wrapper -->
<script>
    var path = '<?=PATH;?>';
    adminpath = '<?=ADMIN;?>';
</script>
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>

<script src="/js/ajaxupload.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/ckeditor/ckeditor.js"></script>

<script src="plugins/select2/select2.full.js"></script>

<script src="plugins/ckeditor/adapters/jquery.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<script src="/js/validator.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="my.js"></script>
</body>
</html>
