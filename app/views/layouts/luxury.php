<!doctype html>
<head>
    <link rel="stylesheet" href="/public/adminLTE/plugins/font-awesome/css/font-awesome.min.css">
    <base href="/">
    <link rel="shortcut icon" href="images/ic.png" type="image/png">
    <?=$this->getMeta(); ?>
    <script src="card/js/jquery-2.1.4.min.js"></script>
    <script src="js/typeahead.bundle.js"></script>
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="card/css/style.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="card/css/flexslider.css" type="text/css" media="screen" />
    <link href="megamenu/css/ionicons.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="card/css/form.css" rel="stylesheet" type="text/css" media="all" />
    <link href="megamenu/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" type="text/css" href="card/css/jquery-ui.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script>
        $(window).load(function() {
            $('.flexslider').flexslider({
                animation: "slide",
                controlNav: "thumbnails"
            });
        });
    </script>
</head>
<body id="index-page">
<div class="top-header">
        <div class="top-header-main">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 top-header-left">
                <a class="logo" href="<?php PATH; ?>" btattached="true"></a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 top-header-left">
                <div id="phone-line" class="left">Горяча лінія
                    <span>096-456-36-14</span>
                </div>
            </div>
<!--            <a href="#modal" class="btn btn-primary">Модальное окно №1</a>-->
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 top-header-left">
                <div class="drop">
                    <div class="box">
                        <select id="currency" tabindex="4" class="dropdown drop">
                            <?php  new app\widgets\currency\Currency();?>
                        </select>
                    </div>
<!--                    <div class="box1">-->
<!--                        <select tabindex="4" class="dropdown">-->
<!--                            <option value="" class="label">English :</option>-->
<!--                            <option value="1">English</option>-->
<!--                            <option value="2">French</option>-->
<!--                            <option value="3">German</option>-->
<!--                        </select>-->
<!--                    </div>-->
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 top-header-left">
                <div class="drop">
                    <div class="cart box_1">
                        <a href="cart/show" onclick="getCart(); return false;">
                            <div class="total">
                                <?php if (!empty($_SESSION['cart'])):?>
                                <span class="simpleCart_total"><?=$_SESSION['cart.currency']['symbol_left'] .$_SESSION['cart.sum'] . $_SESSION['cart.currency']['symbol_right'];?> </span>
                                    <?php else:?>
                                    <span class="simpleCart_total">Порожній кошик</span>
                                <?php endif;?>

                            </div>
                            <img src="images/cart-1.png" alt="" />
                        </a>
                        <div class="clearfix"> </div>
                    </div>
                </div>
            </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="header-bottom">
    <div class="container">
        <div class="header">
            <div class="col-md-12 ">
                <div class="menu-container">
                    <div class="menu">
                        <ul class="menu">
                            <li><a href="/">Головна</a></li>
                            <?php new \app\widgets\menu\Menu([
                                'tpl' => WWW . '/menu/menu_serv.php',
                                'prop' =>'serv',
                                'table' => 'servises',
                                'cashKey' => 'inhouse_menu_ser',
                                'cash' => 3600,
                            ]); ?>
                            <?php new \app\widgets\menu\Menu([
                                'tpl' => WWW . '/menu/menu.php',
                                'prop' =>'cats',
                                'cashKey' => 'inhouse_menu',
                                'cash' => 3600,
                            ]); ?>
                            <li><a href="brand/">Виробники</a></li>
                            <li><a href="builder/">Забудовники</a></li>
                            <li><a href="master/">Бригади</a></li>
                            <li><a href="calculator/">Калькулятори</a></li>
                            <li><a href="recommend/">Рекомендації</a></li>
                            <li><a href="contact/">Контакти</a></li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
<!--            <div class="col-md-2 header-right">-->
<!--                <div class="search-bar">-->
<!--                    <form action="search" method="get" autocomplete="off">-->
<!--                        <input type="text" class="typeahead" id="typeahead" name="s">-->
<!--                        <input type="submit" value="">-->
<!--                    </form>-->
<!--                </div>-->
<!--            </div>-->
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
                <?php endif;?>
                <?php if (isset($_SESSION['success'])):?>
                    <div class="alert alert-success">
                        <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>
    <?=$content; ?>
</div>
<div class="information">
    <div class="container">
        <div class="infor-top">
            <div class="col-md-3 infor-left">
                <h3>Ми в соціальних мережах</h3>
                <ul>
                    <li><a href="#"><span class="fb"></span><h6>Facebook</h6></a></li>
                    <li><a href="#"><span class="twit"></span><h6>Twitter</h6></a></li>
                    <li><a href="#"><span class="google"></span><h6>Google+</h6></a></li>
                </ul>
            </div>
            <div class="col-md-3 infor-left">
                <h3>Інформаційний портал</h3>
                <ul>
                    <li><a href="more/franchiza"><p>Франшиза INHOUSE</p></a></li>
                    <li><a href="#"><p>Для майстрів</p></a></li>
                    <li><a href="#"><p>Для забудовників</p></a></li>
                    <li><a href="contact.html"><p>Стати партнером</p></a></li>
                    <li><a href="#"><p>Частіше запитують</p></a></li>
                </ul>
            </div>
            <div class="col-md-3 infor-left">
                <h3>Мій акаунт</h3>
                <ul>
                    <?php if (!empty($_SESSION['user'])):?>
                    <li><a href="user/profile"><p>Mоя персональна інформація</p></a></li>
                    <li><a href="user/logout"><p>Вихід</p></a></li>
                    <?php else:?>
                        <li><a href="user/login"><p>Вхід</p></a></li>
                        <li><a href="user/signup"><p>Реєстрація</p></a></li>
                    <?php endif;?>
                </ul>
            </div>
            <div class="col-md-3 infor-left">
                <h3>Контактна інформація</h3>
                <h4> INHOUSE дистрибьютор виробників будівельних матеріалів, які зарекомендували себе на ринку України.
                    <span>Телефон,</span>
                    +38(096)-456-3-614.</h4>
                <p><a href="mailto:example@email.com">we.inhouse@gmail.com</a></p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="container">
        <div class="footer-top">
            <div class="col-md-2 top-header-left">
                <a class="logo2" href="<?= ADMIN ?>" btattached="true"></a>
            </div>
            <div class="col-md-5 mt footer-left">
                <form>
                    <input type="text" value="Ваш Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Ваш Email';}">
                    <input type="submit" value="Підписатись">
                </form>
            </div>
            <div class="col-md-5 mt footer-right">
                <p>© 2015 In House. Всі права захищені | Design by  <a href="" target="_blank">ITM</a> </p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="modal fade in" id="cart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="wi" aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Кошик</h4>
            </div>
            <div class="modal-body">


            </div>
            <div class="modal-footer">
                <a type="button" class="order order1" onclick="clearCart()">Очистити кошик</a>
            </div>
        </div>
    </div>
</div>
<div class="preload">
    <img src="images/ring.svg" alt="">
</div>
<?php $curr = \inhouse\App::$app->getProperty('currency'); ?>
<script>
    var path = '<?=PATH; ?>',
        course = '<?=$curr['value']; ?>',
        semboleLeft = '<?= $curr['symbol_left']; ?>',
        semboleRight = '<?= $curr['symbol_right']; ?>';
</script>
<script>
    var path = '<?=PATH;?>';
    adminpath = '<?=ADMIN;?>';
</script>
<script src="js/ajaxupload.js"></script>
<!--<script src="js/remodal.js"></script>-->
<script src="megamenu/js/megamenu.js"></script>
<script src="js/responsiveslides.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/prettify.js"></script>
<script src="js/jquery.jrumble.1.3.min.js"></script>
<script>
    $(function () {
        $("#slider4").responsiveSlides({
            auto: true,
            pager: true,
            nav: true,
            speed: 500,
            namespace: "callbacks",
            before: function () {
                $('.events').append("<li>before event fired.</li>");
            },
            after: function () {
                $('.events').append("<li>after event fired.</li>");
            }
        });

    });
</script>
<script>
    $(function(){
        /*========================================================*/
        /* Ranges
        /*========================================================*/


        /*========================================================*/
        /* Trigger Examples
        /*========================================================*/
        $(' #demo20, #demo21').jrumble();


        var demoStart = function(){
            $('#demo20, #demo21').trigger('startRumble');
            setTimeout(demoStop, 300);
        };

        var demoStop = function(){
            $('#demo20, #demo21').trigger('stopRumble');
            setTimeout(demoStart, 1300);
        };

        demoStart();

    });
</script>
<script src="card/js/bootstrap.min.js"></script>
<script src="js/validator.js"></script>
<script src="card/js/imagezoom.js"></script>
<script defer src="card/js/jquery.flexslider.js"></script>
<script src="js/main.js"></script>
<?php //  $logs = \R::getDatabaseAdapter()
//    ->getDatabase()
//    ->getLogger();
//
//debug( $logs->grep( 'SELECT' ) ); ?>
</body>
</html>








