<div class="header-w3l">
    <h1>Оберіть тарифний план!</h1>
</div>
<div class="pricing">
    <div class="w3l-pricing-grids">
        <div class="agileits-pricing-grid first">
            <div class="pricing_grid">
                <div class="wthree-pricing-info pricing-top">
                    <h3 class="m">Безкоштовний</h3>
                    <p>$<span>0</span>/місяць</p>
                </div>
                <div class="pricing-bottom">
                    <div class="pricing-bottom-bottom">
                        <p><span class="fa fa-check"></span><span>Дилерство-</span> Знижки</p>
                        <p><span class="fa fa-check"></span><span>Видимість-</span> Пошук</p>
                        <p><span class="fa fa-times"></span><span>Реклама на сайті-</span> Банери</p>
                        <p><span class="fa fa-times"></span><span>Просування-</span>Google</p>
                        <p><span class="fa fa-times"></span><span>Просування- </span> Соц. мережі</p>
                        <p><span class="fa fa-times"></span><span>Персональна-</span>Аналітика</p>
                    </div>
                    <div class="one1">
                        <a class="order" href="price/">Дізнатись більше</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="agileits-pricing-grid second">
            <div class="pricing_grid">
                <div class="wthree-pricing-info pricing-top blue-top">
                    <h3 class="m">Стандарт</h3>
                    <p>$<span>5</span>/month</p>
                </div>
                <div class="pricing-bottom">
                    <div class="pricing-bottom-bottom blue-pricing-bottom-top">
                        <p><span class="fa fa-check"></span><span>Дилерство-</span> Знижки</p>
                        <p><span class="fa fa-check"></span><span>Видимість-</span> Пошук</p>
                        <p><span class="fa fa-check"></span><span>Реклама на сайті-</span> Банери</p>
                        <p><span class="fa fa-times"></span><span>Просування-</span>Google</p>
                        <p><span class="fa fa-times"></span><span>Просування- </span> Соц. мережі</p>
                        <p><span class="fa fa-times"></span><span>Персональна-</span>Аналітика</p>
                    </div>
                    <div class="one2">
                        <a class="order" href="price/">Дізнатись більше</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="agileits-pricing-grid third">
            <div class="pricing_grid">
                <div class="wthree-pricing-info pricing-top green-top">
                    <h3 class="m">Популярний</h3>
                    <p>$<span>10</span>/month</p>
                </div>
                <div class="pricing-bottom">
                    <div class="pricing-bottom-bottom green-pricing-bottom-top">
                        <p><span class="fa fa-check"></span><span>Дилерство-</span> Знижки</p>
                        <p><span class="fa fa-check"></span><span>Видимість-</span> Пошук</p>
                        <p><span class="fa fa-check"></span><span>Реклама на сайті-</span> Банери</p>
                        <p><span class="fa fa-check"></span><span>Просування-</span>Google</p>
                        <p><span class="fa fa-times"></span><span>Просування- </span> Соц. мережі</p>
                        <p><span class="fa fa-times"></span><span>Персональна-</span>Аналітика</p>
                    </div>
                    <div class="one3">
                        <a class="order" href="price/">Дізнатись більше</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="agileits-pricing-grid fourth">
            <div class="pricing_grid">
                <div class="wthree-pricing-info pricing-top yellow-top">
                    <h3 class="m">Професійний</h3>
                    <p>$<span>15</span>/month</p>
                </div>
                <div class="pricing-bottom">
                    <div class="pricing-bottom-bottom yellow-pricing-bottom-top">
                        <p><span class="fa fa-check"></span><span>Дилерство-</span> Знижки</p>
                        <p><span class="fa fa-check"></span><span>Видимість-</span> Пошук</p>
                        <p><span class="fa fa-check"></span><span>Реклама на сайті-</span> Банери</p>
                        <p><span class="fa fa-check"></span><span>Просування-</span>Google</p>
                        <p><span class="fa fa-check"></span><span>Просування- </span> Соц. мережі</p>
                        <p><span class="fa fa-check"></span><span>Персональна-</span>Аналітика</p>
                    </div>
                    <div class="one4">
                        <a class="order" href="price/">Дізнатись більше</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
