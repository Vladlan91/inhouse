<div id="page" class="recommends-page" style="background-image: url('images/bc5.jpg');">
    <div class="head-bread">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="/" style="color: #fb4c29; font-size: large; font-weight: 900;">Головна</a></li>
                <li class="active">Пошук послуг у вашому місті</li>
            </ol>
        </div>
    </div>
    <section id="wrapper">
        <div class="inner-block">
            <div id="recommend-list" class="clearfix">
                <div class="row" style="margin-left: 0; margin-right: 0;">
                    <div class="col-lg-6 col-md-5">
                        <h1 class="title no-bot-margin hclas">Пошук послуг</h1>
                    </div>
                    <div class="pading">
                        <div class="col-lg-6 col-md-7">
                            <div class="recommends-filter">
                                <form action="location/find" method="post" class="whereabouts">
                                    <div class=" col-lg-4 col-md-3 col-sm-4 col-xs-12">
                                        <label class="text-label">Оберіть місто</label>
                                        <div  style="position: relative">
                                            <select class="sel-rec" name="location" id="location">
                                                <option value="0">ВСІ МІСТА</option>
                                                <?php foreach ($location as $loc):?>
                                                    <option value="<?=$loc->id;?>"><?=$loc->name;?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12">
                                        <label class="text-label">Оберіть послугу</label>
                                        <div  style="position: relative">
                                            <select class="sel-rec" name="servisec" id="servisec">
                                                <option value="0">ВСІ ПОСЛУГИ</option>
                                                <?php foreach ($select as $item):?>
                                                    <option value="<?=$item->id;?>"><?=$item->title;?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-4 col-xs-12">
                                        <div class="submit">
                                            <button type="submit"  style="width: 100%;"  class="btn bb cont-btn">ПОШУК</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?php foreach ($pages as $page):?>
                    <div class="wr-item">
                        <div class="cell-item">
                            <p class="seves-p"><i class="fa fa-map-marker serves-market"></i>Місто <span class="serves-text-two"><?=$page->location_id;?></span></p>
                            <h3><?=$page->title;?></h3>
                            <img style="width: 50px; height: auto;" src="/images/montage.png" alt="<?=$page->title;?>">
                            <a class="more-link" href="location/<?=$page->alias;?>">Перейти</a>
                            <div class="company"><p class="seves-p">Послугу надає<span class="serves-text-two" ><?=$page->owner_id;?></span></p>
                                <div class="star-rating" style="top: 10px;">
                                    <div class="star-rating__wrap">
                                        <input class="star-rating__input" id="star-rating-5" type="radio" name="rating" value="5">
                                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-5" title="5 out of 5 stars"></label>
                                        <input class="star-rating__input" id="star-rating-4" type="radio" name="rating" value="4">
                                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-4" title="4 out of 5 stars"></label>
                                        <input class="star-rating__input" id="star-rating-3" type="radio" name="rating" value="3">
                                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-3" title="3 out of 5 stars"></label>
                                        <input class="star-rating__input" id="star-rating-2" type="radio" name="rating" value="2">
                                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-2" title="2 out of 5 stars"></label>
                                        <input class="star-rating__input" id="star-rating-1" type="radio" name="rating" value="1">
                                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-1" title="1 out of 5 stars"></label>
                                    </div>
                                </div></div>
                            <div class="photo">
                                <img src="/images/location/<?=$page->avatar;?>" alt="<?=$page->title;?>"></div>
                        </div>

                    </div>
                <?php endforeach;?>
            </div>
            <div>
                <?php if ($pagination->countpage > 1):?>
                    <?=$pagination;?>
                <?php endif; ?>
            </div>
    </section>
</div>