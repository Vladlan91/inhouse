<div id="page" class="recommends-page" style="background-image: url('images/bc5.jpg');">
    <div class="head-bread">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="/" style="color: #fb4c29; font-size: large; font-weight: 900;">Головна</a></li>
                <li class="active">Калькулятор</li>
            </ol>
        </div>
    </div>
    <section id="wrapper">
        <div class="inner-block">
            <div id="recommend-list" class="clearfix">
                <div class="row" style="margin-left: 0; margin-right: 0;">
                    <div class="col-md-6">
                        <h1 class="title no-bot-margin">Прорахунок вартості робіт</h1>
                    </div>
                    <div class="col-md-6">
                        <div class="recommends-filter">
                            <form action="" class="whereabouts">
                                <div class="col-md-12">
                                    <label class="text-label">Оберіть послугу</label>
                                    <div  style="position: relative">
                                        <select onchange="location = this.value;">
                                            <option value="recommend/">ВСІ ПОСЛУГИ</option>
                                            <?php foreach ($select as $item):?>
                                                <option value="recommend/find?id=<?=$item->id;?>" <?php if ($id == $item->id) echo 'selected';?>><?=$item->title;?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="wr-item">
                    <div class="cell-item">
                        <h3>Утеплення фасаду</h3>
                        <img style="width: 50px; height: auto;" src="/images/item3-1.png" alt="<?=$page->title;?>">
                        <a class="more-link" href="<?=$page->alias;?>">Розрахувати орієнтовну вартість</a><div class="photo">
                            <img src="/images/cal1.jpg" alt="<?=$page->title;?>"></div>
                    </div>
                </div>
                <div class="wr-item">
                    <div class="cell-item">
                        <h3>Декоративна наливна підлога</h3>
                        <img style="width: 50px; height: auto;" src="/images/item3-1.png" alt="<?=$page->title;?>">
                        <a class="more-link" href="<?=$page->alias;?>">Розрахувати орієнтовну вартість</a><div class="photo">
                            <img src="/images/cal2.jpg" alt="<?=$page->title;?>"></div>
                    </div>
                </div>
            </div>
            <div>
                <?php if ($pagination->countpage > 1):?>
                    <?=$pagination;?>
                <?php endif; ?>
            </div>
    </section>
</div>