<div class="head-bread">
    <div class="container">
        <ol class="breadcrumb">
            <?=$breadcrumbs;?>
        </ol>
    </div>
</div>
<div class="prdt">
    <div class="container">
        <div class="prdt-top">
            <div class="col-md-9 prdt-left">
                <div class="product-one">
                    <?php if ($products): ?>
                        <?php $curr = \inhouse\App::$app->getProperty('currency'); ?>
                        <?php foreach ($products as $product):?>
                            <div class="col-md-4 product-left p-left">
                                <div class="product-main simpleCart_shelfItem">
                                    <a href="product/<?= $product->alias; ?>" class="mask"><img class="img-responsive zoom-img" src="images/productavatar/<?= $product->img; ?>" alt="" /></a>
                                    <div class="product-bottom">
                                        <?php $product_text = pruning($product->content, 55); ?>
                                        <h3><?= $product->title ?></h3>
                                        <p><?= $product_text; ?></p>
                                        <h4><a id="productAdd" data-id="<?=$product->id; ?>" class="add-to-card-link" href="cart/add?id=<?= $product->id; ?>"><i></i></a> <span class=" item_price"><?= $curr['symbol_left']; ?><?= $product->price * $curr['value']; ?><?= $curr['symbol_right']; ?></span></h4>
                                    </div>
                                    <?php if ($product['hit']):?>
                                        <div class="srch">
                                            <span>НОВИНКА</span>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endforeach;?>
                        <div class="clearfix"></div>
                         <?php if ($pagination->countpage > 1):?>
                             <?=$pagination;?>
                        <?php endif; ?>
                    <?php else:?>
                    <h2>В даній категорії товари відсутні...</h2>
                        <img class="non-pro" src="images/item1-1.png" alt="В даній категорії товари відсутні">
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-3 grid-details">
                <div class="grid-addon">
                    <?php new \app\widgets\filter\Filter();?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
