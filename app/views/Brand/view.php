<div id="page" class="recommends-page" style="background-image: url('images/brands/<?=$brands->logo;?>');">
    <div class="head-bread">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="/" style="color: #fb4c29; font-size: large; font-weight: 900;">Головна</a></li>
                <li class="active" style="font-size: 34px;"><?= $brands->title;?></li>
            </ol>
        </div>
    </div>
    <div class="prdt">
        <div class="brand-conteiner">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-primary card-outline">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                    <img class="profile-user-img img-fluid img-circle" style="width: 100%; height: auto;" src="/images/brands/<?=$brands->img;?>" alt="User profile picture">
                                </div>
                                <h3 class="profile-username text-center"><?=$brands->title;?></h3>
                                    <p class="text-muted text-center">Виробник</p>
                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>Переглядів</b> <a class="float-right" style="font-weight: 900; font-size: 15px; text-decoration: none;">34568</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Додано в закладки:</b> <a class="float-right" style="font-weight: 900; font-size: 15px; text-decoration: none;">1123 чол.</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Замовлень:</b> <a class="float-right" style="font-weight: 900; font-size: 15px; text-decoration: none;">13 287 00 грн.</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header p-2">
                                <ul class="nav nav-pills">
                                        <li class="nav-item active"><a class="active show order"  style="font-size: 10px; margin-left: 5px;" href="#one" data-toggle="tab">Історія  &nbsp; компанії</a></li>
                                        <li class="nav-item"><a class="nav-link order"  style="font-size: 10px; margin-left: 5px;" href="#too" data-toggle="tab">Доставка</a></li>
                                        <li class="nav-item"><a class="nav-link order"  style="font-size: 10px; margin-left: 5px;" href="#three" data-toggle="tab">Контакти</a></li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="one" style="max-height: 100%;">
                                        <?=$brands->history;?>
                                    </div>
                                    <div class="tab-pane fade" id="too">
                                        <?=$brands->delivery;?>
                                    </div>
                                    <div class="tab-pane fade" id="three">
                                        <?=$brands->contact;?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header p-3">
                                <ul class="nav nav-pills">
                                    <li class="nav-item active"><a class="active show order"  style="font-size: 10px; margin-left: 5px;" href="#one-o" data-toggle="tab">Всі  &nbsp; товари &nbsp;компанії</a></li>
                                    <li class="nav-item"><a class="nav-link order"  style="font-size: 10px; margin-left: 5px;" href="#too-o" data-toggle="tab">Новинки</a></li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="one-o" style="max-height: 100%;">
                                        <?php foreach ($products as $product):?>
                                            <div class="col-md-3 product-left">
                                                <div class="product-main simpleCart_shelfItem">
                                                    <a href="product/<?= $product->alias; ?>" class="mask"><img class="img-responsive zoom-img" src="images/productavatar/<?= $product->img; ?>" alt="" /></a>
                                                    <div class="product-bottom">
                                                        <h3><?= $product->title ?></h3>
                                                        <h4><a id="productAdd" data-id="<?=$product->id; ?>" class="add-to-card-link" href="cart/add?id=<?= $product->id; ?>"><i></i></a> <span class=" item_price"><?= $curr['symbol_left']; ?><?= $hit->price * $curr['value']; ?><?= $curr['symbol_right']; ?></span></h4>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach;?>
                                    </div>
                                    <div class="tab-pane fade" id="too-o">
                                        <?php foreach ($hits as $hit):?>
                                            <div class="col-md-3 product-left">
                                                <div class="product-main simpleCart_shelfItem">
                                                    <a href="product/<?= $hit->alias; ?>" class="mask"><img class="img-responsive zoom-img" src="images/productavatar/<?= $hit->img; ?>" alt="" /></a>
                                                    <div class="product-bottom">
                                                        <h3><?= $hit->title ?></h3>
                                                        <h4><a id="productAdd" data-id="<?=$hit->id; ?>" class="add-to-card-link" href="cart/add?id=<?= $hit->id; ?>"><i></i></a> <span class=" item_price"><?= $curr['symbol_left']; ?><?= $hit->price * $curr['value']; ?><?= $curr['symbol_right']; ?></span></h4>
                                                    </div>
                                                    <?php if ($hit['hit']):?>
                                                        <div class="srch">
                                                            <span>НОВИНКА</span>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        <?php endforeach;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
