<div id="page" class="recommends-page" style="background-image: url('images/33.jpg');">
    <div class="head-bread">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="/" style="color: #fb4c29; font-size: large; font-weight: 900;">Головна</a></li>
                <li class="active">Виробники</li>
            </ol>
        </div>
    </div>
    <div class="prdt">
        <div class="brand-conteiner">
            <div class="container">
            <div class="prdt-top">
                <div class="col-md-12 prdt-left">
                    <div class="user-one">
                        <?php foreach ($brands as $user):?>
                            <div class="col-md-5 col-md-offset-1" style="margin-top: 30px;">
                                <!-- Widget: user widget style 1 -->
                                <div class="card card-widget widget-user">
                                    <div class="widget-user-header text-white">
                                        <a  style="text-decoration: none;" href="brand/<?= $user->alias;?>"><img class="img-circle" style="width: 100%; height: 100%; position: relative;" src="images/brands/<?= $user->logo;?>" alt="User Avatar"></a>
                                        <a  style="text-decoration: none;" href="brand/<?= $user->alias;?>"><h3 class="widget-user-username" style="color: rgba(232,81,74,.9); position: absolute;" ><?= $user->title;?></h3></a>
                                        <h5 class="widget-user-desc" style="color: white;">Будівництво</h5>
                                    </div>
                                    <div class="widget-user-image">
                                    </div>
                                    <div class="card-footer" style="z-index: 100">
                                        <div class="star-rating__wrap">
                                            <input class="star-rating__input" id="star-rating-5" type="radio" name="rating" value="5">
                                            <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-5" title="5 out of 5 stars"></label>
                                            <input class="star-rating__input" id="star-rating-4" type="radio" name="rating" value="4">
                                            <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-4" title="4 out of 5 stars"></label>
                                            <input class="star-rating__input" id="star-rating-3" type="radio" name="rating" value="3">
                                            <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-3" title="3 out of 5 stars"></label>
                                            <input class="star-rating__input" id="star-rating-2" type="radio" name="rating" value="2">
                                            <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-2" title="2 out of 5 stars"></label>
                                            <input class="star-rating__input" id="star-rating-1" type="radio" name="rating" value="1">
                                            <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-1" title="1 out of 5 stars"></label>
                                        </div>
                                        <a  style="text-decoration: none;" href="brand/<?= $user->alias;?>"><img class="img-circle" style="margin-top: -34px;width: 50px; height: 50px; float: right; border-radius: 50%;" src="images/brands/<?= $user->img;?>" alt="User Avatar"></a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                    <div class="clearfix"></div>
                        <div class="col-md-11 col-md-offset-1 prdt-left" style="margin-top: 30px;">
                        <?php if ($pagination->countpage > 1):?>
                            <?=$pagination;?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        </div>
    </div>
