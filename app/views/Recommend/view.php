<div class="head-bread">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Головна</a></li>
            <li class="active"><?= $servises->title;?></li>
        </ol>
    </div>
</div>
<div class="showcase-grid">
    <div class="container">
        <div class="col-md-9">
            <h1 class="title no-bot-margin"style="margin-bottom: 30px;" ><?= $servises->title;?></h1>
            <?= $servises->content;?>
            <?php if ($servises->owner_id == 0 && isset($_SESSION['user']) && $_SESSION['user']['role']!= 'user' && $_SESSION['user']['role']!= 'admin'):?>
                <div class="card-body p-0 demo-box" id="demo21">
                    <ul class="products-list product-list-in-card pl-2 pr-2">
                        <li class="item" style="padding: 20px;">
                            <div class="product-img">
                                <img src="images/logo_black.png" style="width: auto;" alt="Product Image" class="img-size-50">
                            </div>
                            <div class="product-info infos">
                                <a href="javascript:void(0)" class="product-title"><span>В цьому місці МОЖЕ БУТИ ВАША РЕКЛАМА!!</span>
                                    <span class="badge float-right"  style="color: white"><i class="fa fa-phone" style="margin-right: 10px;"></i>+38(096) 456 36 14</span></a>
                                <span class="product-description">
                        Послуги з напрямку <?= $servises->title;?> телефонуйте!
                      </span>
                            </div>
                        </li>
                    </ul>
                </div>
            <?php elseif ($servises->owner_id != 0):?>
                <div class="card-body p-0 demo-box">
                    <ul class="products-list product-list-in-card pl-2 pr-2">
                        <li class="item" style="padding: 20px;">
                            <div class="product-img">
                                <?php if ($owner->avatar):?>
                                <a href="<?=$owner->role;?>/<?=$owner->alias;?>"><img src="images/useravatar/<?=$owner->avatar;?>" style="width: auto;" alt="Product Image" class="img-size-50"></a>
                                <?php else:?>
                                <a href="<?=$owner->role;?>/<?=$owner->alias;?>"><img src="images/avatar-profile-icon-man.jpg" style="width: auto;" alt="Product Image" class="img-size-50"></a>
                                <?php endif;?>
                            </div>
                            <div class="product-info infos">
                                <a href="javascript:void(0)" class="product-title"><span> ЗАМОВТЕ!!</span>
                                    <span class="badge float-right" style="color: white"><i class="fa fa-phone" style="margin-right: 10px;"></i><?=$owner->phone;?></span></a>
                                <span class="product-description">
                        Консультацію за телефоном!
                      </span>
                            </div>
                        </li>
                    </ul>
                </div>
            <?php else:?>
                <div class="card-body p-0 demo-box">
                    <ul class="products-list product-list-in-card pl-2 pr-2">
                        <li class="item" style="padding: 20px;">
                            <div class="product-img">
                                <img src="images/logo_black.png" style="width: auto;" alt="Product Image" class="img-size-50">
                            </div>
                            <div class="product-info infos">
                                <a href="javascript:void(0)" class="product-title"><span> ЗАМОВТЕ!!</span>
                                    <span class="badge float-right" style="color: white"><i class="fa fa-phone" style="margin-right: 10px;"></i>+38(096) 456 36 14</span></a>
                                <span class="product-description">
                        Консультацію за телефоном!
                      </span>
                            </div>
                        </li>
                    </ul>
                </div>
            <?php endif;?>
        </div>
        <div class="col-md-3" style="border-bottom: 2px solid #e8514a;">
            <?php if ($servises_all):?>
            <h4 class="sidebar_inhouse" style="border-bottom: 2px solid #e8514a; padding-bottom: 20px;">Інші рекомендації цієї компанії:</h4>
            <div class="col-md-12 sidebar_block widget">

                <?php foreach ($servises_all as $servises):?>
                    <ul class="popular-posts">
                        <li>
                            <a href="https://shody.top/porady-domashn-ogo-energoaudy-tu/">
                                <div class="rec">
                                    <img width="100" height="80" src="/images/recommend/<?=$servises->avatar;?>" class="" alt="<?=$servises->title;?>">
                                </div>
                            </a><p><a href="https://shody.top/porady-domashn-ogo-energoaudy-tu/"></a>
                                <a href="recommend/<?=$servises->alias;?>"><?=$servises->title;?></a>
                            </p>
                            <em><?=$servises->view;?> Перегляд...</em>
                        </li>
                    </ul>
                <?php endforeach;?>
            </div>
            <div class="clearfix"></div>
            <?php else:?>
            <h4 class="sidebar_inhouse" style="border-bottom: 2px solid #e8514a; padding-bottom: 20px;">Інші рекомендації даної категорії:</h4>
            <div class="col-md-12 sidebar_block widget">

                <?php foreach ($servises_anather as $anather):?>
                    <ul class="popular-posts">
                        <li>
                            <a href="https://shody.top/porady-domashn-ogo-energoaudy-tu/">
                                <div class="rec">
                                    <img width="100" height="80" src="/images/recommend/<?=$anather->avatar;?>" class="" alt="<?=$anather->title;?>">
                                </div>
                            </a><p><a href="https://shody.top/porady-domashn-ogo-energoaudy-tu/"></a>
                                <a href="recommend/<?=$anather->alias;?>"><?=$anather->title;?></a>
                            </p>
                            <em><?=$anather->view;?> Перегляд...</em>
                        </li>
                    </ul>
                <?php endforeach;?>
            </div>
            <div class="clearfix"></div>
            <?php endif;?>
        </div>
    </div>
</div>