<div class="head-bread">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Головна</a></li>
            <li class="active">Контакти</li>
        </ol>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<div class="contact">
    <div class="container">
        <div class="col-lg-8">
        <h3>Звязатись з нами</h3>
            <div class="contact-content">
                <form action="contact/add" data-toggle="validator" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" name="name" class="form-control" id="name" placeholder="Ваше і'мя" required>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" name="email" class="form-control" id="email" placeholder="Ваш емейл" required>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" name="phone" class="form-control" id="phone" placeholder="Ваш телефон" required>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="serves_id">Оберіть послугу</label>
                        <select class="form-control" name="serves_id" id="serves_id">
                            <?php new \app\widgets\menu\Menu([
                                'tpl' => WWW . '/menu/select.php',
                                'container' =>'select',
                                'table'=>'servises',
                                'cash' => 0,
                                'cashKey' => 'select_cat',
                            ]);?>
                        </select>
                    </div>
                    <div class="clear"> </div>
                    <div>
                        <textarea id="content" name="content" cols="30" rows="5" style="height: 100px" class="form-control" placeholder="Ваше повідомлення" required></textarea>
                    </div>
                    <div class="submit">
                        <input class="btn btn-default cont-btn" type="submit" value="НАДІСЛАТИ " />
                    </div>
                </form>
            </div>
        </div>
        <div class="col-lg-4">
            <h3>Наша адреса</h3>
            <p>Email: we.inhouse@gmail.com<br>
            <p>м. Львів.</p>
            <p>м. Івано-Франківськ.</p>
                Tel: +38(096)-456-3-614</p>
            <p>INHOUSE дистрибьютор виробників будівельних матеріалів, які зарекомендували себе на ринку України.</p>
            <p></p>
        </div>
    </div>
</div>
