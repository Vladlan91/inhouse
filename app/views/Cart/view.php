<?php if (!empty($_SESSION['cart'])):?>
<div id="carts">
        <div class="modal-bodys">
            <div class="check">
                <div class="container">
                    <div class="col-md-3 cart-total">
                        <a class="continue" href="#">Продовжити</a>
                        <div class="price-details">
                            <h3>Деталі замовлення</h3>
                            <span>Сума замовлення</span>
                            <span class="total11"><?=$_SESSION['cart.currency']['symbol_left'] . $_SESSION['cart.sum'] . $_SESSION['cart.currency']['symbol_right'];?></span>
                            <span>Знижка</span>
                            <span class="total1">0%</span>
                            <span>Сума  знижки </span>
                            <span class="total1">0.00</span>
                            <div class="clearfix"></div>
                        </div>
                        <hr class="featurette-divider">
                        <ul class="total_price">
                            <li class="last_price"> <h4>До сплати</h4></li>
                            <li class="last_price"><span><?=$_SESSION['cart.currency']['symbol_left'] . $_SESSION['cart.sum'] . $_SESSION['cart.currency']['symbol_right'];?></span></li>
                            <div class="clearfix"> </div>
                        </ul>
                        <div class="clearfix"></div>
                        <a class="order" href="cart/checkout">Оформити замовлення</a>
                    </div>
                    <div class="col-md-9 cart-items">
                        <h1>Кількість товару ( <?=$_SESSION['cart.qty'];?> )</h1>
                        <?php foreach ($_SESSION['cart'] as $id => $item):?>
                            <div class="cart-header">
                                <span class="close" data-id="<?=$id;?>" aria-hidden="true">&times;</span>
                                <div class="cart-sec simpleCart_shelfItem">
                                    <div class="cart-item cyc">
                                        <a href="product/<?=$item['alias'];?>"><img src="images/productavatar/<?=$item['img'];?>" class="img-responsive cart-w" alt="<?=$item['title'];?>"/></a>
                                    </div>
                                    <div class="cart-item-info">
                                        <ul class="qty">
                                            <li><p>Торгова марка : <?=$item['brand_title'];?></p></li>
                                            <li><p>Модефікація : <?=$item['modef'];?></p></li>
                                            <li><p>Кількість : <?=$item['qty'];?></p></li>
                                            <li><p>Ціна :  <?= $curr['symbol_left']; ?><?=$item['price'];?><?= $curr['symbol_right']; ?></p></li>
                                        </ul>
                                        <div class="delivery">
                                            <a href="product/<?=$item['alias'];?>"><p><?=$item['title'];?></p></a>
                                            <span>Доставка протягом 5 робочих днів</span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        <?php endforeach;?>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <?php if (!empty($_SESSION['user'])):?>

            <?php else:?>
                <div class="login">
                    <div class="container">
                        <div class="login-grids">
                            <div class="col-md-6 log">
                                <h3>Вхід</h3>
                                <div class="strip"></div>
                                <p>Ласкаво просимо, будь ласка, введіть наступні дані, щоб продовжити.</p>
                                <form method="post" action="user/login" id="login" role="form" data-toggle="validator">
                                    <h5>Ваше імя:</h5>
                                    <div class="form-group has-feedback">
                                        <input type="text" class="form-control" name="login" value="" id="login" placeholder="Login" required>
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <h5>Пароль:</h5>
                                    <div class="form-group has-feedback">
                                        <input type="password" class="form-control" name="password" id="password" value="" placeholder="Password" required><br>
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <input type="submit" value="Увійти">

                                </form>
                                <a href="#">Забули пароль ?</a>
                            </div>
                            <div class="col-md-6 login-right">
                                <h3>Створити акаунт</h3>
                                <div class="strip"></div>
                                <p>Ставши зареєстрованим користувачем Ви матимете нагоду отримувати накопичувальну знижку по товарам які придбаєте у нас, розмір знижки залежитиме від величини замовлень!</p>
                                <a href="user/signup" class="button">Звичайний акаунт</a>
                                <a href="user/signup" class="button">Майстр</a>
                                <a href="user/signup" class="button">Будівельник</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            <?php endif;?>

            <?php else:?>
            <div class="head-bread">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="/">Головна</a></li>
                        <li class="active">Оформлення замовлення</li>
                    </ol>
                </div>
            </div>
            <h2>Кошик порожній...</h2>
            <img class="non-pro" src="images/non-pro.png" alt="В даній категорії товари відсутні">
        <?php endif;?>
    </div>
</div>
