<?php if (!empty($_SESSION['cart'])):?>
<div class="check">
    <div class="container">
        <div class="col-md-3 cart-total">
            <a class="continue" class="close" data-dismiss="modal" aria-label="Close"href="">Продовжити</a>
            <div class="price-details">
                <h3>Деталі замовлення</h3>
                <span>Сума замовлення</span>
                <span class="total11"><?=$_SESSION['cart.currency']['symbol_left'] . $_SESSION['cart.sum'] . $_SESSION['cart.currency']['symbol_right'];?></span>
                <span>Знижка</span>
                <span class="total1">0%</span>
                <span>Сума  знижки </span>
                <span class="total1">0.00</span>
                <div class="clearfix"></div>
            </div>
            <hr class="featurette-divider">
            <ul class="total_price">
                <li class="last_price"> <h4>До сплати</h4></li>
                <li class="last_price"><span><?=$_SESSION['cart.currency']['symbol_left'] . $_SESSION['cart.sum'] . $_SESSION['cart.currency']['symbol_right'];?></span></li>
                <div class="clearfix"> </div>
            </ul>
            <div class="clearfix"></div>
            <a class="order" href="cart/view">Оформити замовлення</a>
        </div>
        <div class="col-md-9 cart-items">
            <h1>Кількість товару ( <?=$_SESSION['cart.qty'];?> )</h1>
<?php foreach ($_SESSION['cart'] as $id => $item):?>
            <div class="cart-header">
                <span class="close" data-id="<?=$id;?>" aria-hidden="true">&times;</span>
                <div class="cart-sec simpleCart_shelfItem">
                    <div class="cart-item cyc">
                        <a href="product/<?=$item['alias'];?>"><img src="images/productavatar/<?=$item['img'];?>" class="img-responsive cart-w" alt="<?=$item['title'];?>"/></a>
                    </div>
                    <div class="cart-item-info">
                        <ul class="qty">
                            <li><p>Торгова марка : <?=$item['brand_title'];?></p></li>
                            <li><p>Модефікація : <?=$item['modef'];?></p></li>
                            <li><p>Кількість : <?=$item['qty'];?></p></li>
                            <li><p>Ціна :  <?= $curr['symbol_left']; ?><?=$item['price'];?><?= $curr['symbol_right']; ?></p></li>
                        </ul>
                        <div class="delivery">
                            <a href="product/<?=$item['alias'];?>"><p><?=$item['title'];?></p></a>
                            <span>Доставка протягом 5 робочих днів</span>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
<?php endforeach;?>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php else:?>
    <h3>Порожній кошик</h3>
<?php endif;?>



