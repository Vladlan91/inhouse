<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Замовлення №<?=$order_id;?> від <?=$orde['name'];?></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/">Головна</a></li>
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/order">Список замовлень</a></li>
                    <li class="breadcrumb-item">Замовлення <?=$order_id;?></li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="callout callout-info">
                                    <h5><i class="fa fa-info"></i> Примітка:</h5>
                                    На даній сторінці можливий друк, для цього натисніть кнопку "Print" яка знаходиться нижче!.
                                </div>
                                <!-- Main content -->
                                <div class="invoice p-3 mb-3">
                                    <!-- title row -->
                                    <div class="row">
                                        <div class="col-12">
                                            <h4>
                                                <img src="../images/logo_black.png" alt="">
                                                <small class="float-right">Вих №001/2018  від 25.05.2018р</small>
                                            </h4>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- info row -->
                                    <div class="row invoice-info">
                                        <div class="col-sm-4 invoice-col">
                                            Рахунок виданий
                                            <address>
                                                <strong>INHOUSE, Inniti.</strong><br>
                                                м. Львів<br>
                                                вул. Кравчика 225<br>
                                                Телефон: +38(096)-456-3-64<br>
                                                Email: we.inhouse@gmail.com
                                            </address>
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-sm-4 invoice-col">
                                            Кому
                                            <address>
                                                <strong>Іваніцькому Владиславу</strong><br>
                                                м. Івано-Франківсь<br>
                                                вул. Фрунзе 335<br>
                                                Телефон: +38(075)-346-3-73<br>
                                                Email: john.doe@example.com
                                            </address>
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-sm-4 invoice-col">
                                            <b>Адреса доставки: м. Івано-Франківсь</b><br>
                                            вул. Фрунзе 335<br>
                                            <b>ID: Замовлення:</b> 4F3S8J<br>
                                            <b>Дата Замовлення:</b> 2/22/2014<br>
                                            <b>Account:</b> Vladlan91
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->

                                    <!-- Table row -->
                                    <div class="row">
                                        <div class="col-12">
                                            <p class="lead">Деталі замовлення</p>

                                            <div class="table-responsive">
                                                <table class="table">
                                                    <tr>
                                                        <th style="width:50%">Номер замовлення:</th>
                                                        <td>№ <?=$orde['id'];?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Дата замовлення:</th>
                                                        <td><?=$orde['data'];?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Дата оформлення:</th>
                                                        <?php if ($orde['update_at'] != 0):?>
                                                            <td><?=$orde['update_at'];?></td>
                                                        <?php else:?>
                                                            <?php $today = date("Y-m-d H:i:s");  ?>
                                                            <td><?=$today;?></td>
                                                        <?php endif;?>
                                                    </tr>
                                                    <tr>
                                                        <th>Кількість позицій в замовлені:</th>
                                                        <td><?=count($order_products);?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Сума замовленя:</th>
                                                        <td><?=$orde['sum'];?><?=$orde['currency'];?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Ім'я замовника:</th>
                                                        <td><?=$orde['name'];?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Cтатус замовлення:</th>
                                                        <form  method="get" action="<?=ADMIN;?>/order/changed?id=<?=$orde['id'];?>">
                                                        <?php if ($orde['status'] == 0):?>
                                                            <td><select name="opt" class="form-control" style="background-color: #dc3545 ">
                                                                <option value="0">не опрацьоване</option>
                                                                <option value="1">оформлене</option>
                                                                <option value="2">відправлене</option>
                                                                <option value="3">оплачене</option>
                                                            </select></td>
                                                        <?php elseif ($orde['status'] == 1):?>
                                                            <td><select name="opt" class="form-control" style="background-color: #ffc107">
                                                                    <option value="1">оформлене</option>
                                                                    <option value="2">відправлене</option>
                                                                    <option value="3">оплачене</option>
                                                                    <option value="0">не опрацьоване</option>
                                                                </select></td>
                                                        <?php elseif ($orde['status'] == 2):?>
                                                            <td><select name="opt" class="form-control" style="background-color:  #007bff">
                                                                    <option value="2">відправлене</option>
                                                                    <option value="3">оплачене</option>
                                                                    <option value="0">не опрацьоване</option>
                                                                    <option value="1">оформлене</option>
                                                                </select></td>
                                                        <?php else:?>
                                                            <td><select name="opt" class="form-control" style="background-color: #28a745">
                                                                    <option value="3">оплачене</option>
                                                                    <option value="0">не опрацьоване</option>
                                                                    <option value="1">оформлене</option>
                                                                    <option value="2">відправлене</option>
                                                                </select></td>
                                                        <?php endif;?>
                                                    </tr>
                                                    <?php if ($orde['status'] == 3):?>
                                                        <th>Виконавець замовлення:</th>
                                                        <td><select name="executor" class="form-control">
                                                                <option>Оберіть виконавця</option>
                                                                <?php foreach ($users as $user):?>
                                                                    <option value="<?=$user->id;?>"><?=$user->name;?></option>
                                                                <?php endforeach;?>
                                                            </select></td>
                                                    <?php endif;?>
                                                    <tr>
                                                        <th>Примітка замовника:</th>
                                                        <th><textarea class="form-control" rows="3" placeholder="Enter ..."></textarea></th>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="card">
                                                <div class="card-header no-border">
                                                </div>
                                                <div class="card-body p-0">
                                                    <table class="table table-striped table-valign-middle">
                                                        <thead>
                                                        <tr>
                                                            <th>Назва:</th>
                                                            <th>ID продуту:</th>
                                                            <th>Кількість:</th>
                                                            <th>Ціна:</th>
                                                            <th>#</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php $qty = 0; foreach ($order_products as $product):?>
                                                            <tr>
                                                                <td>
                                                                    <img src="../images/<?=$product->img;?>" alt="Product 1" class="img-circle img-size-32 mr-2">
                                                                    <?=$product->title;?>
                                                                </td>
                                                                <td><?=$product->product_id;?></td>
                                                                <td><?=$product->qty; $qty += $product->qty;?></td>
                                                                <td><?=$product->price;?><?=$orde['currency'];?></td>
                                                                <td>
                                                                    <a href="#" class="text-muted">
                                                                        <i class="fa fa-search"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        <?php endforeach;?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <p class="lead">Amount Due 2/22/2014</p>

                                            <div class="table-responsive">
                                                <table class="table">
                                                    <tr>
                                                        <th style="width:50%">Ціна всього:</th>
                                                        <td><?=$orde['sum'];?><?=$orde['currency'];?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Знижка (0%)</th>
                                                        <td>0<?=$orde['currency'];?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Кількість:</th>
                                                        <td><?=$qty;?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Всього до сплати:</th>
                                                        <td><?=$orde['sum'];?><?=$orde['currency'];
                                                        $_SESSION['order_id_change'] = $orde['id'];?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <a style="margin-right: 15px;" class="btn btn-danger float-right delete" href="<?=ADMIN;?>/order/delete?id=<?=$orde['id'];?>"><i class="fa fa-arrow-circle-down"></i>Видалити</a>&emsp;
                                            <input type="submit" style="margin-right: 15px;" class="btn btn-success float-right update" value="Обновити">
                                            </form>&emsp;
                                        </div>
                                    </div>
                                    <div class="row no-print">
                                        <div class="col-12">
                                            <a href="../../../../public/adminLTE/invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
</section>