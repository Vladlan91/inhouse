<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Список замовлень</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/">Головна</a></li>
                    <li class="breadcrumb-item">Список замовлень</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box mb-3">
            <span class="info-box-icon bg-danger elevation-1"><i class="fa fa-bullhorn"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Не опрацьовані</span>
                <span class="info-box-number">41 шт.</span>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box mb-3">
            <span class="info-box-icon bg-warning elevation-1"><i class="fa fa-cart-arrow-down"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Оформлене</span>
                <span class="info-box-number">41 шт.</span>
            </div>
        </div>
    </div>
    <div class="clearfix hidden-md-up"></div>
    <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box">
            <span class="info-box-icon bg-info elevation-1"><i class="fa fa-car"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Відправлені</span>
                <span class="info-box-number">41 шт.</span>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box mb-3">
            <span class="info-box-icon bg-success elevation-1"><i class="fa fa-cc-mastercard"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Оплачені</span>
                <span class="info-box-number">41 шт.</span>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Замовлення</h3>
                        <div class="card-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control float-right" placeholder="Пошук по ID">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-warning"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <table class="table">
                            <tr>
                                <th style="width: 10px">ID</th>
                                <th>Замовник</th>
                                <th>Статус</th>
                                <th>Сума</th>
                                <th>Дата створення</th>
                                <th>Дата зміни статусу</th>
                                <th>Час реагування</th>
                                <th>Перегляд</th>
                            </tr>
                            <?php foreach ($orders as $order):?>
                                <?php
                                if ($order['update_at'] == 0){
                                    $today = date("Y-m-d H:i:s");
                                    $curDate = new DateTime($today); //Сравниваемая дата (текущая) Сюда нужно передавать дату для сравнения DateTime('2017-06-22').
                                }else{
                                    $curDate = new DateTime($order['update_at']); //Сравниваемая дата (текущая) Сюда нужно передавать дату для сравнения DateTime('2017-06-22').
                                }
                                $diffDate = new DateTime();
                                $diffDate = $diffDate->modify($order['data']); //Дата на 1 месяц меньше текущей (можно '-30 day')
                                $difference = $curDate->diff($diffDate);
                                $dat = $difference->format('%R%a дні'); // return $difference->format('%R%a'); // Возвращаем разницу дней в строковом виде "-31"
                                $dat = substr("$dat", 1, 1);
                                $dat .=' д.';
                                ?>
                            <tr>
                                <td><?=$order['id'];?></td>
                                <td><a href="<?= ADMIN ?>/user/edit?id=<?=$order['user_id'];?>"><?=$order['name'];?></a></td>
                                <?php if ($order['status'] == 0):?>
                                    <td><span class="badge bg-danger">не опрацьоване</span></td>
                                    <?php elseif ($order['status'] == 1):?>
                                    <td><span class="badge bg-warning">оформлене</span></td>
                                    <?php elseif ($order['status'] == 2):?>
                                    <td><span class="badge bg-primary">відправлене</span></td>
                                    <?php else:?>
                                    <td><span class="badge bg-success">оплачене</span></td>
                                <?php endif;?>
                                <td><span class="badge bg-warning"><?=$order['sum'];?> <?=$order['currency'];?></span></td>
                                <td><?=$order['data'];?></td>
                                <td><?=$order['update_at'];?></td>
                                <?php if ($dat == 0):?>
                                    <td><span class="badge bg-success"><?=$dat;?></span></td>
                                    <?php elseif ($dat == 1):?>
                                    <td><span class="badge bg-primary"><?=$dat;?></span></td>
                                    <?php elseif ($dat == 2):?>
                                    <td><span class="badge bg-warning"><?=$dat;?></span></td>
                                    <?php elseif ($dat > 2):?>
                                    <td><span class="badge bg-danger"><?=$dat;?></span></td>
                                    <?php endif;?>
                                <td><a href="<?= ADMIN; ?>/order/view?id=<?=$order['id'];?>" class="text-muted"><i class="fa fa-pencil"></i></a>&ensp;&ensp;&ensp;
                                    <a class="text-muted delete" href="<?=ADMIN;?>/order/delete?id=<?=$order['id'];?>"><i class="fa fa-close"></i></a></td>

                            </tr>
                            <?php endforeach;?>
                        </table>
                    </div>
                    <div class="card-footer clearfix">
                        <?php if ($pagination->countpage > 1):?>
                            <?=$pagination;?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>