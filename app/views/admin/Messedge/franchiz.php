<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Список запитів на франшизу</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/">Головна</a></li>
                    <li class="breadcrumb-item">Список запитів на франшизу</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<div class="container">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Список</h3>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <tbody><tr>
                    <th style="width: 10px">ID</th>
                    <th>Ім'я</th>
                    <th>Email</th>
                    <th>Телефон</th>
                    <th>Дата</th>
                </tr>
                <?php foreach ($massedge as $item):?>
                    <tr>
                        <td><?=$item->id;?></td>
                        <td><?=$item->name;?></td>
                        <td><?=$item->email;?></td>
                        <td><?=$item->phone;?></td>
                        <td><?=$item->date;?></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <div class="card-footer clearfix">
            <?php if ($pagination->countpage > 1):?>
                <?=$pagination;?>
            <?php endif; ?>
        </div>
    </div>
</div>