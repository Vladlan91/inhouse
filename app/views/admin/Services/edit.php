<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Редагування категорії: <?=$servises->title;?></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/">Головна</a></li>
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/services/">Список категорій</a></li>
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/services/list">Список</a></li>
                    <li class="breadcrumb-item">Редагування категорії</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Заповніть форму!</h3>
                        </div>
                        <div class="card-body">
                            <form action="<?= ADMIN ?>/services/edit" method="post" data-toggle="validator">
                                <div class="box-body">
                                    <div class="form-group has-feedback">
                                        <label for="title">Назва категорії</label>
                                        <input type="text" name="title" class="form-control" id="title" placeholder="Name" value="<?=h($servises->title);?>" required>
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="parent_id">Категорія до якої відноситься</label>
                                        <select class="form-control" name="parent_id" id="parent_id">
                                            <?php new \app\widgets\menu\Menu([
                                                'tpl' => WWW . '/menu/select.php',
                                                'container' =>'select',
                                                'cash' => 0,
                                                'table'=>'servises',
                                                'cashKey' => 'select_cat',
                                                'prepend' => '<option value="5">Батьківська категорія</option>',

                                            ]);?>
                                        </select>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="owner_id">Власник сторінки</label>
                                        <select class="form-control" name="owner_id" id="owner_id">
                                            <option>Оберіть власника</option>
                                            <?php foreach ($users as $user):?>
                                                <option value="<?=$user->id;?>"<?php if ($user->id == $servises->owner_id) echo 'selected';?>><?=$user->name;?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="bought_to">Дата бронювання по...</label>
                                        <input type="date" id="bought_to" name="bought_to" class="form-control" value="<?=$servises->bought_to;?>">
                                    </div>
                                    <div class="form-group has-feedback bg-white" style="border-radius: 5px">
                                        <label for="content" style="margin: 15px;">Контент</label>
                                        <textarea id="editor1" name="content" cols="30" rows="5" style="height: 100px" class="form-control" placeholder="Контент"><?=$servises->content;?></textarea>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="alias">Url</label>
                                        <input type="text" name="alias" class="form-control" id="alias" placeholder="Url" value="<?=$servises->alias;?>">
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="price">Місячна вартість сторінки</label>
                                        <input type="text" name="price" class="form-control" id="price" placeholder="Ціна" value="<?=$servises->price;?>">
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="keywords">Ключові слова для пошуку</label>
                                        <input type="text" name="keywords" class="form-control" id="keywords" placeholder="Keywords" value="<?=h($servises->keywords);?>">
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="description">Опис категорії</label>
                                        <input type="text" name="description" class="form-control" id="description" placeholder="Description" value="<?=h($servises->description);?>">
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="status" id="status" <?php if ($servises->status == 1) echo 'checked';?>>
                                        <label class="form-check-label" for="status">Статус активний</label>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <input type="hidden" name="monthe_view" value="<?=$servises->monthe_view;?>">
                                    <input type="hidden" name="view" value="<?=$servises->view;?>">
                                    <input type="hidden" name="last_month" value="<?=$servises->last_month;?>">
                                    <input type="hidden" name="id" value="<?=$servises->id;?>">
                                    <button type="submit" class="btn btn-info">Внести зміни</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>