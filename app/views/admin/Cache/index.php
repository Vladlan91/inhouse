<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Очистка кешу</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/">Головна</a></li>
                    <li class="breadcrumb-item">Очистка кешу</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<div class="container">
    <div class="card">
    <div class="card-header">
        <h3 class="card-title">Bordered Table</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table class="table table-bordered">
            <tbody><tr>
                <th style="width: 10px">#</th>
                <th>Назва по ключу</th>
                <th>Призначення і опис</th>
                <th style="width: 40px">Дія</th>
            </tr>
            <tr>
                <td>1.</td>
                <td>Категорій</td>
                <td>
                    Категорії товарів, час кешування 1 година.
                </td>
                <td><a class="text-muted delete-cash" href="<?=  ADMIN;?>/cache/delete?key=category"><i class="fa fa-close"></i></a></td>
            </tr>
            <tr>
                <td>2.</td>
                <td>Фільтри</td>
                <td>
                    Фільтри товарів, час кешування 1 година.
                </td>
                <td><a class="text-muted delete-cash" href="<?=  ADMIN;?>/cache/delete?key=filter"><i class="fa fa-close"></i></a></td>
            </tr>
            <tr>
                <td>3.</td>
                <td>Послуги</td>
                <td>
                    Список послуг, час кешування 1 година.
                </td>
                <td><a class="text-muted delete-cash" href="<?=  ADMIN;?>/cache/delete?key=servises"><i class="fa fa-close"></i></a></td>
            </tr>
            </tbody></table>
    </div>
    <!-- /.card-body -->
    </div>
</div>