<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Редагування валюти <?= h($currency->title);?> </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/">Головна</a></li>
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/currency">Список валют</a></li>
                    <li class="breadcrumb-item">Редагування валюти</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Заповніть форму!</h3>
                        </div>
                        <div class="card-body">
                            <form action="<?= ADMIN ?>/currency/edit" method="post" data-toggle="validator">
                                <div class="box-body">
                                    <div class="form-group has-feedback">
                                        <label for="title">Назва валюти</label>
                                        <input type="text" name="title" class="form-control" id="title" required value="<?= h($currency->title);?>">
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="code">Код валюти</label>
                                        <input type="text" name="code" class="form-control" id="code" required value="<?= h($currency->code);?>">
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="symbol_left">Cимвол з ліва</label>
                                        <input type="text" name="symbol_left" class="form-control" id="symbol_left" value="<?= h($currency->symbol_left);?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="symbol_right">Cимвол з права</label>
                                        <input type="text" name="symbol_right" class="form-control" id="symbol_right"  value="<?= h($currency->symbol_right);?>">
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="value">Значення</label>
                                        <input type="text" name="value" class="form-control" id="value"  data-error="Допускаються цілі числа і з крапкою" pattern="^[0-9.]{1,}$" value="<?= h($currency->value);?>">
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="base">
                                            <input type="checkbox" name="base" style="margin-right: 20px;" <?php if ($currency->base) echo 'checked';?>>Базова валюта</label>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <input type="hidden" name="id" value="<?=$currency->id;?> ">
                                    <button type="submit" class="btn btn-info">Зберегти зміни</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>