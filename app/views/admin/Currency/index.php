<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Cписок валют</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/">Головна</a></li>
                    <li class="breadcrumb-item">Cписок валют</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<div class="container">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Список</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table class="table table-bordered">
                <tbody><tr>
                    <th style="width: 10px">ID</th>
                    <th>Назва</th>
                    <th>Код</th>
                    <th>Значення</th>
                    <th>Статус</th>
                    <th style="width: 100px">Дія</th>
                </tr>
                <?php foreach ($currency as $item):?>
                <tr>
                    <td><?=$item->id;?></td>
                    <td><?=$item->title;?></td>
                    <td><?=$item->code;?></td>
                    <td><?=$item->value;?></td>
                    <td><?=$item->base ? "<span class=\"badge bg-info\">On</span>" : "<span class=\"badge bg-danger\">Off</span>";?></td>
                    <td><a class="text-muted" href="<?=ADMIN;?>/currency/edit?id=<?=$item->id;?>"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                        <a class="text-muted delete" href="<?=  ADMIN;?>/currency/delete?id=<?=$item->id;?>"><i class="fa fa-close"></i></a></td>
                    <?php endforeach;?>
                </tr>
                </tbody></table>
        </div>
        <!-- /.card-body -->
    </div>
</div>