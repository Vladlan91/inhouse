<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Профайл користувача № <?=$user->id;?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/">Головна</a></li>
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/user">Список користувачів</a></li>
                    <li class="breadcrumb-item">Профайл користувача</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <?php if ($user->avatar):?>
                            <img class="profile-user-img img-fluid img-circle" src="/images/useravatar/<?=$user->avatar;?>" alt="User profile picture">
                            <?php else:?>
                                <img class="profile-user-img img-fluid img-circle" src="/images/avatar-profile-icon-man.jpg" alt="User profile picture">
                            <?php endif;?>
                        </div>
                        <h3 class="profile-username text-center"><?=$user->name;?></h3>
                        <?php if ($user->role == 'user'):?>
                        <p class="text-muted text-center">Користувач</p>
                        <?php elseif ($user->role == 'admin'):?>
                        <p class="text-muted text-center">Адміністратор</p>
                        <?php elseif ($user->role == 'master'):?>
                        <p class="text-muted text-center">Майстер</p>
                        <?php else:?>
                        <p class="text-muted text-center">Будівельник</p>
                        <?php endif;?>
                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>Переглядів</b> <a class="float-right">454 чол.</a>
                            </li>
                            <li class="list-group-item">
                                <b>Додано в закладки:</b> <a class="float-right">1123 чол.</a>
                            </li>
                            <li class="list-group-item">
                                <b>Замовлень:</b> <a class="float-right">13 287 00 грн.</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Інформація</h3>
                    </div>
                    <div class="card-body">
                    <?php if ($user->role == 'user' || $user->role == 'admin'):?>
                        <hr>
                        <strong><i class="fa fa-map-marker mr-1"></i>Адреса</strong>
                        <p class="text-muted"><?=$user->address;?></p>
                        <hr>
                    <?php else:?>
                        <strong><i class="fa fa-book mr-1"></i>м. Надання послуг</strong>
                        <p class="text-muted">
                            B.S. in Computer Science from the University of Tennessee at Knoxville
                        </p>
                        <hr>
                        <strong><i class="fa fa-map-marker mr-1"></i>Адреса</strong>
                        <p class="text-muted"><?=$user->address;?></p>
                        <hr>
                        <strong><i class="fa fa-pencil mr-1"></i>Сфера діяльності</strong>
                        <p class="text-muted">
                            <span class="tag tag-danger">UI Design</span>
                            <span class="tag tag-success">Coding</span>
                            <span class="tag tag-info">Javascript</span>
                            <span class="tag tag-warning">PHP</span>
                            <span class="tag tag-primary">Node.js</span>
                        </p>
                        <hr>
                        <strong><i class="fa fa-file-text-o mr-1"></i>Примітки</strong>
                        <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
                    <?php endif;?>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header p-2">
                        <ul class="nav nav-pills">
                            <li class="nav-item"><a class="nav-link active show" href="#activity" data-toggle="tab">Список замовлень</a></li>
                            <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Загальна інформація</a></li>
                            <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Налаштування</a></li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane active show" id="activity">
                            <?php if (!$orders):?>
                                <h4>Даний користувач ще не оформляв замовлення...</h4>
                                <img class="non-pro" src="/images/non-pro.png" alt="В даній категорії товари відсутні">
                            <?php else:?>
                                <section class="content">
                                    <div class="card-body p-0">
                                        <table class="table">
                                            <tr>
                                                <th style="font-size: small">Статус</th>
                                                <th style="font-size: small">Сума</th>
                                                <th style="font-size: small">Дата створення</th>
                                                <th style="font-size: small">Дата зміни статусу</th>
                                                <th style="font-size: small">Час реагування</th>
                                                <th style="font-size: small">Перегляд</th>
                                            </tr>
                                            <?php foreach ($orders as $order):?>
                                                <?php
                                                if ($order['update_at'] == 0){
                                                    $today = date("Y-m-d H:i:s");
                                                    $curDate = new DateTime($today); //Сравниваемая дата (текущая) Сюда нужно передавать дату для сравнения DateTime('2017-06-22').
                                                }else{
                                                    $curDate = new DateTime($order['update_at']); //Сравниваемая дата (текущая) Сюда нужно передавать дату для сравнения DateTime('2017-06-22').
                                                }
                                                $diffDate = new DateTime();
                                                $diffDate = $diffDate->modify($order['data']); //Дата на 1 месяц меньше текущей (можно '-30 day')
                                                $difference = $curDate->diff($diffDate);
                                                $dat = $difference->format('%R%a дні'); // return $difference->format('%R%a'); // Возвращаем разницу дней в строковом виде "-31"
                                                $dat = substr("$dat", 1, 1);
                                                $dat .=' д.';
                                                ?>
                                                <tr>
                                                    <?php if ($order['status'] == 0):?>
                                                        <td><span class="badge bg-danger">не опрацьоване</span></td>
                                                    <?php elseif ($order['status'] == 1):?>
                                                        <td><span class="badge bg-warning">оформлене</span></td>
                                                    <?php elseif ($order['status'] == 2):?>
                                                        <td><span class="badge bg-primary">відправлене</span></td>
                                                    <?php else:?>
                                                        <td><span class="badge bg-success">оплачене</span></td>
                                                    <?php endif;?>
                                                    <td><span class="badge bg-warning"><?=$order['sum'];?> <?=$order['currency'];?></span></td>
                                                    <td style="font-size: small"><?=$order['data'];?></td>
                                                    <td style="font-size: small"><?=$order['update_at'];?></td>
                                                    <?php if ($dat == 0):?>
                                                        <td style="font-size: small"><span class="badge bg-success"><?=$dat;?></span></td>
                                                    <?php elseif ($dat == 1):?>
                                                        <td style="font-size: small"><span class="badge bg-primary"><?=$dat;?></span></td>
                                                    <?php elseif ($dat == 2):?>
                                                        <td style="font-size: small"><span class="badge bg-warning"><?=$dat;?></span></td>
                                                    <?php elseif ($dat > 2):?>
                                                        <td style="font-size: small"><span class="badge bg-danger"><?=$dat;?></span></td>
                                                    <?php endif;?>
                                                    <td><a href="<?= ADMIN; ?>/order/view?id=<?=$order['id'];?>" class="text-muted"><i class="fa fa-search"></i></a>&ensp;&ensp;&ensp;
                                                        <a class="text-muted delete" href="<?=ADMIN;?>/order/delete?id=<?=$order['id'];?>"><i class="fa fa-close"></i></a></td>

                                                </tr>
                                            <?php endforeach;?>
                                        </table>
                                    </div>
                                </section>
                            <?php endif;?>
                            </div>
                            <div class="tab-pane" id="timeline">
                                <ul class="timeline timeline-inverse">
                                    <li>
                                        <i class="fa fa-envelope bg-primary"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i></span>
                                            <h3 class="timeline-header">Відсотки по брендам</h3>
                                            <div class="timeline-body">

                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <i class="fa fa-user bg-info"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-comments bg-warning"></i>

                                        <div class="timeline-item">
                                            <h3 class="timeline-header">Відгуки</h3>
                                            <div class="timeline-body">

                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <i class="fa fa-camera bg-purple"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">Галерея робіт</h3>
                                            <div class="timeline-body">
                                                <img src="http://placehold.it/150x100" alt="..." class="margin">
                                                <img src="http://placehold.it/150x100" alt="..." class="margin">
                                                <img src="http://placehold.it/150x100" alt="..." class="margin">
                                                <img src="http://placehold.it/150x100" alt="..." class="margin">
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-pane" id="settings">
                                <form class="form-horizontal" action="<?=ADMIN;?>/user/edit?id=<?=$user->id;?>" method="post" data-toggle="validator">
                                    <div class="form-group">
                                        <label for="login" class="col-sm-2 control-label">Логін</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="login" name="login" value="<?=h($user->login);?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="col-sm-2 control-label">Пароль</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="password" name="password" placeholder="Введіть новий пароль" ">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-2 control-label">Ім'я</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="name" name="name" value="<?=h($user->name);?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="col-sm-2 control-label">Емейл</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control has-feedback" name="email" id="email" value="<?=h($user->email);?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="role" class="col-sm-2 control-label">Роль</label>
                                        <div class="col-sm-10">
                                            <select name="role" id="role" class="form-control">
                                                <option value="user"<?php if ($user->role == 'user') echo ' selected';?>>Користувач</option>
                                                <option value="master"<?php if ($user->role == 'master') echo ' selected';?>>Майстер</option>
                                                <option value="builder"<?php if ($user->role == 'builder') echo ' selected';?>>Будівельник</option>
                                                <option value="admin"<?php if ($user->role == 'admin') echo ' selected';?>>Адміністратор</option>
                                            </select>
                                        </div>
                                    </div>
                                    <?php if ($user->role == 'user' || $user->role == 'admin'):?>
                                    <div class="form-group">
                                        <label for="address" class="col-sm-2 control-label">Адреса</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control has-feedback" id="address" name="address" value="<?=h($user->address);?>">
                                        </div>
                                    </div>
                                    <?php else:?>
                                    <div class="form-group">
                                        <label for="address" class="col-sm-2 control-label">Адреса</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control has-feedback" id="address" name="address" value="<?=h($user->address);?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="description" class="col-sm-2 control-label">Опис</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" name="description"  id="description" placeholder="Опис"><?=h($user->description);?></textarea>
                                        </div>
                                    </div>
                                        <div class="form-group">
                                            <label for="phone" class="col-sm-2 control-label">Телефон</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control"  name="phone"  id="phone" placeholder="Телефон"><?=h($user->phone);?></textarea>
                                            </div>
                                        </div>
                                    <?php endif;?>
                                    <div class="box-footer">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <input type="hidden" name="avatar" value="<?=$user->avatar;?>">
                                            <input type="hidden" name="back_avatar" value="<?=$user->back_avatar;?>">
                                            <input type="hidden" name="alias" value="<?=$user->alias;?>">
                                            <input type="hidden" name="id" value="<?=$user->id;?>">
                                            <button type="submit" class="btn btn-danger">Внести зміни</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>