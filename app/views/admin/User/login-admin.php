<div class="login-box">
    <div class="login-logo">
        <a href="<?= PATH ?>"><b>IN</b>HOUSE</a>
    </div>
    <?php if (isset($_SESSION['error'])):?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fa fa-ban"></i> Помилка!</h5>
            <?=$_SESSION['error']; unset($_SESSION['error']);?>
        </div>
    <?php endif;?>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Увійти в адмін панель</p>

            <form action="<?=ADMIN;?>/user/login-admin" method="post">
                <div class="form-group has-feedback">
                    <input type="text" name="login" class="form-control" placeholder="Login">
                    <span class=" form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control" placeholder="Password">
                    <span class=" form-control-feedback"></span>
                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Увійти</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>