<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Список користувачів</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/">Головна</a></li>
                    <li class="breadcrumb-item">Список користувачів</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Користуваі</h3>
                <div class="card-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control float-right" placeholder="Пошук по ID">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-warning"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body p-0">
                <table class="table">
                    <tr>
                        <th style="width: 10px">ID</th>
                        <th>Логін</th>
                        <th>Email</th>
                        <th>Ім'я</th>
                        <th>Роль</th>
                        <th>Перегляд</th>
                    </tr>
                    <?php foreach ($users as $user):?>
                        <tr>
                            <td><?=$user->id;?></td>
                            <td><?=$user->login;?></td>
                            <td><?=$user->email;?></td>
                            <td><?=$user->name;?></td>
                            <td><span class="badge" style="background-color: #1ed950; font-size: 12px;"><?=$user->role;?></span></td>
                            <td><a href="<?= ADMIN; ?>/user/edit?id=<?=$user->id;?>" class="text-muted"><i class="fa fa-pencil"></i></a>&ensp;&ensp;&ensp;
                        </tr>
                    <?php endforeach;?>
                </table>
            </div>
            <div class="card-footer clearfix">
                <?php if ($pagination->countpage > 1):?>
                    <?=$pagination;?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>


