<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Новий користувач <?=$user->id;?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/">Головна</a></li>
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/user">Список користувачів</a></li>
                    <li class="breadcrumb-item">Новий користувач</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="card bg-white">
                        <div class="card-header">
                            <h3 class="card-title">Заповніть форму!</h3>
                        </div>
                        <div class="card-body">
                            <div class="tab-pane" id="settings">
                                <form method="post" action="/user/signup" data-toggle="validator" id="signup" role="form">
                                    <div class="form form-group has-feedback">
                                        <ul>
                                            <li class="text-info">Login: </li>
                                            <input type="text" class="form-control" name="login" value="<?=isset($_SESSION['form_data']['login']) ? h($_SESSION['form_data']['login']) : '';?>" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        </ul>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <ul>
                                            <li class="text-info">Password: </li>
                                            <input type="password" class="form-control" name="password" value="<?=isset($_SESSION['form_data']['password']) ? h($_SESSION['form_data']['password']): '' ;?>" id="password" placeholder="Password" data-error="Пароль повинен бути більше 6 символів" data-minLenght="6" required>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <div class="help-block with-errors"></div>
                                        </ul>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <ul>
                                            <li class="text-info">Name: </li>
                                            <input type="text" class="form-control" name="name" value="<?=isset($_SESSION['form_data']['name']) ? h($_SESSION['form_data']['name']) : '';?>"  required>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        </ul>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <ul>
                                            <li class="text-info">Email: </li>
                                            <input type="text" class="form-control" name="email" value="<?=isset($_SESSION['form_data']['email']) ? h($_SESSION['form_data']['email']) : '';?>"  required>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        </ul>
                                    </div>
                                    <div class=" form-group has-feedback">
                                        <ul>
                                            <li class="text-info">Address:</li>
                                            <input type="text" class="form-control" name="address" value="<?=isset($_SESSION['form_data']['address']) ? h($_SESSION['form_data']['address']): '';?>"  required>
                                        </ul>
                                    </div>
                                    <div class="form-group">
                                        <label for="role" class="col-sm-2 control-label">Роль</label>
                                        <div class="col-sm-12">
                                            <select name="role" id="role" class="form-control">
                                                <option value="user">Користувач</option>
                                                <option value="master">Майстер</option>
                                                <option value="builder">Будівельник</option>
                                                <option value="admin">Адміністратор</option>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="submit" class="btn btn-primary" value="Register Now">
                                </form>
                                <?php if (isset($_SESSION['form_data'])); unset($_SESSION['form_data']);?>
                            </div>
                        </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>