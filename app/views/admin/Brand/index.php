<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Виробники</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/">Головна</a></li>
                    <li class="breadcrumb-item">Список виробників</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Bordered Table</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table class="table table-bordered">
                <tbody><tr>
                    <th style="width: 10px; font-size: small;">ID</th>
                    <th style="font-size: small">Назва</th>
                    <th style="font-size: small">Дата підписання</th>
                    <th style="font-size: small">Дата завершення</th>
                    <th style="font-size: small">Плановий обіг</th>
                    <th style="font-size: small">У валюті</th>
                    <th style="font-size: small">Виконання дилерських зобов'язань</th>
                    <th style="width: 40px; font-size: small;">%</th>
                    <th style="width: 40px; font-size: small;">Дія</th>
                </tr>
                <?php foreach ($brands as $brand):?>
                <tr>
                    <td><?=$brand->id;?></td>
                    <td><?=$brand->title;?></td>
                    <td><?=$brand->date_start;?></td>
                    <td><?=$brand->date_finish;?></td>
                    <td><?=$brand->plan;?></td>
                    <td>UAH</td>
                    <td>
                        <div class="progress progress-xs">
                            <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
                        </div>
                    </td>
                    <td><span class="badge bg-danger">55%</span></td>
                    <td><a href="<?= ADMIN;?>/brand/edit?id=<?=$brand->id;?>" class="text-muted"><i class="fa fa-search"></i></a></td>
                </tr>
                <?php endforeach;?>
                </tbody></table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
            <?php if ($pagination->countpage > 1):?>
                <?=$pagination;?>
            <?php endif; ?>
        </div>
    </div>
</div>