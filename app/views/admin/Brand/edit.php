<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Новий виробник </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/">Головна</a></li>
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/brand">Список виробників</a></li>
                    <li class="breadcrumb-item">Редагування  </li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Заповніть форму!</h3>
                        </div>
                        <div class="card-body">
                            <form action="<?= ADMIN ?>/brand/edit" method="post" data-toggle="validator">
                                <div class="box-body">
                                    <div class="form-group has-feedback">
                                        <label for="title">Назва компанії</label>
                                        <input type="text" name="title" class="form-control" id="title" placeholder="Назва" value="<?=$brand->title;?>" required>
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="plan">Плановий обіг</label>
                                        <input type="text" name="plan" class="form-control" id="plan" placeholder="Обіг товару, грн" value="<?=$brand->plan;?>">
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="date_start">Дата підписання</label>
                                        <input type="date" id="date_start" name="date_start" class="form-control" value="<?=$brand->date_start;?>">
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="date_finish">Дата завершення</label>
                                        <input type="date" id="date_finish" name="date_finish" class="form-control" value="<?=$brand->date_finish;?>">
                                    </div>
                                    <div class="form-group has-feedback bg-white" style="border-radius: 5px">
                                        <label for="history" style="margin: 15px;">Історія компанії</label>
                                        <textarea id="editor1" name="history" cols="30" rows="5" style="height: 100px" class="form-control" placeholder="Призначення"><?=h($brand->history);?></textarea>
                                    </div>
                                    <div class="form-group has-feedback bg-white" style="border-radius: 5px">
                                        <label for="delivery" style="margin: 15px;">Доставка</label>
                                        <textarea id="editor2" name="delivery" cols="30" rows="5" style="height: 100px" class="form-control" placeholder="Призначення"><?=h($brand->delivery);?></textarea>
                                    </div>
                                    <div class="form-group has-feedback bg-white" style="border-radius: 5px">
                                        <label for="contact" style="margin: 15px;">Контакти</label>
                                        <textarea id="editor3" name="contact" cols="30" rows="5" style="height: 100px" class="form-control" placeholder="Призначення"><?=h($brand->contact);?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="card card-warning file-upload">
                                                    <div class="card-header">
                                                        <h3 class="card-title">Базове зображення</h3>
                                                    </div>
                                                    <div class="card-body">
                                                        <div id="img" class="btn bg-danger" data-url="brand/add-image" data-name="img">Оберіть файл</div>
                                                        <p style="color: #0c0c0c; padding-top: 20px;">Бажані розміри 700x700 px;</p>
                                                    </div>
                                                    <div class="img" style="padding: 10px;">
                                                        <img class="margin" src="/images/brands/<?=$brand->img;?>" alt="<?=h($brand->title);?>" style="max-height: 150px; border-radius: 5px; vertical-align: middle;">
                                                    </div>
                                                    <div class="overlay">
                                                        <i class="fa fa-refresh fa-spin"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="card card-warning file-upload">
                                                    <div class="card-header">
                                                        <h3 class="card-title">Галерея</h3>
                                                    </div>
                                                    <div class="card-body">
                                                        <div id="logo" class="btn bg-danger" data-url="brand/add-image" data-name="logo">Оберіть файл</div>
                                                        <p style="color: #0c0c0c; padding-top: 20px;">Бажані розміри 700x700 px;</p>
                                                    </div>
                                                    <div class="logo" style="padding: 10px;">
                                                        <img class="margin del-item" src="/images/brands/<?=$brand->logo;?>" style="max-height: 150px; border-radius: 5px; vertical-align: middle; cursor: pointer" alt="<?=h($brand->title);?>">
                                                    </div>
                                                    <div class="overlay">
                                                        <i class="fa fa-refresh fa-spin"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <input type="hidden" name="id" value="<?=$brand->id;?>">
                                    <input type="hidden" name="img" value="<?=$brand->img;?>">
                                    <input type="hidden" name="logo" value="<?=$brand->logo;?>">
                                    <button type="submit" class="btn btn-info">Редагувати виробника</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>