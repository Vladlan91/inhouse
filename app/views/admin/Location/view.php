<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Детальна інформація про послугу</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/">Головна</a></li>
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/location/list">Список послуг у містах</a></li>
                    <li class="breadcrumb-item">Детальна інформація<?=$order_id;?></li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <!-- Main content -->
                                <div class="invoice p-3 mb-3">
                                    <!-- title row -->
                                    <div class="row">
                                        <div class="col-12">
                                            <h4>
                                                <img src="../images/logo_black.png" alt="">
                                            </h4>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                        <!-- /.col -->
                                    <!-- /.row -->

                                    <!-- Table row -->
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <tr>
                                                        <th style="width:50%">Назва послуги:</th>
                                                        <td><?=$servises->title;?></td>
                                                    </tr>
                                                    <tr>
                                                        <th style="width:50%">Категорія до якої видноситься:</th>
                                                        <td><?=$parent->title;?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Місто надання послуги:</th>
                                                        <td><?=$location->name;?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Орендована по:</th>
                                                        <td><?=$servises->bought_to;?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Власник сторінки:</th>
                                                        <td><?=$user->name;?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Вартість сторінки:</th>
                                                        <td><?=$servises->price;?>&nbsp;грн/м.</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Переглядів за поточний місяць:</th>
                                                        <td><?=$servises->monthe_view;?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Переглядів в місяць:</th>
                                                        <td><?=$servises->last_month;?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Всього переглядів:</th>
                                                        <td><?=$servises->view;?></td>
                                                    </tr>
                                                    <tr>
                                                        <th style="margin-top: 20px; font-size: 26px;">Залежні сторінки:</th>
                                                        <th></th>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="card">
                                                <div class="card-header no-border">
                                                </div>
                                                <div class="card-body p-0">
                                                    <table class="table table-striped table-valign-middle">
                                                        <thead>
                                                        <tr style="font-size: small;">
                                                            <th>Назва послуги:</th>
                                                            <th>Кількість переглядів:</th>
                                                            <th>Переглядів за місяць:</th>
                                                            <th>Ціна:</th>
                                                            <th>Дія</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php  foreach ($type as $item):?>
                                                            <tr style="font-size: small;">
                                                                <td><?=$item->title;?></td>
                                                                <td><?=$item->view;?></td>
                                                                <td><?=$item->monthe_view;?></td>
                                                                <td><?=$item->price;?></td>
                                                                <td>
                                                                    <a href="#" class="text-muted">
                                                                        <i class="fa fa-search"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        <?php endforeach;?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <p class="lead">Прибутковість сторінки</p>

                                            <div class="table-responsive">
                                                <table class="table" style="font-size: small;">
                                                    <tr>
                                                        <th style="width:50%">Вартість сторінки:</th>
                                                        <td><?=$servises->price;?>&nbsp;грн/м.</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Кількість проплат</th>
                                                        <td>32</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Прибуток від оренди:</th>
                                                        <td>5 900 грн.</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Прибуток від залежних сторінок:</th>
                                                        <td>3 230&nbsp;грн/м.</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Прибуток від напрямку всього:</th>
                                                        <td>9 130&nbsp;грн/м.</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <input type="submit" style="margin-right: 15px;" class="btn btn-success float-right" value="Обновити">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
</section>