<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Послуги У містах</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/">Головна</a></li>
                    <li class="breadcrumb-item">Список послуг У містах</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Послуги</h3>
                        <div class="card-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control float-right" placeholder="Пошук по ID">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-warning"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <table class="table">
                            <tr>
                                <th class="small" style="width: 10px; font-size: 12px;">ID</th>
                                <th class="small" style=" font-weight: 900; font-size: 12px;" >Назва</th>
                                <th class="small" style=" font-weight: 900; font-size: 12px;" >Місто</th>
                                <th class="small" style=" font-weight: 900; font-size: 12px;" >Власник</th>
                                <th class="small" style=" font-weight: 900; font-size: 12px;" >Дата завершення броні</th>
                                <th class="small" style=" font-weight: 900; font-size: 12px;" >Днів до завершення</th>
                                <th class="small" style=" font-weight: 900; font-size: 12px;" >Переглядів</th>
                                <th class="small" style=" font-weight: 900; font-size: 12px;" >За місяць</th>
                                <th class="small" style=" font-weight: 900; font-size: 12px;" >Ціна</th>
                                <th class="small" style=" font-weight: 900; font-size: 12px;" >Дія</th>
                            </tr>
                            <?php foreach ($pages as $page):?>

                                <?php

                                $today = date("Y-m-d");
                                $curDate = new DateTime($today); //Сравниваемая дата (текущая) Сюда нужно передавать дату для сравнения DateTime('2017-06-22').
                                $diffDate = new DateTime();
                                $diffDate = $diffDate->modify($page['bought_to']); //Дата на 1 месяц меньше текущей (можно '-30 day')
                                $difference = $curDate->diff($diffDate);
                                $dat = $difference->format('%R%a д.'); // return $difference->format('%R%a'); // Возвращаем разницу дней в строковом виде "-31"
                                $dat = substr("$dat", 1, 10);
                                ?>
                                <tr>
                                    <td><?=$page['id'];?></td>
                                    <td class="small" ><?=$page['title'];?></td>
                                    <td class="small" ><?=$page['town'];?></td>
                                    <?php if ($page['owner_id'] != 0):?>
                                        <td class="small" ><a href="<?= ADMIN ?>/user/edit?id=<?=$page['owner_id'];?>"><?=$page['owner_id'];?></a></td>
                                    <?php else:?>
                                        <td style="color: #dc3545; font-weight: 900; font-size: 10px">INHOUSE</td>
                                    <?php endif;?>
                                    <td class="small" ><?=$page['bought_to'];?></td>
                                    <?php if ($dat < 7):?>
                                        <td class="small" ><span class="badge bg-danger"><?=$dat;?></span></td>
                                    <?php else:?>
                                        <td class="small" ><span class="badge bg-primary"><?=$dat;?></span></td>
                                    <?php endif;?>
                                    <td class="small" ><?=$page['view'];?></td>
                                    <td class="small" ><?=$page['monthe_view'];?></td>
                                    <td class="small" ><?=$page['price'];?></td>
                                    <td><a href="<?= ADMIN; ?>/location/edit?id=<?=$page['id'];?>" class="text-muted"><i class="fa fa-pencil"></i></a>&ensp;&ensp;
                                        <a href="<?= ADMIN; ?>/location/view?id=<?=$page['id'];?>" class="text-muted"><i class="fa fa-search"></i></a>&ensp;&ensp;&ensp;


                                </tr>
                            <?php endforeach;?>
                        </table>
                    </div>
                    <div class="card-footer clearfix">
                        <?php if ($pagination->countpage > 1):?>
                            <?=$pagination;?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
