<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Редагування категорії: <?=$category->title;?></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/">Головна</a></li>
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/category">Список категорій</a></li>
                    <li class="breadcrumb-item">Редагування категорії</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Заповніть форму!</h3>
                        </div>
                        <div class="card-body">
                            <form action="<?= ADMIN ?>/category/edit" method="post" data-toggle="validator">
                                <div class="box-body">
                                    <div class="form-group has-feedback">
                                        <label for="title">Назва категорії</label>
                                        <input type="text" name="title" class="form-control" id="title" placeholder="Name" value="<?=h($category->title);?>" required>
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="parent_id">Категорія до якої відноситься</label>
                                        <select class="form-control" name="parent_id" id="parent_id">
                                            <?php new \app\widgets\menu\Menu([
                                                'tpl' => WWW . '/menu/select.php',
                                                'container' =>'select',
                                                'cashKey' => 'select_cat',
                                                'prepend' => '<option value="2">Батьківська категорія</option>',

                                            ]);?>
                                        </select>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="keywords">Ключові слова для пошуку</label>
                                        <input type="text" name="keywords" class="form-control" id="keywords" placeholder="Keywords" value="<?=h($category->keywords);?>">
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="description">Опис категорії</label>
                                        <input type="text" name="description" class="form-control" id="description" placeholder="Description" value="<?=h($category->description);?>">
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <input type="hidden" name="id" value="<?=$category->id;?>">
                                    <button type="submit" class="btn btn-info">Внести зміни</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>