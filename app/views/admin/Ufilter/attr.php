<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Список атрібутів</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/">Головна</a></li>
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/ufilter/">Група фільтрів</a></li>
                    <li class="breadcrumb-item">Фільтри</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<div class="container">
    <div class="card">
        <div class="card-header">
            <a href="<?= ADMIN ?>/ufilter/attr-add" class="btn btn-info">Додати атрібут</a>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <th>Назва </th>
                    <th>Група</th>
                    <th style="width: 100px">Дія</th>
                </tr>
                <?php foreach ($attrs as $id => $item):?>
                    <tr>
                        <td><?=$item['value'];?> </td>
                        <td><?=$item['title'];?> </td>
                        <td><a class="text-muted" href="<?=ADMIN;?>/ufilter/attr-edit?id=<?=$id;?>"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                            <a class="text-muted delete" href="<?=ADMIN;?>/ufilter/attr-delete?id=<?=$id;?>"><i class="fa fa-close"></i></a></td>
                    </tr>
                <?php endforeach;?>
                </tbody></table>
        </div>
        <div class="card-footer">
        </div>
    </div>
</div>