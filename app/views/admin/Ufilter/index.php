<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Група фільтрів</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/">Головна</a></li>
                    <li class="breadcrumb-item">Група фільтрів</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<div class="container">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Групи</h3>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <th>Назва </th>
                    <th style="width: 100px">Дія</th>
                </tr>
                <?php foreach ($attrs_group as $item):?>
                <tr>
                    <td><?=$item->title;?> </td>
                    <td><a class="text-muted" href="<?=ADMIN;?>/ufilter/group-edit?id=<?=$item->id;?>"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                    <a class="text-muted delete" href="<?=ADMIN;?>/ufilter/group-delete?id=<?=$item->id;?>"><i class="fa fa-close"></i></a></td>
                </tr>
                <?php endforeach;?>
                </tbody></table>
        </div>
        <div class="card-footer">
            <a href="<?= ADMIN ?>/ufilter/group-add" class="btn btn-info">Додати групу</a>
        </div>
    </div>
</div>