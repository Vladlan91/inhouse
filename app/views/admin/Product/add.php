<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Новий товар </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/">Головна</a></li>
                    <li class="breadcrumb-item"><a href="<?= ADMIN ?>/product">Список товарів</a></li>
                    <li class="breadcrumb-item">Новий товар</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Заповніть форму!</h3>
                        </div>
                        <div class="card-body">
                            <form action="<?= ADMIN ?>/product/add" method="post" data-toggle="validator" id="add">
                                <div class="box-body">
                                    <div class="form-group has-feedback">
                                        <label for="title">Назва товару</label>
                                        <input type="text" name="title" class="form-control" value="<?php isset($_SESSION['form_data']['title']) ? h($_SESSION['form_data']['title']) : null;?>" id="title" placeholder="Назав товару" required>
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="category_id">Категорія до якої відноситься товар</label>
                                        <select class="form-control" name="category_id" id="category_id">
                                            <?php new \app\widgets\menu\Menu([
                                                'tpl' => WWW . '/menu/select.php',
                                                'container' =>'select',
                                                'cash' => 0,
                                                'cashKey' => 'select_cat',
                                                'prepend' => '<option>Оберіть категорію</option>',

                                            ]);?>
                                        </select>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="keywords">Ключові слова для пошуку товару</label>
                                        <input type="text" name="keywords" class="form-control" id="keywords" placeholder="Keywords" value="<?php isset($_SESSION['form_data']['keywords']) ? h($_SESSION['form_data']['keywords']) : null;?>" >
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="brand_id">Бренд товару</label>
                                        <select class="form-control" name="brand_id" id="category_id"> value="<?php isset($_SESSION['form_data']['brand_id']) ? h($_SESSION['form_data']['brand_id']) : null;?>" >
                                            <option>Оберіть назву виробника</option>
                                            <?php foreach ($brands as $brand):?>
                                                <option value="<?=$brand->id;?>"><?=$brand->title;?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                    <h6 class="card-title">Модефікація товару</h6>
                                    <div data-role="dynamic-fields">
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <label class="sr-only" for="field-name">Модефікація товару</label>
                                                <input type="text" class="form-control"  name="modification[]" id="field-title" placeholder="Назва модефікації">
                                            </div>
                                            <span>-</span>
                                            <div class="form-group">
                                                <label class="sr-only" for="field-value">Field Value</label>
                                                <input type="text" class="form-control" name="modification[]" id="field-price" placeholder="Ціна модефікації">
                                            </div>
                                            <button class="btn btn-danger" data-role="remove">
                                                <span class="fa fa-remove"></span>
                                            </button>
                                            <button class="btn btn-primary" data-role="add">
                                                <span class="fa fa-plus"></span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="description">Опис товару</label>
                                        <input type="text" name="description" class="form-control" id="description" placeholder="Description"  value="<?php isset($_SESSION['form_data']['description']) ? h($_SESSION['form_data']['description']) : null;?>">
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="status" id="status" checked>
                                        <label class="form-check-label" for="status">Статус активний</label>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="hit" id="hit" checked>
                                        <label class="form-check-label" for="hit">Новий товар</label>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="description">Ціна</label>
                                        <input type="text" name="price" class="form-control" pattern="^[0-9.]{1,}$" id="price" placeholder="Ціна"  value="<?php isset($_SESSION['form_data']['price']) ? h($_SESSION['form_data']['price']) : null;?>" required data-error="Допускаються тільки цифри">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group has-feedback bg-white" style="border-radius: 5px">
                                        <label for="content" style="margin: 15px;">Призначення</label>
                                        <textarea id="editor1" name="content" cols="30" rows="5" style="height: 100px" class="form-control" placeholder="Призначення"><?php isset($_SESSION['form_data']['content']) ? $_SESSION['form_data']['content'] : null;?></textarea>
                                    </div>
                                    <div class="form-group has-feedback bg-white" style="border-radius: 5px"">
                                        <label for="properties"style="margin: 15px;">Характеристики</label>
                                        <textarea name="properties" cols="30" rows="5" class="form-control" id="editor2" placeholder="Характеристики"><?php isset($_SESSION['form_data']['properties']) ? $_SESSION['form_data']['properties'] : null;?></textarea>
                                    </div>
                                    <div class="form-group has-feedback bg-white" style="border-radius: 5px"">
                                        <label for="application"style="margin: 15px;">Застосування</label>
                                        <textarea name="application" cols="30" rows="5" class="form-control" id="editor3" placeholder="Застосування"><?php isset($_SESSION['form_data']['application']) ? $_SESSION['form_data']['application'] : null;?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="related">Залежні товари</label>
                                        <select name="related[]" class="form-control select2" id="related" multiple></select>
                                    </div>
                                    <h6 class="card-title">Фільтри</h6>
                                    <?php new \app\widgets\filter\Filter(null, WWW . '/filter/admin_filter_tpl.php');?>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="card card-warning file-upload">
                                                    <div class="card-header">
                                                        <h3 class="card-title">Базове зображення</h3>
                                                    </div>
                                                    <div class="card-body">
                                                        <div id="single" class="btn bg-danger" data-url="product/add-image" data-name="single">Оберіть файл</div>
                                                        <p style="color: #0c0c0c; padding-top: 20px;">Бажані розміри 700x700 px;</p>
                                                    </div>
                                                    <div class="single" style="padding: 10px;"></div>
                                                    <div class="overlay">
                                                        <i class="fa fa-refresh fa-spin"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="card card-warning file-upload">
                                                    <div class="card-header">
                                                        <h3 class="card-title">Галерея</h3>
                                                    </div>
                                                    <div class="card-body">
                                                        <div id="multi" class="btn bg-danger" data-url="product/add-image" data-name="multi">Оберіть файл</div>
                                                        <p style="color: #0c0c0c; padding-top: 20px;">Бажані розміри 700x700 px;</p>
                                                    </div>
                                                    <div class="multi" style="padding: 10px;"></div>
                                                    <div class="overlay">
                                                        <i class="fa fa-refresh fa-spin"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-info">Cтворити товар</button>
                                </div>
                            </form>
                            <?php if (isset($_SESSION['form_data'])) unset($_SESSION['form_data']);?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>