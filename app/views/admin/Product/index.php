<div class="content-header">
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Список товарів</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= ADMIN ?>/">Головна</a></li>
                <li class="breadcrumb-item">Список товарів</li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-12">
            <!-- Custom Tabs -->
            <div class="card">
                <div class="card-header d-flex p-0">
                    <h3 class="card-title p-3">Список</h3>
                    <div class="card-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control float-right" placeholder="Пошук товару">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-warning"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <section class="content">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <table class="table table-striped table-valign-middle">
                                            <thead>
                                            <tr>
                                                <th>Продукт</th>
                                                <th>Ціна</th>
                                                <th>Бренд</th>
                                                <th>Категорія</th>
                                                <th>Публікація</th>
                                                <th>Дія</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($products as $product):?>
                                            <tr>
                                                <td>
                                                    <img style="width: auto; height: 60px; border-radius: unset" src="/images/productavatar/<?=$product['img'];?>" alt="Product 1" class="img-circle img-size-32 mr-2">
                                                    <?=$product['title'];?>
                                                    <?php if ($product['hit']):?>
                                                    <span class="badge bg-danger">new</span>
                                                    <?php endif;?>
                                                </td>
                                                <td>$ <?=$product['price'];?></td>
                                                <td><?=$product['brand'];?></td>
                                                <td><a href="<?= ADMIN; ?>/category/edit?id=<?=$product['category_id'];?>"><?=$product['cat'];?></a></td>
                                                <td><?=$product['status'] ? "<span class=\"badge bg-info\">On</span>" : "<span class=\"badge bg-danger\">Off</span>";?></td>
                                                <td>
                                                    <a href="<?= ADMIN ?>/product/edit?id=<?=$product['id'];?>" class="text-muted"><i class="fa fa-pencil"></i></a>
                                                    <a href="<?= ADMIN ?>/product/delete?id=<?=$product['id'];?>" class="text-muted delete"><i class="fa fa-close"></i></a>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                            </tbody>
                                        </table>
                                        <div class="card-footer clearfix">
                                            <?php if ($pagination->countpage > 1):?>
                                                <?=$pagination;?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>