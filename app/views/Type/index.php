<div class="head-bread">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Головна</a></li>
            <li class="active"><?= $servises->title;?></li>
        </ol>
    </div>
</div>
<div class="showcase-grid">
    <div class="container">
        <div class="col-md-9">
            <h1 class="title no-bot-margin"style="margin-bottom: 30px;" ><?= $servises->title;?></h1>
            <p class="seves-p"><i class="fa fa-map-marker serves-market"></i> У місті <span class="serves-text-two "><?=$location->name;?></span></p>
            <?= $servises->content;?>
            <?php if ($servises->owner_id == 0 && isset($_SESSION['user']) && $_SESSION['user']['role']!= 'user' && $_SESSION['user']['role']!= 'admin'):?>
                <div class="card-body p-0 demo-box" id="demo21">
                    <ul class="products-list product-list-in-card pl-2 pr-2">
                        <li class="item" style="padding: 20px;">
                            <div class="product-img">
                                <img src="images/logo_black.png" style="width: auto;" alt="Product Image" class="img-size-50">
                            </div>
                            <div class="product-info infos">
                                <a href="javascript:void(0)" class="product-title"><span>В цьому місці МОЖЕ БУТИ ВАША РЕКЛАМА!!</span>
                                    <span class="badge float-right"  style="color: white"><i class="fa fa-phone" style="margin-right: 10px;"></i>+38(096) 456 36 14</span></a>
                                <span class="product-description">
                        Послуги з напрямку <?= $servises->title;?> телефонуйте!
                      </span>
                            </div>
                        </li>
                    </ul>
                </div>
            <?php elseif ($servises->owner_id != 0):?>
                <div class="card-body p-0 demo-box">
                    <ul class="products-list product-list-in-card pl-2 pr-2">
                        <li class="item" style="padding: 20px;">
                            <div class="product-img">
                                <a href="<?=$owner->role;?>/<?=$owner->alias;?>"><img src="images/<?=$owner->avatar;?>" style="width: auto;" alt="Product Image" class="img-size-50"></a>
                            </div>
                            <div class="product-info infos">
                                <a href="javascript:void(0)" class="product-title"><span> ЗАМОВТЕ!!</span>
                                    <span class="badge float-right" style="color: white"><i class="fa fa-phone" style="margin-right: 10px;"></i><?=$owner->phone;?></span></a>
                                <span class="product-description">
                        <?= $servises->title;?> за телефоном!
                      </span>
                            </div>
                        </li>
                    </ul>
                </div>
            <?php else:?>
                <div class="card-body p-0 demo-box">
                    <ul class="products-list product-list-in-card pl-2 pr-2">
                        <li class="item" style="padding: 20px;">
                            <div class="product-img">
                                <img src="images/logo_black.png" style="width: auto;" alt="Product Image" class="img-size-50">
                            </div>
                            <div class="product-info infos">
                                <a href="javascript:void(0)" class="product-title"><span> ЗАМОВТЕ!!</span>
                                    <span class="badge float-right" style="color: white"><i class="fa fa-phone" style="margin-right: 10px;"></i>+38(096) 456 36 14</span></a>
                                <span class="product-description">
                        <?= $servises->title;?> за телефоном!
                      </span>
                            </div>
                        </li>
                    </ul>
                </div>
            <?php endif;?>
        </div>
        <div class="col-md-3">
            <h4 class="sidebar_inhouse" style="border-bottom: 2px solid #e8514a;    padding-bottom: 20px;">Оберіть послугу за видами:</h4>
            <div class="col-md-12 sidebar_block widget" style="border-bottom: 2px solid #e8514a;">
                <?php foreach ($type_all as $item):?>
                    <ul class="popular-posts">
                        <li>
                            <a href="">
                                <div class="rec">
                                    <img width="100" height="80" src="/images/type/<?=$item->avatar;?>" class="" alt="<?=$anather->title;?>">
                                </div>
                            </a><p><a href=""></a>
                                <a href="type/<?=$item->alias;?>"><?=$item->title;?></a>
                            </p>
                            <em><?=$item->view;?> Перегляд...</em>
                        </li>
                    </ul>
                <?php endforeach;?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>