
<div class ="body">
    <div ng-view>  <!-- PRELOADER -->
        <div class="spn_hol">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="bnr" id="home">
        <div  id="top" class="callbacks_container">
            <ul class="rslides" id="slider4">
                <li>
                    <img src="images/baners/bnr-1.jpg" alt=""/>
                    <h3 class="baner-main top50 font42">Промислові підлоги</h3>
                    <h3 class="baner-main top60 font32">>12000 м2</h3>
                    <h3 class="baner-main top70 font32">>300 реалізованих проктів</h3>
                    <p class="baner-main top80 font12 seves-p"><i class="fa fa-map-marker serves-market"></i>Місто:<span class="serves-text">Івано-Франківськ</span><i class="fa fa-phone serves-market" style="margin-left: 30px;"></i>Телефон:<span class="serves-text">(096)-456-3-614</span></p>
                    <div class="logo_baner">
                        <img src="images/logo.png" alt="">
                    </div>
                </li>
                <li>
                    <img src="images/baners/bnr-2.jpg" alt=""/>
                    <h3 class="baner-main top50 font42">Заливка стяжок</h3>
                    <h3 class="baner-main top60 font32">+ Якість</h3>
                    <h3 class="baner-main top70 font32">+ Ціна</h3>
                    <p class="baner-main top80 font12 seves-p"><i class="fa fa-map-marker serves-market"></i>Місто <span class="serves-text">Львів</span></p>
                    <p class="baner-main top80 font12 seves-p"><i class="fa fa-map-marker serves-market"></i>Місто:<span class="serves-text">Львів</span><i class="fa fa-phone serves-market" style="margin-left: 30px;"></i>Телефон:<span class="serves-text">(096)-456-3-614</span></p>
                    <div class="logo_baner">
                        <img src="images/logo.png" alt="">
                    </div>
                </li>
                <li>
                    <img src="images/baners/bnr-3.jpg" alt=""/>
                    <h3 class="baner-main top50 font42">Декоративні наливні підлоги</h3>
                    <h3 class="baner-main top60 font32">>300 реалізованих проктів</h3>
                    <h3 class="baner-main top70 font32">>1900 м2</h3>
                    <p class="baner-main top80 font12 seves-p"><i class="fa fa-map-marker serves-market"></i>Місто:<span class="serves-text">Львів</span><i class="fa fa-phone serves-market" style="margin-left: 30px;"></i>Телефон:<span class="serves-text">(096)-456-3-614</span></p>
                    <div class="logo_baner">
                        <img src="images/logo.png" alt="">
                    </div>
                </li>
            </ul>
            <div style="width: 100%; height: 80px; padding: 3px; margin-top:10px; margin-bottom: 15px; background: linear-gradient(135deg, #0e0e1f,#e8514a); position: absolute;">
                <div class="border">
                    <img style="height: 100%; width: auto; margin-left: 10%; float: left;top: 20px;" src="/images/montage.png" alt="<?=$page->title;?>">
                    <h3>В пошуках замовлень?&nbsp;</h3>
                    <h3 class="next">Стань майстром на INHOUSE</h3>
                    <a href="user/signup" class="btn bb  go">Зареєструватись</a>
                </div>

            </div>
        </div>

        <div class="clearfix"> </div>
    </div>
</div>
<?php //if ($brands): ?>
<!--<h2>Якість для професіоналів</h2>-->
<!--    <div class="about">-->
<!--        <div class="container">-->
<!--            <div class="about-top grid-1">-->
<!--                --><?php //foreach ($brands as $brand):?>
<!--                    <div class="col-md-4 about-left">-->
<!--                        <figure class="effect-bubba">-->
<!--                            <img class="img-responsive" src="images/--><?//= $brand->img; ?><!--" alt=""/>-->
<!--                            <figcaption>-->
<!--                                <h2>--><?//= $brand->title; ?><!--</h2>-->
<!--                                <p>--><?//= $brand->description; ?><!--</p>-->
<!--                            </figcaption>-->
<!--                        </figure>-->
<!--                    </div>-->
<!--                --><?php //endforeach;?>
<!--                <div class="clearfix"></div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<?php //endif; ?>

<?php if ($hits): ?>
    <?php $curr = \inhouse\App::$app->getProperty('currency'); ?>
    <h2 style="margin-top: 80px;">Новинки в світі матеріалів</h2>
    <div class="product">
        <div class="container">
            <div class="product-top">
                <div class="product-one">
                    <?php foreach ($hits as $hit):?>
                        <div class="col-md-3 product-left">
                            <div class="product-main simpleCart_shelfItem">
                                <a href="product/<?= $hit->alias; ?>" class="mask"><img class="img-responsive zoom-img" src="images/productavatar/<?= $hit->img; ?>" alt="" /></a>
                                <div class="product-bottom">
                                    <?php $hit_text = pruning($hit->content, 55); ?>
                                    <h3><?= $hit->title ?></h3>
                                    <p><?= $hit_text; ?></p>
                                    <h4><a id="productAdd" data-id="<?=$hit->id; ?>" class="add-to-card-link" href="cart/add?id=<?= $hit->id; ?>"><i></i></a> <span class=" item_price"><?= $curr['symbol_left']; ?><?= $hit->price * $curr['value']; ?><?= $curr['symbol_right']; ?></span></h4>
                                </div>
                                <?php if ($hit['hit']):?>
                                    <div class="srch">
                                        <span>НОВИНКА</span>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endforeach;?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<h2>Розвиваємось разом</h2>
<div class="container gradient">
    <div id="about-us" class="clearfix">
        <div class="cell" style="background-image: url(/images/6cfcb605c5a5b067b37e51ee970191f0.png); padding: 25px;">
                <h3>Продукція</h3>
                <p>Ми є дистриб'ютором утеплювальних і шумоізоляционних матеріалів, а також облицювальні та фасадні матеріали, матеріали для влаштування підлог, фарби, гідроізоляцію, піни і герметики, монтажні клеї та інші допоміжні матеріали. </p>
            <img style="width: 50px; height: auto;" src="/images/item1-1.png" alt="До каталогу">
                <a href="category/products"><span class="more-link">До каталогу</span></a>
        </div>
        <div class="cell" style="background-image: url(/images/3ebc071a4b06b7a6559f3c78f77804ea.jpg); padding: 25px;">
                <h3>Калькулятори</h3>
                <p>З нами розрахувати необхідну кількість а також їхню вартість різних відомих брендів стало зручніше та швидше.</p>
            <img style="width: 50px; height: auto;" src="/images/item3-1.png" alt="Розрахувати">
                <a href="calculator/"><span class="more-link">Розрахувати</span></a>
        </div>
        <div class="clearfix"></div>
        <div class="cell" style="background-image: url(/images/b1241923a1e32636e56640da507aedb1.png); padding: 25px;">
                <h3>Наші послуги</h3>
                <p>Ознайомтесь з всіма видами пслуг які надає компанія Inhouse, якість гарантовано. </p>
            <img style="width: 50px; height: auto;" src="/images/montage.png" alt="До списку послуг">
            <a href="services/location/"><span class="more-link">До списку послуг</span></a>
        </div>
        <div class="cell" style="background-image: url(/images/0a3ebbee99b06deb0faa0a33af597c54.jpg); padding: 25px;">
                <h3>Рекомендації</h3>
                <p>Наші спеціалісти розробили рекомендації та послідовні інструкції по застосуванню всіх матеріалів.</p>
            <img style="width: 50px; height: auto;" src="/images/item2-1.png" alt="До списку рекомендацій">
                <a href="recommend/"><span class="more-link">До списку рекомендацій</span></a>
        </div>
    </div>
</div>
<?php if ($last_post): ?>
    <?php $curr = \inhouse\App::$app->getProperty('currency'); ?>
    <h2>Останні рекомендації</h2>
    <div class="product">
        <div class="container">
            <div class="product-top">
                <div class="product-one">
                    <?php foreach ($last_post as $post):?>
                    <div class="col-md-3 product-left">
                    <div class="product-mains simpleCart_shelfItem">
                                <div class="recommends">
                                    <img src="images/recommend/<?=$post->avatar;?>" class="post_img" alt="<?=$post->title;?>">
                                </div>
                                <a href="recommend/<?= $post->alias; ?>">
                                <div class="product-bottom">
                                    <h3><?= $post->title ?></h3>
                                </div>
                                </a>
                        </div>
                    </div>
                    <?php endforeach;?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
