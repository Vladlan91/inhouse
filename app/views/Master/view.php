<div id="page" class="recommends-page" style="background-image: url('images/useravatar/<?=$users->back_avatar;?>');">
    <div class="head-bread">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="/" style="color: #fb4c29; font-size: large; font-weight: 900;">Головна</a></li>
                <li class="active" style="font-size: 34px;"><?= $users->name;?></li>
            </ol>
        </div>
    </div>
    <div class="prdt">
        <div class="brand-conteiner">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-primary card-outline">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                    <?php if ($users->avatar):?>
                                        <img class="profile-user-img img-fluid img-circle" style="width: 100%; height: auto;" src="/images/useravatar/<?=$users->avatar;?>" alt="User profile picture">
                                    <?php else:?>
                                        <img class="profile-user-img img-fluid img-circle" style="width: 100%; height: auto;" src="/images/no-img-avatar.png" alt="User profile picture">
                                    <?php endif;?>
                                </div>
                                <h3 class="profile-username text-center"><?=$users->name;?></h3>
                                    <p class="text-muted text-center">Бригада</p>
                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>Переглядів</b> <a class="float-right" style="font-weight: 900; font-size: 15px; text-decoration: none;">34568</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Додано в закладки:</b> <a class="float-right" style="font-weight: 900; font-size: 15px; text-decoration: none;">1123 чол.</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Замовлень:</b> <a class="float-right" style="font-weight: 900; font-size: 15px; text-decoration: none;">13 287 00 грн.</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header p-2">
                                <ul class="nav nav-pills">
                                        <li class="nav-item active"><a class="active show order"  style="font-size: 10px; margin-left: 5px;" href="#one" data-toggle="tab">Історія  &nbsp; бригади</a></li>
                                        <li class="nav-item"><a class="nav-link order"  style="font-size: 10px; margin-left: 5px;" href="#three" data-toggle="tab">Контакти</a></li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="one" style="max-height: 100%;">

                                    </div>
                                    <div class="tab-pane fade" id="three">

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header p-3">
                                <ul class="nav nav-pills">
                                    <li class="nav-item active"><a class="active show order"  style="font-size: 10px; margin-left: 5px;" href="#one-o" data-toggle="tab">Реалзовані  &nbsp; проекти</a></li>
                                    <li class="nav-item"><a class="nav-link order"  style="font-size: 10px; margin-left: 5px;" href="#too-o" data-toggle="tab">Прайс  &nbsp;  цін  &nbsp;  на  &nbsp;  послуги</a></li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="one-o" style="max-height: 100%;">

                                    </div>
                                    <div class="tab-pane fade" id="too-o">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
