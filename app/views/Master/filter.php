<?php if ($users): ?>
    <?php foreach ($users as $user):?>
        <div class="col-md-5 col-md-offset-1" style="margin-top: 30px;">
            <!-- Widget: user widget style 1 -->
            <div class="card card-widget widget-user">
                <div class="widget-user-header text-white">
                    <?php if ($user->back_avatar):?>
                        <a  style="text-decoration: none;" href="master/<?= $user->alias;?>"><img class="img-circle" style="width: 100%; height: 100%; position: relative;" src="images/useravatar/<?= $user->back_avatar;?>" alt="User Avatar"></a>
                    <?php else:?>
                        <a  style="text-decoration: none;" href="master/<?= $user->alias;?>"><img class="img-circle" style="width: 100%; height: 100%; position: relative;" src="images/no-img-avatar.png" alt="User Avatar"></a>
                    <?php endif;?>
                    <a  style="text-decoration: none;" href="master/<?= $user->alias;?>"><h3 class="widget-user-username" style="color: rgba(232,81,74,.9); position: absolute;" ><?= $user->name;?></h3></a>
                    <h5 class="widget-user-desc" style="color: white;">Будівництво</h5>
                </div>
                <div class="widget-user-image">
                </div>
                <div class="card-footer" style="z-index: 100">
                    <div class="star-rating__wrap">
                        <input class="star-rating__input" id="star-rating-5" type="radio" name="rating" value="5">
                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-5" title="5 out of 5 stars"></label>
                        <input class="star-rating__input" id="star-rating-4" type="radio" name="rating" value="4">
                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-4" title="4 out of 5 stars"></label>
                        <input class="star-rating__input" id="star-rating-3" type="radio" name="rating" value="3">
                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-3" title="3 out of 5 stars"></label>
                        <input class="star-rating__input" id="star-rating-2" type="radio" name="rating" value="2">
                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-2" title="2 out of 5 stars"></label>
                        <input class="star-rating__input" id="star-rating-1" type="radio" name="rating" value="1">
                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-1" title="1 out of 5 stars"></label>
                    </div>
                    <?php if ($user->avatar):?>
                        <a  style="text-decoration: none;" href="master/<?= $user->alias;?>"><img class="img-circle" style="margin-top: -34px;width: 50px; height: 50px; float: right; border-radius: 50%;" src="images/useravatar/<?= $user->avatar;?>" alt="User Avatar"></a>
                    <?php else:?>
                        <a  style="text-decoration: none;" href="master/<?= $user->alias;?>"><img class="img-circle" style="margin-top: -34px;width: 50px; height: 50px; float: right; border-radius: 50%;" src="images/avatar-profile-icon-man.jpg" alt="User Avatar"></a>
                    <?php endif;?>
                </div>
            </div>
        </div>
    <?php endforeach;?>
<div class="clearfix"></div>
<?php if ($pagination->countpage > 1):?>
    <?=$pagination;?>
<?php endif; ?>
<?php else:?>
<h2>За даним фільтром бригади відсутні...</h2>
<img class="non-pro" src="images/montage.png" alt="В даній категорії товари відсутні">
<?php endif; ?>
