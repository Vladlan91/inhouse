<div class="head-bread">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Головна</a></li>
            <li><a href="/user/cabinet">Мій кабінет</a></li>
            <li class="active">Огляд замовлення</li>
        </ol>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<section class="container">
<div class="row">
    <div class="col-8">
        <div class="card">
            <div class="card-header no-border">
            </div>
            <div class="card-body p-0">
                <table class="table table-striped table-valign-middle">
                    <thead style="background-color: #202022; color: #fff;">
                    <tr>
                        <th style="font-size: small; width: 5%;">ID:</th>
                        <th style="font-size: small; width: 15%;">ФОТО:</th>
                        <th style="font-size: small; width: 20%;">НАЗВА:</th>
                        <th style="font-size: small; width: 15%;">БРЕНД:</th>
                        <th style="font-size: small; width: 15%;">МОДЕФІКАЦІЯ:</th>
                        <th style="font-size: small; width: 10%;">КІЛЬКІСТЬ:</th>
                        <th style="font-size: small; width: 10%;">ЦІНА:</th>
                        <th style="font-size: small; width: 20%;">ПЕРЕГЛЯНУТИ</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $qty = 0; foreach ($order_products as $product):?>
                        <tr>
                            <td><span class="badge" style="background-color: #d9534f;"><?=$product->product_id;?></span></td>
                            <td>
                                <img style="width: auto; height: 60px; border-radius: unset" src="images/productavatar/<?=$product->img;?>" alt="Product 1" class="img-circle img-size-32 mr-2">
                            </td>
                            <td><?=$product->title;?></td>
                            <td><?=$product->brand_title;?></td>
                            <td><?=$product->brand_title;?></td>
                            <td><?=$product->qty; $qty += $product->qty;?></td>
                            <td><span class="badge" style="background-color: #d9315e;"><?=$product->price;?>&ensp;<?=$orde['currency'];?></span></td>
                            <td>&ensp;&ensp;&ensp;&ensp;&ensp;<span class="badge" style="background-color: #d9534f;">
                               <a href="product/<?=$product->alias;?>" class="text-muted">
                                    <i class="fa fa-search"></i>
                                </a></span>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="table-responsive">
            <table class="table" style="width:50%; background-color: #202022; margin-top: 20px; border-right: 2px solid #ddd; border-left: 2px solid #ddd; border-top: 2px solid #ddd;  border-bottom: 2px solid #ddd; color: white;">
                <tr>
                    <th style="font-weight: 600; font-size: 14px;">Ціна всього:</th>
                    <td><span class="badge"><?=$orde['sum'];?>&ensp;<?=$orde['currency'];?></span></td>
                </tr>
                <tr>
                    <th style="font-weight: 600; font-size: 14px;">Знижка (<span class="badge">0&ensp;%</span>)</th>
                    <td><span class="badge" style="background-color: #00b08c;">0&ensp;<?=$orde['currency'];?></span></td>
                </tr>
                <tr>
                    <th style="font-weight: 600; font-size: 14px;">Кількість одениць:</th>
                    <td><span class="badge"><?=$qty;?></span></td>
                </tr>
                <tr>
                    <th style="font-weight: 600; font-size: 14px;">Всього до сплати:</th>
                    <td><span class="badge" style="background-color: #00b08c;"><?=$orde['sum'];?>&ensp;<?=$orde['currency'];?></span></td>
                </tr>
            </table>
        </div>
    </div>
</div>
</section>