<div class="head-bread">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Головна</a></li>
            <li class="active">Авторизація</li>
        </ol>
    </div>
</div>
<div class="login">
    <div class="container">
        <div class="login-grids">
            <div class="col-md-6 log">
                <h3>Вхід</h3>
                <div class="strip"></div>
                <p>Ласкаво просимо, будь ласка, введіть наступні дані, щоб продовжити.</p>
                <form method="post" action="user/login" id="login" role="form" data-toggle="validator">
                    <h5>Ваше імя:</h5>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" name="login" value="" id="login" placeholder="Login" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                    <h5>Пароль:</h5>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" name="password" id="password" value="" placeholder="Password" required><br>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                    <input type="submit" value="Увійти">

                </form>
                <a href="#">Забули пароль ?</a>
            </div>
            <div class="col-md-6 login-right">
                <h3>Створити акаунт</h3>
                <div class="strip"></div>
                <p>Ставши зареєстрованим користувачем Ви матимете нагоду отримувати накопичувальну знижку по товарам які придбаєте у нас, розмір знижки залежитиме від величини замовлень!</p>
                <a href="user/signup" class="button">Звичайний акаунт</a>
                <a href="user/signup-master" class="button">Майстр</a>
                <a href="user/signup-builder" class="button">Будівельник</a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>