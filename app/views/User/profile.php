<div class="head-bread" style="background-image: url('images/useravatar/<?=$_SESSION['user']['back_avatar'];?>');">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Головна</a></li>
            <li class="active">Мій кабінет</li>
        </ol>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<?php if ($_SESSION['user']['role'] == user):?>

<?php elseif ($_SESSION['user']['role'] == master):?>
    <div class="prdt">
        <div class="brand-conteiner">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-primary card-outline">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                    <?php if ($_SESSION['user']['avatar']):?>
                                    <img class="profile-user-img img-fluid img-circle" style="width: 100%; height: auto;" src="/images/useravatar/<?=$_SESSION['user']['avatar'];?>" alt="User profile picture">
                                    <?php else:?>
                                    <img class="profile-user-img img-fluid img-circle" style="width: 100%; height: auto;" src="/images/no-img-avatar.png" alt="User profile picture">
                                    <?php endif;?>
                                </div>
                                <h3 class="profile-username text-center"><?=$_SESSION['user']['name'];?></h3>
                                <p class="text-muted text-center">Бригада</p>
                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>Переглядів</b> <a class="float-right" style="font-weight: 900; font-size: 15px; text-decoration: none;">34568</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Додано в закладки:</b> <a class="float-right" style="font-weight: 900; font-size: 15px; text-decoration: none;">1123 чол.</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Замовлень:</b> <a class="float-right" style="font-weight: 900; font-size: 15px; text-decoration: none;">13 287 00 грн.</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header p-2">
                                <ul class="nav nav-pills">
                                    <li class="nav-item active"><a class="active show order"  style="font-size: 10px; margin-left: 5px;" href="#one" data-toggle="tab">Історія  &nbsp; бригади</a></li>
                                    <li class="nav-item"><a class="nav-link order"  style="font-size: 10px; margin-left: 5px;" href="#two" data-toggle="tab">Контакти</a></li>
                                    <li class="nav-item"><a class="nav-link order"  style="font-size: 10px; margin-left: 5px;" href="#three" data-toggle="tab">Мої  &nbsp; замовлення</a></li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="one" style="max-height: 100%;">
                                        <?=$_SESSION['user']['history'];?>
                                    </div>
                                    <div class="tab-pane fade" id="two">
                                        <?=$_SESSION['user']['contact'];?>
                                    </div>
                                    <div class="tab-pane fade" id="three">
                                        <?php if (!$orders):?>
                                            <h4>Ви ще не оформляли замовлення...</h4>
                                            <img class="non-pro" src="/images/non-pro.png" alt="В даній категорії товари відсутні">
                                        <?php else:?>
                                            <section class="content">
                                                <div class="card-body p-0">
                                                    <table class="table">
                                                        <tr>
                                                            <th style="font-size: small; width: 20%;">ID</th>
                                                            <th style="font-size: small; width: 20%;">Статус</th>
                                                            <th style="font-size: small; width: 20%;">Сума</th>
                                                            <th style="font-size: small; width: 20%;">Дата створення</th>
                                                            <th style="font-size: small; width: 20%;">Дата зміни статусу</th>
                                                            <th style="font-size: small">Перегляд</th>
                                                        </tr>
                                                        <?php foreach ($orders as $order):?>
                                                            <tr>
                                                                <td><span class="badge" style="background-color: #d9534f;"><?=$order['id'];?></span></td>
                                                                <?php if ($order['status'] == 0):?>
                                                                    <td><span class="badge" style="background-color: #d97d3a; font-size: 12px;">не опрацьоване</span></td>
                                                                <?php elseif ($order['status'] == 1):?>
                                                                    <td><span class="badge" style="background-color: #cdd931; font-size: 12px;">оформлене</span></td>
                                                                <?php elseif ($order['status'] == 2):?>
                                                                    <td><span class="badge" style="background-color: #7c1cd9; font-size: 12px;">відправлене</span></td>
                                                                <?php else:?>
                                                                    <td><span class="badge" style="background-color: #1ed950; font-size: 12px;">оплачене</span></td>
                                                                <?php endif;?>
                                                                <td><span class="badge bg-warning"><?=$order['sum'];?> <?=$order['currency'];?></span></td>
                                                                <td style="font-size: 10px;"><?=$order['data'];?></td>
                                                                <td style="font-size: 10px;"><?=$order['update_at'];?></td>
                                                                <td><span class="badge" style="background-color: #d9534f;"><a href="user/order-view?id=<?=$order['id'];?>" class="text-muted"><i class="fa fa-search"></i></a></span>
                                                            </tr>
                                                        <?php endforeach;?>
                                                    </table>
                                                </div>
                                            </section>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header p-3">
                                <ul class="nav nav-pills">
                                    <li class="nav-item active"><a class="active show order"  style="font-size: 10px; margin-left: 5px;" href="#one-o" data-toggle="tab">Реалзовані  &nbsp; проекти</a></li>
                                    <li class="nav-item"><a class="nav-link order"  style="font-size: 10px; margin-left: 5px;" href="#too-o" data-toggle="tab">Прайс  &nbsp;  цін</a></li>
                                    <li class="nav-item"><a class="nav-link order"  style="font-size: 10px; margin-left: 5px;" href="#three-o" data-toggle="tab">Налаштування</a></li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="one-o" style="max-height: 100%;">
                                        <?php if (empty($_SESSION['user']['price'])):?>
                                            <div class="col-md-6 login-right">
                                                <h3>Створити портфоліо</h3>
                                                <div class="strip"></div>
                                                <p>Якщо бажаєте створити портфоліо на Ваші послуги, зв'яжіться з нашим адміністратором, або надішліть нам повідомлення.</p>
                                                <a href="contact/" class="button">Надіслати повідомлення</a>
                                            </div>
                                        <?php else:?>
                                            <?=$_SESSION['user']['price'];?>
                                        <?php endif;?>
                                    </div>
                                    <div class="tab-pane fade" id="too-o">
                                        <?php if (empty($_SESSION['user']['price'])):?>
                                        <div class="col-md-6 login-right">
                                            <h3>Створити прайс цін</h3>
                                            <div class="strip"></div>
                                            <p>Якщо бажаєте створити прайс на Ваші послуги, зв'яжіться з нашим адміністратором, або надішліть нам повідомлення.</p>
                                            <a href="contact/" class="button">Надіслати повідомлення</a>
                                        </div>
                                        <?php else:?>
                                            <?=$_SESSION['user']['price'];?>
                                        <?php endif;?>
                                    </div>
                                    <div class="tab-pane fade" id="three-o">
                                        <form class="form-horizontal" action="user/profile" method="post" data-toggle="validator">
                                            <div class="card-body">
                                                <div class="tab-content">
                                                    <div class="form-group">
                                                        <label for="login" class="col-sm-2 control-label">Логін</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" id="login" name="login" value="<?=h($_SESSION['user']['login']);?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="password" class="col-sm-2 control-label">Пароль</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" id="password" name="password" placeholder="Введіть новий пароль">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="phone" class="col-sm-2 control-label">Телефон</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" id="phone" name="phone" placeholder="Введіть головний телефон який відображатиметься під Вашими орендованими сторінками, в формат  +38 (096)-123-45-67" value="<?=h($_SESSION['user']['phone']);?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 control-label">Ім'я</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" id="name" name="name" value="<?=h($_SESSION['user']['name']);?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email" class="col-sm-2 control-label">Емейл</label>
                                                        <div class="col-sm-10">
                                                            <input type="email" class="form-control has-feedback" name="email" id="email" value="<?=h($_SESSION['user']['email']);?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="address" class="col-sm-2 control-label">Адреса</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control has-feedback" id="address" name="address" value="<?=h($_SESSION['user']['address']);?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="description" class="col-sm-2 control-label">Опис послуг і вашої компанії (використовується для SEO )</label>
                                                        <div class="col-sm-10">
                                                            <textarea class="form-control" name="description" id="description" placeholder="ПРИКЛАД - ІВ ТЕХ - надаємо пуслуги з влаштування кабельного телебачення в Львові та Івано-Франківську. Кабельне телебачення за вігідною ціною у Львові - ІВ-ТЕХ"><?=h($_SESSION['user']['description']);?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 login-right">
                                                    <h3>Фільтер по якому Вас шукатимуть!</h3>
                                                    <div class="strip"></div>
                                                    <p style="margin-bottom: 30px;">Оберіть послуги, локацію та міста, для налаштування ефективного пошуку на платформі INHOUSE.</p>
                                                </div>
                                                   <?php new \app\widgets\filter\Ufilter($filter, WWW . '/filter/cabin_filter_tpl.php');?>
                                                <div class="box-footer">
                                                    <div class="col-sm-10" style="margin-top: 30px">
                                                        <input type="hidden" name="id" value="<?=$_SESSION['user']['id'];?>">
                                                        <button type="submit" class="btn btn-danger">Внести зміни</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header p-3">
                                <ul class="nav nav-pills">
                                    <li class="nav-item"><a class="nav-link order"  style="font-size: 10px; margin-left: 5px;" href="#one-o-o" data-toggle="tab">Орендовані &nbsp; вами &nbsp;сторінки</a></li>
                                    <li class="nav-item"><a class="nav-link order"  style="font-size: 10px; margin-left: 5px;" href="#too-o-o" data-toggle="tab">Вільні  &nbsp;  сторінки</a></li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade" id="one-o-o" style="max-height: 100%;">
                                        <h4 class="cabin">Рекомендаційні сторінки</h4>
                                        <?php if (!$pages):?>
                                            <img class="non-pro" src="images/montage.png" alt="В даній категорії товари відсутні">
                                            <h4 class="more-user">Ви ще не орендуєте "Рекомендаційні сторінки"...</h4>
                                        <?php else:?>
                                            <section class="content">
                                                <div class="card-body p-0">
                                                    <table class="table">
                                                        <tr>
                                                            <th style="font-size: small; width: 5%;">ID</th>
                                                            <th style="font-size: small; width: 20%;">Назва</th>
                                                            <th style="font-size: small; width: 20%;">Ціна</th>
                                                            <th style="font-size: small; width: 20%;">Категорія</th>
                                                            <th style="font-size: small; width: 20%;">Дата завершення броні</th>
                                                            <th style="font-size: small; width: 20%;">Переглядів в місяць</th>
                                                            <th style="font-size: small">Перейти</th>
                                                        </tr>
                                                        <?php foreach ($pages as $page):?>
                                                            <tr>
                                                                <td><span class="badge" style="background-color: #d9534f; font-size: 10px;"><?=$page['id'];?></span></td>
                                                                <td><span class="badge" style="background-color: #cdd931; font-size: 10px; text-transform: uppercase;"><?=$page['title'];?></span></td>
                                                                <td><span class="badge" style="background-color: #7c1cd9; font-size: 10px;"><?=$page['price'];?>&nbsp;$</span></td>
                                                                <td><span class="badge" style="font-size: 10px;"><?=$page['cat'];?></span></td>
                                                                <td style="font-size: small"><?=$page['bought_to'];?></td>
                                                                <td style="font-size: small"><?=$page['monthe_view'];?></td>
                                                                <td><span class="badge" style="background-color: #d9534f;font-size: 10px;"><a href="recommend/<?=$page['alias'];?>" class="text-muted"><i class="fa fa-search"></i></a></span>
                                                            </tr>
                                                        <?php endforeach;?>
                                                    </table>
                                                </div>
                                            </section>
                                        <?php endif;?>
                                        <h4 class="cabin">Головні сторінки меню</h4>
                                        <?php if (!$servises):?>
                                            <img class="non-pro" src="images/montage.png" alt="В даній категорії товари відсутні">
                                            <h4 class="more-user">Ви ще не орендуєте "Головні сторінки"...</h4>
                                        <?php else:?>
                                            <section class="content">
                                                <div class="card-body p-0">
                                                    <table class="table">
                                                        <tr>
                                                            <th style="font-size: small; width: 5%;">ID</th>
                                                            <th style="font-size: small; width: 20%;">Назва</th>
                                                            <th style="font-size: small; width: 20%;">Ціна</th>
                                                            <th style="font-size: small; width: 20%;">Дата завершення броні</th>
                                                            <th style="font-size: small; width: 20%;">Переглядів в місяць</th>
                                                            <th style="font-size: small">Перейти</th>
                                                        </tr>
                                                        <?php foreach ($servises as $servis):?>
                                                            <tr>
                                                                <td><span class="badge" style="background-color: #d9534f; font-size: 10px;"><?=$servis['id'];?></span></td>
                                                                <td><span class="badge" style="background-color: #cdd931; font-size: 10px; text-transform: uppercase;"><?=$servis['title'];?></span></td>
                                                                <td><span class="badge" style="background-color: #7c1cd9; font-size: 10px;"><?=$servis['price'];?>&nbsp;$</span></td>
                                                                <td style="font-size: small"><?=$servis['bought_to'];?></td>
                                                                <td style="font-size: small"><?=$servis['monthe_view'];?></td>
                                                                <td><span class="badge" style="background-color: #d9534f; font-size: 10px;"><a href="services/<?=$servis['alias'];?>" class="text-muted"><i class="fa fa-search"></i></a></span>
                                                            </tr>
                                                        <?php endforeach;?>
                                                    </table>
                                                </div>
                                            </section>
                                        <?php endif;?>
                                        <h4 class="cabin">Послуги у містах</h4>
                                        <?php if (!$location_servises):?>
                                            <img class="non-pro" src="images/montage.png" alt="В даній категорії товари відсутні">
                                            <h4 class="more-user">Ви ще не орендуєте сторінки "Послуги у містах"...</h4>
                                        <?php else:?>
                                            <section class="content">
                                                <div class="card-body p-0">
                                                    <table class="table">
                                                        <tr>
                                                            <th style="font-size: small; width: 5%;">ID</th>
                                                            <th style="font-size: small; width: 20%;">Назва</th>
                                                            <th style="font-size: small; width: 20%;">Місто</th>
                                                            <th style="font-size: small; width: 20%;">Ціна</th>
                                                            <th style="font-size: small; width: 20%;">Дата завершення броні</th>
                                                            <th style="font-size: small; width: 20%;">Переглядів в місяць</th>
                                                            <th style="font-size: small">Перейти</th>
                                                        </tr>
                                                        <?php foreach ($location_servises as $servis):?>
                                                            <tr>
                                                                <td><span class="badge" style="background-color: #d9534f; font-size: 10px;"><?=$servis['id'];?></span></td>
                                                                <td><span class="badge" style="background-color: #cdd931; font-size: 10px; text-transform: uppercase;"><?=$servis['title'];?></span></td>
                                                                <td><span class="badge" style="background-color: #0e0e1f; font-size: 10px; text-transform: uppercase;"><?=$servis['location_id'];?></span></td>
                                                                <td><span class="badge" style="background-color: #7c1cd9; font-size: 10px;"><?=$servis['price'];?>&nbsp;$</span></td>
                                                                <td style="font-size: small"><?=$servis['bought_to'];?></td>
                                                                <td style="font-size: small"><?=$servis['monthe_view'];?></td>
                                                                <td><span class="badge" style="background-color: #d9534f; font-size: 10px;"><a href="services/<?=$servis['alias'];?>" class="text-muted"><i class="fa fa-search"></i></a></span>
                                                            </tr>
                                                        <?php endforeach;?>
                                                    </table>
                                                </div>
                                            </section>
                                        <?php endif;?>
                                        <h4 class="cabin"> Типи послуги</h4>
                                        <?php if (!$type_services):?>
                                            <img class="non-pro" src="images/montage.png" alt="В даній категорії товари відсутні">
                                            <h4 class="more-user">Ви ще не орендуєте сторінки "Типи послуг у містах"...</h4>
                                        <?php else:?>
                                            <section class="content">
                                                <div class="card-body p-0">
                                                    <table class="table">
                                                        <tr>
                                                            <th style="font-size: small; width: 5%;">ID</th>
                                                            <th style="font-size: small; width: 20%;">Назва</th>
                                                            <th style="font-size: small; width: 20%;">Послуга у містах</th>
                                                            <th style="font-size: small; width: 20%;">Ціна</th>
                                                            <th style="font-size: small; width: 20%;">Дата завершення броні</th>
                                                            <th style="font-size: small; width: 20%;">Переглядів в місяць</th>
                                                            <th style="font-size: small">Перейти</th>
                                                        </tr>
                                                        <?php foreach ($type_services as $servis):?>
                                                            <tr>
                                                                <td><span class="badge" style="background-color: #d9534f; font-size: 10px;"><?=$servis['id'];?></span></td>
                                                                <td><span class="badge" style="background-color: #cdd931; font-size: 10px; text-transform: uppercase;"><?=$servis['title'];?></span></td>
                                                                <td><span class="badge" style="background-color: #0e0e1f; font-size: 10px; text-transform: uppercase;"><?=$servis['servises_id'];?></span></td>
                                                                <td><span class="badge" style="background-color: #7c1cd9; font-size: 10px;"><?=$servis['price'];?>&nbsp;$</span></td>
                                                                <td style="font-size: small"><?=$servis['bought_to'];?></td>
                                                                <td style="font-size: small"><?=$servis['monthe_view'];?></td>
                                                                <td><span class="badge" style="background-color: #d9534f; font-size: 10px;"><a href="services/<?=$servis['alias'];?>" class="text-muted"><i class="fa fa-search"></i></a></span>
                                                            </tr>
                                                        <?php endforeach;?>
                                                    </table>
                                                </div>
                                            </section>
                                        <?php endif;?>
                                    </div>
                                    <div class="tab-pane fade" id="too-o-o">
                                        <?php if (empty($_SESSION['user']['price'])):?>
                                            <div class="col-md-6 login-right">
                                                <h3>Як збільшити кількість замовлень?</h3>
                                                <div class="strip"></div>
                                                <p>Якщо бажаєте створити прайс на Ваші послуги, зв'яжіться з нашим адміністратором, або надішліть нам повідомлення.</p>
                                                <a href="contact/" class="button">Надіслати повідомлення</a>
                                            </div>
                                        <?php else:?>
                                            <?=$_SESSION['user']['price'];?>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php elseif ($_SESSION['user']['role'] == builder):?>
    <div class="prdt">
        <div class="brand-conteiner">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-primary card-outline">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                    <?php if ($_SESSION['user']['avatar']):?>
                                        <img class="profile-user-img img-fluid img-circle" style="width: 100%; height: auto;" src="/images/useravatar/<?=$_SESSION['user']['avatar'];?>" alt="User profile picture">
                                    <?php else:?>
                                        <img class="profile-user-img img-fluid img-circle" style="width: 100%; height: auto;" src="/images/no-img-avatar.png" alt="User profile picture">
                                    <?php endif;?>
                                </div>
                                <h3 class="profile-username text-center"><?=$_SESSION['user']['name'];?></h3>
                                <p class="text-muted text-center">Забудовник</p>
                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>Переглядів</b> <a class="float-right" style="font-weight: 900; font-size: 15px; text-decoration: none;">34568</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Додано в закладки:</b> <a class="float-right" style="font-weight: 900; font-size: 15px; text-decoration: none;">1123 чол.</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Замовлень:</b> <a class="float-right" style="font-weight: 900; font-size: 15px; text-decoration: none;">13 287 00 грн.</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header p-2">
                                <ul class="nav nav-pills">
                                    <li class="nav-item active"><a class="active show order"  style="font-size: 10px; margin-left: 5px;" href="#one" data-toggle="tab">Історія  &nbsp; компанії</a></li>
                                    <li class="nav-item"><a class="nav-link order"  style="font-size: 10px; margin-left: 5px;" href="#two" data-toggle="tab">Контакти</a></li>
                                    <li class="nav-item"><a class="nav-link order"  style="font-size: 10px; margin-left: 5px;" href="#three" data-toggle="tab">Замовлення &nbsp;компанії</a></li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="one" style="max-height: 100%;">
                                        <?=$_SESSION['user']['history'];?>
                                    </div>
                                    <div class="tab-pane fade" id="two">
                                        <?=$_SESSION['user']['contact'];?>
                                    </div>
                                    <div class="tab-pane fade" id="three">
                                        <?php if (!$orders):?>
                                            <h4>Ви ще не оформляли замовлення...</h4>
                                            <img class="non-pro" src="/images/non-pro.png" alt="В даній категорії товари відсутні">
                                        <?php else:?>
                                            <section class="content">
                                                <div class="card-body p-0">
                                                    <table class="table">
                                                        <tr>
                                                            <th style="font-size: small; width: 20%;">ID</th>
                                                            <th style="font-size: small; width: 20%;">Статус</th>
                                                            <th style="font-size: small; width: 20%;">Сума</th>
                                                            <th style="font-size: small; width: 20%;">Дата створення</th>
                                                            <th style="font-size: small; width: 20%;">Дата зміни статусу</th>
                                                            <th style="font-size: small">Перегляд</th>
                                                        </tr>
                                                        <?php foreach ($orders as $order):?>
                                                            <tr>
                                                                <td><span class="badge" style="background-color: #d9534f;"><?=$order['id'];?></span></td>
                                                                <?php if ($order['status'] == 0):?>
                                                                    <td><span class="badge" style="background-color: #d97d3a; font-size: 12px;">не опрацьоване</span></td>
                                                                <?php elseif ($order['status'] == 1):?>
                                                                    <td><span class="badge" style="background-color: #cdd931; font-size: 12px;">оформлене</span></td>
                                                                <?php elseif ($order['status'] == 2):?>
                                                                    <td><span class="badge" style="background-color: #7c1cd9; font-size: 12px;">відправлене</span></td>
                                                                <?php else:?>
                                                                    <td><span class="badge" style="background-color: #1ed950; font-size: 12px;">оплачене</span></td>
                                                                <?php endif;?>
                                                                <td><span class="badge bg-warning"><?=$order['sum'];?> <?=$order['currency'];?></span></td>
                                                                <td style="font-size: 10px;"><?=$order['data'];?></td>
                                                                <td style="font-size: 10px;"><?=$order['update_at'];?></td>
                                                                <td><span class="badge" style="background-color: #d9534f;"><a href="user/order-view?id=<?=$order['id'];?>" class="text-muted"><i class="fa fa-search"></i></a></span>
                                                            </tr>
                                                        <?php endforeach;?>
                                                    </table>
                                                </div>
                                            </section>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header p-3">
                                <ul class="nav nav-pills">
                                    <li class="nav-item active"><a class="active show order"  style="font-size: 10px; margin-left: 5px;" href="#one-o" data-toggle="tab">Реалзовані  &nbsp; проекти</a></li>
                                    <li class="nav-item"><a class="nav-link order"  style="font-size: 10px; margin-left: 5px;" href="#too-o" data-toggle="tab">Прайс  &nbsp;  цін</a></li>
                                    <li class="nav-item"><a class="nav-link order"  style="font-size: 10px; margin-left: 5px;" href="#three-o" data-toggle="tab">Налаштування</a></li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="one-o" style="max-height: 100%;">
                                        <?php if (empty($_SESSION['user']['price'])):?>
                                            <div class="col-md-6 login-right">
                                                <h3>Створити портфоліо</h3>
                                                <div class="strip"></div>
                                                <p>Якщо бажаєте створити портфоліо на Ваші послуги, зв'яжіться з нашим адміністратором, або надішліть нам повідомлення.</p>
                                                <a href="contact/" class="button">Надіслати повідомлення</a>
                                            </div>
                                        <?php else:?>
                                            <?=$_SESSION['user']['price'];?>
                                        <?php endif;?>
                                    </div>
                                    <div class="tab-pane fade" id="too-o">
                                        <?php if (empty($_SESSION['user']['price'])):?>
                                            <div class="col-md-6 login-right">
                                                <h3>Створити прайс цін</h3>
                                                <div class="strip"></div>
                                                <p>Якщо бажаєте створити прайс на Ваші послуги, зв'яжіться з нашим адміністратором, або надішліть нам повідомлення.</p>
                                                <a href="contact/" class="button">Надіслати повідомлення</a>
                                            </div>
                                        <?php else:?>
                                            <?=$_SESSION['user']['price'];?>
                                        <?php endif;?>
                                    </div>
                                    <div class="tab-pane fade" id="three-o">
                                        <form class="form-horizontal" action="user/profile" method="post" data-toggle="validator">
                                            <div class="card-body">
                                                <div class="tab-content">
                                                    <div class="form-group">
                                                        <label for="login" class="col-sm-2 control-label">Логін</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" id="login" name="login" value="<?=h($_SESSION['user']['login']);?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="password" class="col-sm-2 control-label">Пароль</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" id="password" name="password" placeholder="Введіть новий пароль">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="phone" class="col-sm-2 control-label">Телефон</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" id="phone" name="phone" placeholder="Введіть головний телефон який відображатиметься під Вашими орендованими сторінками, в формат  +38 (096)-123-45-67" value="<?=h($_SESSION['user']['phone']);?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 control-label">Ім'я</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" id="name" name="name" value="<?=h($_SESSION['user']['name']);?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email" class="col-sm-2 control-label">Емейл</label>
                                                        <div class="col-sm-10">
                                                            <input type="email" class="form-control has-feedback" name="email" id="email" value="<?=h($_SESSION['user']['email']);?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="address" class="col-sm-2 control-label">Адреса</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control has-feedback" id="address" name="address" value="<?=h($_SESSION['user']['address']);?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="description" class="col-sm-2 control-label">Опис послуг і вашої компанії (використовується для SEO )</label>
                                                        <div class="col-sm-10">
                                                            <textarea class="form-control" name="description" id="description" placeholder="ПРИКЛАД - ІВ ТЕХ - надаємо пуслуги з влаштування кабельного телебачення в Львові та Івано-Франківську. Кабельне телебачення за вігідною ціною у Львові - ІВ-ТЕХ"><?=h($_SESSION['user']['description']);?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 login-right">
                                                    <h3>Фільтер по якому Вас шукатимуть!</h3>
                                                    <div class="strip"></div>
                                                    <p style="margin-bottom: 30px;">Оберіть послуги, локацію та міста, для налаштування ефективного пошуку на платформі INHOUSE.</p>
                                                </div>
                                                <?php new \app\widgets\filter\Ufilter($filter, WWW . '/filter/cabin_filter_tpl.php');?>
                                                <div class="box-footer">
                                                    <div class="col-sm-10" style="margin-top: 30px">
                                                        <input type="hidden" name="id" value="<?=$_SESSION['user']['id'];?>">
                                                        <button type="submit" class="btn btn-danger">Внести зміни</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header p-3">
                                <ul class="nav nav-pills">
                                    <li class="nav-item"><a class="nav-link order"  style="font-size: 10px; margin-left: 5px;" href="#one-o-o" data-toggle="tab">Орендовані &nbsp; вами &nbsp;сторінки</a></li>
                                    <li class="nav-item"><a class="nav-link order"  style="font-size: 10px; margin-left: 5px;" href="#too-o-o" data-toggle="tab">Вільні  &nbsp;  сторінки</a></li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade" id="one-o-o" style="max-height: 100%;">
                                        <h4 class="cabin">Рекомендаційні сторінки</h4>
                                        <?php if (!$pages):?>
                                            <img class="non-pro" src="images/montage.png" alt="В даній категорії товари відсутні">
                                            <h4 class="more-user">Ви ще не орендуєте "Рекомендаційні сторінки"...</h4>
                                        <?php else:?>
                                            <section class="content">
                                                <div class="card-body p-0">
                                                    <table class="table">
                                                        <tr>
                                                            <th style="font-size: small; width: 5%;">ID</th>
                                                            <th style="font-size: small; width: 20%;">Назва</th>
                                                            <th style="font-size: small; width: 20%;">Ціна</th>
                                                            <th style="font-size: small; width: 20%;">Категорія</th>
                                                            <th style="font-size: small; width: 20%;">Дата завершення броні</th>
                                                            <th style="font-size: small; width: 20%;">Переглядів в місяць</th>
                                                            <th style="font-size: small">Перейти</th>
                                                        </tr>
                                                        <?php foreach ($pages as $page):?>
                                                            <tr>
                                                                <td><span class="badge" style="background-color: #d9534f; font-size: 10px;"><?=$page['id'];?></span></td>
                                                                <td><span class="badge" style="background-color: #cdd931; font-size: 10px; text-transform: uppercase;"><?=$page['title'];?></span></td>
                                                                <td><span class="badge" style="background-color: #7c1cd9; font-size: 10px;"><?=$page['price'];?>&nbsp;$</span></td>
                                                                <td><span class="badge" style="font-size: 10px;"><?=$page['cat'];?></span></td>
                                                                <td style="font-size: small"><?=$page['bought_to'];?></td>
                                                                <td style="font-size: small"><?=$page['monthe_view'];?></td>
                                                                <td><span class="badge" style="background-color: #d9534f;font-size: 10px;"><a href="recommend/<?=$page['alias'];?>" class="text-muted"><i class="fa fa-search"></i></a></span>
                                                            </tr>
                                                        <?php endforeach;?>
                                                    </table>
                                                </div>
                                            </section>
                                        <?php endif;?>
                                        <h4 class="cabin">Головні сторінки меню</h4>
                                        <?php if (!$servises):?>
                                            <img class="non-pro" src="images/montage.png" alt="В даній категорії товари відсутні">
                                            <h4 class="more-user">Ви ще не орендуєте "Головні сторінки"...</h4>
                                        <?php else:?>
                                            <section class="content">
                                                <div class="card-body p-0">
                                                    <table class="table">
                                                        <tr>
                                                            <th style="font-size: small; width: 5%;">ID</th>
                                                            <th style="font-size: small; width: 20%;">Назва</th>
                                                            <th style="font-size: small; width: 20%;">Ціна</th>
                                                            <th style="font-size: small; width: 20%;">Дата завершення броні</th>
                                                            <th style="font-size: small; width: 20%;">Переглядів в місяць</th>
                                                            <th style="font-size: small">Перейти</th>
                                                        </tr>
                                                        <?php foreach ($servises as $servis):?>
                                                            <tr>
                                                                <td><span class="badge" style="background-color: #d9534f; font-size: 10px;"><?=$servis['id'];?></span></td>
                                                                <td><span class="badge" style="background-color: #cdd931; font-size: 10px; text-transform: uppercase;"><?=$servis['title'];?></span></td>
                                                                <td><span class="badge" style="background-color: #7c1cd9; font-size: 10px;"><?=$servis['price'];?>&nbsp;$</span></td>
                                                                <td style="font-size: small"><?=$servis['bought_to'];?></td>
                                                                <td style="font-size: small"><?=$servis['monthe_view'];?></td>
                                                                <td><span class="badge" style="background-color: #d9534f; font-size: 10px;"><a href="services/<?=$servis['alias'];?>" class="text-muted"><i class="fa fa-search"></i></a></span>
                                                            </tr>
                                                        <?php endforeach;?>
                                                    </table>
                                                </div>
                                            </section>
                                        <?php endif;?>
                                        <h4 class="cabin">Послуги у містах</h4>
                                        <?php if (!$location_servises):?>
                                            <img class="non-pro" src="images/montage.png" alt="В даній категорії товари відсутні">
                                            <h4 class="more-user">Ви ще не орендуєте сторінки "Послуги у містах"...</h4>
                                        <?php else:?>
                                            <section class="content">
                                                <div class="card-body p-0">
                                                    <table class="table">
                                                        <tr>
                                                            <th style="font-size: small; width: 5%;">ID</th>
                                                            <th style="font-size: small; width: 20%;">Назва</th>
                                                            <th style="font-size: small; width: 20%;">Місто</th>
                                                            <th style="font-size: small; width: 20%;">Ціна</th>
                                                            <th style="font-size: small; width: 20%;">Дата завершення броні</th>
                                                            <th style="font-size: small; width: 20%;">Переглядів в місяць</th>
                                                            <th style="font-size: small">Перейти</th>
                                                        </tr>
                                                        <?php foreach ($location_servises as $servis):?>
                                                            <tr>
                                                                <td><span class="badge" style="background-color: #d9534f; font-size: 10px;"><?=$servis['id'];?></span></td>
                                                                <td><span class="badge" style="background-color: #cdd931; font-size: 10px; text-transform: uppercase;"><?=$servis['title'];?></span></td>
                                                                <td><span class="badge" style="background-color: #0e0e1f; font-size: 10px; text-transform: uppercase;"><?=$servis['location_id'];?></span></td>
                                                                <td><span class="badge" style="background-color: #7c1cd9; font-size: 10px;"><?=$servis['price'];?>&nbsp;$</span></td>
                                                                <td style="font-size: small"><?=$servis['bought_to'];?></td>
                                                                <td style="font-size: small"><?=$servis['monthe_view'];?></td>
                                                                <td><span class="badge" style="background-color: #d9534f; font-size: 10px;"><a href="services/<?=$servis['alias'];?>" class="text-muted"><i class="fa fa-search"></i></a></span>
                                                            </tr>
                                                        <?php endforeach;?>
                                                    </table>
                                                </div>
                                            </section>
                                        <?php endif;?>
                                        <h4 class="cabin"> Типи послуги</h4>
                                        <?php if (!$type_services):?>
                                            <img class="non-pro" src="images/montage.png" alt="В даній категорії товари відсутні">
                                            <h4 class="more-user">Ви ще не орендуєте сторінки "Типи послуг у містах"...</h4>
                                        <?php else:?>
                                            <section class="content">
                                                <div class="card-body p-0">
                                                    <table class="table">
                                                        <tr>
                                                            <th style="font-size: small; width: 5%;">ID</th>
                                                            <th style="font-size: small; width: 20%;">Назва</th>
                                                            <th style="font-size: small; width: 20%;">Послуга у містах</th>
                                                            <th style="font-size: small; width: 20%;">Ціна</th>
                                                            <th style="font-size: small; width: 20%;">Дата завершення броні</th>
                                                            <th style="font-size: small; width: 20%;">Переглядів в місяць</th>
                                                            <th style="font-size: small">Перейти</th>
                                                        </tr>
                                                        <?php foreach ($type_services as $servis):?>
                                                            <tr>
                                                                <td><span class="badge" style="background-color: #d9534f; font-size: 10px;"><?=$servis['id'];?></span></td>
                                                                <td><span class="badge" style="background-color: #cdd931; font-size: 10px; text-transform: uppercase;"><?=$servis['title'];?></span></td>
                                                                <td><span class="badge" style="background-color: #0e0e1f; font-size: 10px; text-transform: uppercase;"><?=$servis['servises_id'];?></span></td>
                                                                <td><span class="badge" style="background-color: #7c1cd9; font-size: 10px;"><?=$servis['price'];?>&nbsp;$</span></td>
                                                                <td style="font-size: small"><?=$servis['bought_to'];?></td>
                                                                <td style="font-size: small"><?=$servis['monthe_view'];?></td>
                                                                <td><span class="badge" style="background-color: #d9534f; font-size: 10px;"><a href="services/<?=$servis['alias'];?>" class="text-muted"><i class="fa fa-search"></i></a></span>
                                                            </tr>
                                                        <?php endforeach;?>
                                                    </table>
                                                </div>
                                            </section>
                                        <?php endif;?>
                                    </div>
                                    <div class="tab-pane fade" id="too-o-o">
                                        <?php if (empty($_SESSION['user']['price'])):?>
                                            <div class="col-md-6 login-right">
                                                <h3>Як збільшити кількість замовлень?</h3>
                                                <div class="strip"></div>
                                                <p>Якщо бажаєте створити прайс на Ваші послуги, зв'яжіться з нашим адміністратором, або надішліть нам повідомлення.</p>
                                                <a href="contact/" class="button">Надіслати повідомлення</a>
                                            </div>
                                        <?php else:?>
                                            <?=$_SESSION['user']['price'];?>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php endif;?>
