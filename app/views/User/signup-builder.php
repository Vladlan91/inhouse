<div class="head-bread">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Головна</a></li>
            <li class="active">Реєстрація</li>
        </ol>
    </div>
</div>
<div class="reg-form">
    <div class="container">
        <div class="reg">
            <form method="post" action="user/signup" data-toggle="validator" id="signup" role="form">
                <div class="col-md-6 log">
                    <h3>Реєстрація</h3>
                    <p>Ласкаво просимо, будь ласка, введіть наступну інформацію, щоб продовжити.</p>
                    <p>Якщо ви раніше зареєструвалися у нас, <a href="user/login">натисніть тут</a></p>
                    <div class="form form-group has-feedback">
                        <ul>
                            <li class="text-info">Логін: </li>
                            <input type="text" class="form-control" name="login" value="<?=isset($_SESSION['form_data']['login']) ? h($_SESSION['form_data']['login']) : '';?>" required >
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </ul>
                    </div>
                    <div class="form-group has-feedback">
                        <ul>
                            <li class="text-info">Пароль: </li>
                            <input type="password" class="form-control" name="password" value="<?=isset($_SESSION['form_data']['password']) ? h($_SESSION['form_data']['password']): '' ;?>" id="password" placeholder="Password" data-error="Пароль повинен бути більше 6 символів" data-minLenght="6" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </ul>
                    </div>
                    <div class="form-group has-feedback">
                        <ul>
                            <li class="text-info">Ім'я: </li>
                            <input type="text" class="form-control" name="name" value="<?=isset($_SESSION['form_data']['name']) ? h($_SESSION['form_data']['name']) : '';?>"  required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </ul>
                    </div>
                    <div class="form-group has-feedback">
                        <ul>
                            <li class="text-info">Емейл: </li>
                            <input type="text" class="form-control" name="email" value="<?=isset($_SESSION['form_data']['email']) ? h($_SESSION['form_data']['email']) : '';?>"  required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </ul>
                    </div>
                    <div class=" form-group has-feedback">
                        <ul>
                            <li class="text-info">Адреса:</li>
                            <input type="text" class="form-control" name="address" value="<?=isset($_SESSION['form_data']['address']) ? h($_SESSION['form_data']['address']): '';?>"  required>
                        </ul>
                    </div>
                    <div class=" form-group has-feedback">
                        <ul>
                            <li class="text-info">Телефон:</li>
                            <input type="text" class="form-control" id="phone" name="phone" placeholder="Введіть головний телефон, в формат  +38 (096)-123-45-67" value="<?=isset($_SESSION['form_data']['phone']) ? h($_SESSION['form_data']['phone']): '';?>">
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 login-right">
                    <h3>Створити акаунт</h3>
                    <div class="strip"></div>
                    <p>Ставши зареєстрованим користувачем Ви матимете нагоду отримувати накопичувальну знижку по товарам які придбаєте у нас, розмір знижки залежитиме від величини замовлень!</p>
                    <a href="user/signup" class="button">Звичайний акаунт</a>
                    <a href="user/signup-master" class="button">Майстр</a>
                    <a href="user/signup-builder" class="button">Будівельник</a>
                </div>
                <div class="clearfix"></div>
                <input type="hidden" name="role" id="role" value="builder">
                <input type="submit" class="order" value="Зареєструватись">
                <p class="click">Натиснувши цю кнопку, ви погоджуєтеся з <a href="#">Загальними положеннями та умовами політики</a></p>
                <?php if (isset($_SESSION['form_data'])); unset($_SESSION['form_data']);?>
            </form>
        </div>
    </div>