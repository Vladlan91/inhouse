<div class="head-bread">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Головна</a></li>
            <li class="active">Перегляд профіля</li>
        </ol>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error']; unset( $_SESSION['error']); ?>
                </div>
            <?php endif;?>
            <?php if (isset($_SESSION['success'])):?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success']; unset( $_SESSION['success']); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<section class="container">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle" src="/images/user2-160x160.jpg<?=$user->avatar;?>" alt="User profile picture">
                        </div>
                        <h3 class="profile-username text-center"><?=$user->name;?></h3>
                        <?php if ($_SESSION['user']['role'] == user):?>
                            <p class="text-muted text-center">Користувач</p>
                        <?php elseif ($_SESSION['user']['role'] == admin):?>
                            <p class="text-muted text-center">Адміністратор</p>
                        <?php elseif ($_SESSION['user']['builder'] == master):?>
                            <p class="text-muted text-center">Майстер</p>
                        <?php elseif ($_SESSION['user']['builder'] == builder):?>
                            <p class="text-muted text-center">Будівельник</p>
                        <?php endif;?>
                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>Переглядів</b> <a class="float-right">454 чол.</a>
                            </li>
                            <li class="list-group-item">
                                <b>Додано в закладки:</b> <a class="float-right">1123 чол.</a>
                            </li>
                            <li class="list-group-item">
                                <b>Замовлень:</b> <a class="float-right">13 287 00 грн.</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title" style="margin: 15px;">Інформація</h3>
                    </div>
                    <div class="card-body">
                        <?php if ($user->role == 'user' || $user->role == 'admin'):?>
                            <hr>
                            <strong><i class="fa fa-map-marker mr-1"  style="margin-right: 15px;"></i>Адреса</strong>
                            <p class="text-muted"><?=$user->address;?></p>
                            <hr>
                        <?php else:?>
                            <strong><i class="fa fa-book mr-1"  style="margin-right: 15px;"></i>м. Надання послуг</strong>
                            <p class="text-muted">
                                <span class="tag tag-danger">Львів/</span>
                                <span class="tag tag-danger">Тернопіль/</span>
                            </p>
                            <hr>
                            <strong><i class="fa fa-map-marker mr-1"  style="margin-right: 15px;"></i>Адреса</strong>
                            <p class="text-muted"><?=$user->address;?></p>
                            <hr>
                            <strong><i class="fa fa-pencil mr-1"  style="margin-right: 15px;"></i>Сфера діяльності</strong>
                            <p class="text-muted">
                                <span class="tag tag-danger">UI Design</span>
                                <span class="tag tag-success">Coding</span>
                                <span class="tag tag-info">Javascript</span>
                                <span class="tag tag-warning">PHP</span>
                                <span class="tag tag-primary">Node.js</span>
                            </p>
                            <hr>
                            <strong><i class="fa fa-file-text-o mr-1"  style="margin-right: 15px;"></i>Примітки</strong>
                            <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
                        <?php endif;?>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header p-2">
                        <ul class="nav nav-pills">
                            <li class="nav-item"><a class="nav-link order"  style="font-size: 10px; margin-left: 5px;" href="#settings" data-toggle="tab">Послуги</a></li>
                            <li class="nav-item"><a class="nav-link order"  style="font-size: 10px; margin-left: 5px;" href="#timeline" data-toggle="tab">Відгуки</a></li>
                            <li class="nav-item"><a class="nav-link order"  style="font-size: 10px; margin-left: 5px;" href="#timeline" data-toggle="tab">Портфоліо</a></li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane" id="activity">

                            </div>
                            <div class="tab-pane" id="timeline">

                            </div>
                            <div class="tab-pane" id="settings">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
