<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 14/10/2018
 * Time: 11:02
 */

namespace app\widgets\filter;

use inhouse\App;
use inhouse\Cash;

class Ufilter {

    public $groups;
    public $attrs;
    public $tpl;
    public $filter;
    public $cash = 3600;

    public function __construct($filter = null, $tpl ='') {
        $this->filter = $filter;
        $this->tpl = $tpl ?: __DIR__ . '/filter_tpl/ufilter_tpl.php';
        $this->run();
    }

    protected function run(){
        $cash = Cash::instance();
        $this->groups = $cash->get('u_filter_group');
        if (!$this->groups){
            $this->groups = $this->getGroups();
            $cash->set('u_filter_group', $this->groups);

        }
        $this->attrs = $cash->get('u_filter_attrs');
        if (!$this->attrs){
            $this->attrs = self::getAttrs();
            $cash->set('u_filter_attrs', $this->attrs, $this->cash);
        }
        $filters = $this->getHtml();
        echo $filters;
    }

    protected function getHtml(){
        ob_start();
        require $this->tpl;
        return ob_get_clean();

    }

    public static function setFilter(){
        $filter = self::getFilter();
        $filter = explode(',', $filter);
        $_SESSION['ufilter'] = $filter;
    }

    protected function getGroups(){
        return \R::getAssoc('SELECT id, title FROM u_attribute_group');
    }

    protected static function getAttrs(){
        $data =  \R::getAssoc('SELECT * FROM u_attribute_value');
        $attrs = [];
        foreach ($data as $k => $v){
            $attrs[$v['attr_group_id']][$k] = $v['value'];
        }
        return $attrs;
    }

    public static function getFilter() {
        $filter = null;
        if (!empty($_GET['filter'])) {
            $filter = preg_replace("#[^\d,]+#", '', $_GET['filter']);
            $filter = trim($filter, ',');
        }
        return $filter;
    }

    public static function getCountGroups($filter){

        $filters = explode(',', $filter);
        $cash = Cash::instance();
        $attrs = $cash->get('u_filter_attrs');
        if (!$attrs){
            $attrs = self::getAttrs();
        }
        $data = [];
        foreach ($attrs as $kay => $item){
            foreach ($item as $k=> $v){
                if (in_array($k, $filters)){
                    $data [] = $k;
                    break;
                }
            }
        }
        return count($data);

    }
}