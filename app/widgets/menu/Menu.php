<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 06/06/2018
 * Time: 12:06
 */

namespace app\widgets\menu;


use inhouse\App;
use inhouse\Cash;

class Menu {

    protected $data;
    protected $tree;
    protected $menuHtml;
    protected $tpl;
    protected $class = '';
    protected $container = '';
    protected $table = 'category';
    protected $cash = 0;
    protected $cashKey = 'inhouse_menu';
    protected $attrs = [];
    protected $prepend = '';
    protected $prop ='';

    public function __construct($option = []) {
        $this->tpl = __DIR__ . '/menu_tpl/menu.php';
        $this->getOption($option);
        $this->run();

    }

    protected function getOption($option){
        foreach ($option as $k => $v){
            if (property_exists($this, $k)){
                $this->$k = $v;
            }
        }


    }

    protected function run(){
        $cash = Cash::instance();
        $this->menuHtml = $cash->get($this->cashKey);

        if (!$this->menuHtml){
            $this->data = App::$app->getProperty($this->prop);
            if (!$this->data){
                $this->data = $cats = \R::getAssoc("SELECT * FROM {$this->table}");
            }
            $this->tree = $this->getTree();
            $this->menuHtml = $this->getMenuHtml($this->tree);
            if ($this->cash){
                $cash->set($this->cashKey, $this->menuHtml, $this->cash);
            }
        }
        $this->output();

    }

    protected function output(){
        $attrs = '';
        if (!empty($this->attrs)){
            foreach ($this->attrs as $k=>$v){
                $attrs .= " $k='$v' ";
            }
        }
//        echo "<{$this->container} class=' {$this->class}' $attrs>";
             echo $this->prepend;
             echo $this->menuHtml;
        echo "</{$this->container}>";

    }

    protected function getTree(){
        $tree = [];
        $data = $this->data;
        foreach ($data as $id=>&$node){
            if (!$node['parent_id']){
                $tree[$id] = &$node;
            }else{
                $data[$node['parent_id']]['childs'][$id] = &$node;
            }
        }
        return $tree;

    }

    protected function getMenuHtml($tree, $tab = ''){
    $str = '';
        foreach ($tree as $id => $category){
            $str .= $this->catToTemplate($category, $tab, $id);
        }
    return $str;

    }

    protected function catToTemplate($category, $tab, $id){
    ob_start();
    require $this->tpl;
    return ob_get_clean();

    }


}