<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 23/05/2018
 * Time: 21:55
 */

namespace inhouse;


trait TSingleton {

 private static $instance;

 public static function instance(){

     if(self::$instance === null){
         self::$instance = new self;
     }
     return self::$instance;
 }

}