<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 20/06/2018
 * Time: 12:27
 */

namespace inhouse\libs;


class Pagination {

    public $currentPage;
    public $perpage;
    public $total;
    public $countpage;
    public $uri;
//    public $links = [
//        'back' => '', //Cилка НАЗАД
//        'forward' => '', //Cилка ВПЕРЕД
//        'startpage' => '', //Cилка НА ПОЧАТОК
//        'endpage' => '', //Cилка В КІНЕЦЬ
//        'page2left' => '', //друга сторінка з ліва
//        'page1left' => '', //перша сторінка з ліва
//        'page2right' => '', //друга сторінка з права
//        'back1right' => '', //перша сторінка з права
//    ];

    public function __construct($page, $perpage, $total) {
        $this->perpage = $perpage;
        $this->total = $total;
        $this->countpage = $this->getCountPages();
        $this->currentPage = $this->getCurrentPage($page);
        $this->uri = $this->getParams();
    }

    public function getHTML(){
        $back = null; //Cилка НАЗАД
        $forward = null; //Cилка ВПЕРЕД
        $startpage = null; //Cилка НА ПОЧАТОК
        $endpage = null; //Cилка В КІНЕЦЬ
        $page2left = null; //друга сторінка з ліва
        $page1left = null; //перша сторінка з ліва
        $page2right = null; //друга сторінка з права
        $page1right = null; //перша сторінка з права


        if ($this->currentPage > 1){
            $back = "<a class='page rig' href='{$this->uri} page=" .($this->currentPage -1)."'>&lt;</a>";
        }
        if ($this->currentPage < $this->countpage){
            $forward = "<a class='page lef' href='{$this->uri} page=" .($this->currentPage +1)."'>&gt;</a>";
        }
        if ($this->currentPage > 3){
            $startpage = "<a class='page' href='{$this->uri} page=1'>&laquo;</a>";
        }
        if ($this->currentPage < ($this->countpage - 2)){
            $endpage = "<a class='page ' href='{$this->uri} page={$this->countpage}'>&raquo;</a>";
        }
        if ($this->currentPage -2 > 0){
            $page2left = "<a class='page' href='{$this->uri} page=".($this->currentPage -2)."'>".($this->currentPage -2)."</a>";
        }
        if ($this->currentPage -1 > 0){
            $page1left = "<a class='page' href='{$this->uri} page=".($this->currentPage -1)."'>".($this->currentPage -1)."</a>";
        }
        if ($this->currentPage +1 <= $this->countpage){
            $page1right = "<a class='page' href='{$this->uri} page=".($this->currentPage +1)."'>".($this->currentPage +1)."</a>";
        }
        if ($this->currentPage +2 <= $this->countpage){
            $page2right = "<a class='page' href='{$this->uri} page=".($this->currentPage +2)."'>".($this->currentPage +2)."</a>";
        }
        return '<div id="page-nav" class="">'.$startpage.$back.$page2left.$page1left.'<a class="page page active">'.$this->currentPage.'</a>'.$page1right.$page2right.$forward.$endpage.'</div>';

    }

    public function __toString() {
        return $this->getHTML();
    }

    public function getCountPages(){
        return ceil($this->total / $this->perpage) ?: 1;
        //Ok
    }

    public function getCurrentPage($page){
        if (!$page || $page < 1) $page = 1;
        if ($page > $this->countpage) $page = $this->countpage;
        return $page;
    }

    public function getStart(){
        return ($this->currentPage - 1) * $this->perpage;
    }

    public function getParams(){
        $url = $_SERVER['REQUEST_URI'];
        preg_match_all("#filter=[\d,&]#", $url, $matches);
        if (count($matches[0]) > 1){
            $url = preg_replace("#filter=[\d,&]+#", "", $url, 1);
        }
        $url = explode('?', $url);
        $uri = $url[0] . '?';
        if (isset($url[1]) && $url[1] != ''){
            $params = explode('&', $url[1]);
            foreach ($params as $param){
            if (!preg_match("#page=#", $param)) $uri .= "{$param}&amp;";
            }
        }
        return urldecode($uri);
    }

}