<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 24/05/2018
 * Time: 10:15
 */

function debug($arr, $die = false){
    echo '<pre style="background-color: #1F1818; color: yellow; padding: 30px; border-color: red">'.print_r($arr, true).'</pre>';
    if ($die){
        die;
    }
}

function redirect($http = false){
    if ($http){
        $redirect = $http;
    }else{
        $redirect = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : PATH;
    }
    header("Location: $redirect ");
    exit;
}

function pruning($text, $lenght = 0){
    $b = mb_substr($text, 0, $lenght);
    if (mb_strlen($text) > $lenght) {
        $b .= '...';
    }
    return $b;
}

function h($str){

    return htmlspecialchars($str, ENT_QUOTES);

}