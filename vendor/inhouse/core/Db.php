<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 26/05/2018
 * Time: 11:33
 */

namespace inhouse;


class Db {

    use TSingleton;

    protected function __construct(){
        $db = require_once CONFIG . '/config_db.php';
        class_alias('\RedBeanPHP\R','\R');
        \R::setup( $db['dsn'], $db['user'], $db['pass']);
        //\R::setup("mysql:host=localhost;dbname=inhouse","root","root");
        if ( !\R::testConnection()){
            throw new \Exception("База даних не підключена", 404);
        }
        \R::freeze('true');
        if (DEBAG){
            \R::debug(true, 1);
        }

        \R::ext('xdispense', function ($type){
            return \R::getRedBean()->dispense($type);
        });
    }

}