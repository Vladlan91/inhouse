<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 24/05/2018
 * Time: 12:27
 */

namespace inhouse;


class ErrorHandler {

    public function __construct() {

        if (DEBAG){
            error_reporting(-1);
        }
        else {
            error_reporting(0);
        }
        set_exception_handler([$this,'exceptionHandler']);
    }

    public function exceptionHandler($a){
        $this->logErrors($a->getMessage(), $a->getFile(), $a->getLine());
        $this->displayError('Исключения',$a->getMessage(), $a->getFile(), $a->getLine(), $a->getCode());

    }

    protected function logErrors($message = '', $file = '', $line = ''){
        error_log("[". date("Y-m-d H:i:s")."] Teкст помилки: {$message}| Файл: {$file}| 
        Строка: {$line}\n=========================\n",3, ROOT . '/tmp/errors.log');

    }

    protected function displayError($errno, $errstr, $errfile, $errline, $response = 404){

        http_response_code($response);
        if ($response == 404 && !DEBAG){
            require WWW . '/errors/404.php';
            die;
        }
        if (DEBAG){
            require WWW . '/errors/dev.php';

        }
        else{
            require WWW . '/errors/prod.php';
        }
        die;

    }

}