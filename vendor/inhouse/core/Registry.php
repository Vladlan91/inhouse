<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 23/05/2018
 * Time: 21:46
 */

namespace inhouse;


class Registry {

    use TSingleton;

    public static $propertise;

    public function setProperty($name, $value){

        self::$propertise[$name]=$value;
    }

    public function getProperty($name){

        if(isset(self::$propertise[$name])){
            return self::$propertise[$name];
        }
        return null;
    }

    public function getPropertise(){

        return self::$propertise;
    }


}