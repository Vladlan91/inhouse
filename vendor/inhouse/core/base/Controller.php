<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 25/05/2018
 * Time: 11:27
 */

namespace inhouse\base;


abstract class Controller {

    public $route;
    public $controller;
    public $model;
    public $view;
    public $prefix;
    public $layout;
    public $data = [];
    public $meta = ['title'=>'','desc'=>'','keywords'=>'',];
    public $order_id;

public function __construct($route) {

    $this->route = $route;
    $this->controller = $route['controller'];
    $this->model = $route['controller'];
    $this->view = $route['action'];
    $this->prefix = $route['prefix'];
}
    public function getView(){

        $viewObject = new View($this->route, $this->layout, $this->view, $this->meta );
        $viewObject->render($this->data);
    }

    public function set($data) {

        $this->data = $data;

    }

    public function setMeta($title = '', $desc = '', $keywords = '') {

        $this->meta['title'] = $title;
        $this->meta['desc'] = $desc;
        $this->meta['keywords'] = $keywords;


    }

    public function isAjax() {
        isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] : null;
        return ($header === 'XMLHttpRequest');
        //return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && (strtolower(getenv('HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest'));
    }

    public function loadView($view, $vars = []){

        extract($vars);
        require APP . "/views/{$this->prefix}{$this->controller}/{$view}.php";
        die;
    }


}