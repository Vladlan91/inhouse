<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 24/05/2018
 * Time: 18:42
 */

namespace inhouse;


use app\controllers\ServicesController;

class Router {

    public static $routes = [];
    public static $route = [];

    public static function add($regexp, $route = [] ){

        self::$routes[$regexp] = $route;

    }

    public static function getRoutes(){

        return self::$routes;

    }

    public static function getRoute(){

        return self::$route;

    }

    public static function dispatch($url){
        $url = self::removeQueryString($url);
        if(self::matchRoute($url)){
            if(self::matchRoute($url)){
                //Внесено зміни
                if (self::$route['prefix']){
                    $prefix = substr(self::$route['prefix'],0,strlen(self::$route['prefix'])-1);
                    $prefix .='\\';
                }
                $controller = 'app\controllers\\' . self::$route['prefix'] . self::$route['controller'] . 'Controller';
                if (class_exists( $controller)){
                  $controllerObgect = new $controller(self::$route);
                  $action = self::lowerCamelCase(self::$route['action']) . 'Action';
                  if (method_exists($controllerObgect, $action)){
                      $controllerObgect->$action();
                      $controllerObgect->getView();


                  } else{

                      throw new \Exception("Метод $controller::$action не існує", 404);
                  }
              } else{

                  throw new \Exception("Контролер $controller не існує", 404);
              }
            }
        } else {

            throw new \Exception("Cторінка не існує", 404);
        }

    }

    public static function matchRoute($url){
        foreach (self::$routes as $pattern=>$route){
            if (preg_match("#{$pattern}#i", $url,$matches)){
                foreach ($matches as $k => $v){
                    if (is_string($k)){
                        $route[$k] = $v;
                    }
                }
                if (empty($route['action'])){
                    $route['action'] = 'index';
                }
                if (!isset($route['prefix'])){
                    $route['prefix'] = '';
                } else{

                    $route['prefix'] .= '\\';

                }
                $route['controller'] = self::upperCamelCase($route['controller']);
                self::$route = $route;
                return true;
            }
        }

        return false;
    }

    protected static function upperCamelCase($name){

        return str_replace( ' ', '',ucwords(str_replace('-', ' ', $name)));

    }

    protected static function lowerCamelCase($name){

        return lcfirst(self::upperCamelCase($name));
    }

    protected static function removeQueryString($url){

        $params = explode('&',$url,2);
        if (false === strpos($params[0],'=')){
            return rtrim($params[0], '/');
        }else{
            return '';
        }

    }

}