<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 24/05/2018
 * Time: 18:41
 */

use inhouse\Router;

Router::add('^admin$', ['controller'=>'Main', 'action'=>'index', 'prefix'=>'admin']);
Router::add('^admin/?(?P<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$' , ['prefix'=>'admin']);
Router::add('^$', ['controller'=>'Main', 'action'=>'index']);
//Router::add('^cart/delete$', ['controller'=>'Cart', 'action'=>'delete']);
Router::add('^product/(?P<alias>[a-z-/0-9]+)/?$', ['controller'=>'Product', 'action'=>'view']);
Router::add('^services/location$',  ['controller'=>'Services', 'action'=>'location']);
Router::add('^services/(?P<alias>[a-z-/0-9]+)/?$', ['controller'=>'Services', 'action'=>'index']);
Router::add('^category/(?P<alias>[a-z-/0-9]+)/?$', ['controller'=>'Category', 'action'=>'view']);
Router::add('^recommend/find$', ['controller'=>'Recommend', 'action'=>'find']);
Router::add('^recommend/(?P<alias>[a-z-/0-9]+)/?$', ['controller'=>'Recommend', 'action'=>'view']);
Router::add('^location/find$',  ['controller'=>'Location', 'action'=>'find']);
Router::add('^location/(?P<alias>[a-z-/0-9]+)/?$', ['controller'=>'Location', 'action'=>'view']);
Router::add('^type/(?P<alias>[a-z-/0-9]+)/?$', ['controller'=>'Type', 'action'=>'index']);
Router::add('^brand/(?P<alias>[a-z-/0-9]+)/?$', ['controller'=>'Brand', 'action'=>'view']);
Router::add('^master/(?P<alias>[a-z-/0-9]+)/?$', ['controller'=>'Master', 'action'=>'view']);
Router::add('^builder/(?P<alias>[a-z-/0-9]+)/?$', ['controller'=>'Builder', 'action'=>'view']);
//
//Router::add('^category/all$', ['controller'=>'Category', 'action'=>'all']);
//Router::add('^cart/show$', ['controller'=>'Cart', 'action'=>'show']);
//Router::add('^cart/view$', ['controller'=>'Cart', 'action'=>'view']);
//Router::add('^cart/checkout$', ['controller'=>'Cart', 'action'=>'checkout']);
//Router::add('^cart/clear$', ['controller'=>'Cart', 'action'=>'clear']);
//Router::add('^brand$', ['controller'=>'Brand', 'action'=>'index']);
//Router::add('^builder$', ['controller'=>'Builder', 'action'=>'index']);
//Router::add('^price$', ['controller'=>'Price', 'action'=>'index']);
//Router::add('^cart/add$', ['controller'=>'Cart', 'action'=>'add']);
//Router::add('^currency/change$', ['controller'=>'Currency', 'action'=>'change']);
//Router::add('^master$', ['controller'=>'Master', 'action'=>'index']);
//Router::add('^calculator$', ['controller'=>'Calculator', 'action'=>'index']);
//Router::add('^contact$', ['controller'=>'Contact', 'action'=>'index']);
//Router::add('^contact/add$', ['controller'=>'Contact', 'action'=>'add']);
//Router::add('^search$', ['controller'=>'Search', 'action'=>'index']);
//Router::add('^user/view$', ['controller'=>'User', 'action'=>'view']);
//Router::add('^user/logout$', ['controller'=>'User', 'action'=>'logout']);
//Router::add('^user/login$', ['controller'=>'User', 'action'=>'login']);
//Router::add('^user/signup$', ['controller'=>'User', 'action'=>'signup']);
//Router::add('^user/signup-master$', ['controller'=>'User', 'action'=>'signup-master']);
//Router::add('^user/signup-builder$', ['controller'=>'User', 'action'=>'signup-builder']);
//Router::add('^user/cabinet$', ['controller'=>'User', 'action'=>'cabinet']);
//Router::add('^user/profile$', ['controller'=>'User', 'action'=>'profile']);
//Router::add('^user/order-view$', ['controller'=>'User', 'action'=>'order-view']);
//Router::add('^recommend$', ['controller'=>'Recommend', 'action'=>'index']);
//Router::add('^location$',  ['controller'=>'Location', 'action'=>'index']);
//Router::add('^location/find$',  ['controller'=>'Location', 'action'=>'find']);
//Router::add('^(?P<alias>[a-z-/0-9]+)/?$', ['controller'=>'Type', 'action'=>'index']);
Router::add('^(?P<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$');


