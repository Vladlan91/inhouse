<?php

define("DEBAG",1);
define("ROOT",dirname(__DIR__));
define("WWW", ROOT . '/public');
define("APP", ROOT . '/app');
define("CORE", ROOT . '/vendor/inhouse/core');
define("LIBS", ROOT . '/vendor/inhouse/core/libs');
define("CASH", ROOT . '/tmp/cash');
define("CONFIG", ROOT . '/config');
define("LAYOUT", 'luxury');
//http:// localhost:8888/public/post.php
$app_path = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['PHP_SELF']}";
//http:// localhost:8888/public/
$app_path = preg_replace("#[^/]+$#", '', $app_path );
//http:// localhost:8888
$app_path = str_replace('/public/', '', $app_path);

define("PATH", $app_path);
define("ADMIN", PATH.'/admin');

require_once ROOT . '/vendor/autoload.php';

